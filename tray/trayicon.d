/* Invisible Vector Notification Icon Server
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
module tray.trayicon;
private:

import core.stdc.config;
import iv.x11;
import iv.imlib2;

import iv.vfs;


// ///////////////////////////////////////////////////////////////////////// //
static immutable(ubyte)[] TrayIconColorPng = cast(immutable(ubyte)[])import("tray/logo-16.png");
static immutable(ubyte)[] TrayIconGrayPng = cast(immutable(ubyte)[])import("tray/logo-16-gray.png");

public enum TrayIconColorName = "/tmp/miri_icon_000.png";
public enum TrayIconGrayName = "/tmp/miri_icon_001.png";


// ///////////////////////////////////////////////////////////////////////// //
public void writeTrayIcons () {
  try {
    VFile(TrayIconColorName, "w").rawWriteExact(TrayIconColorPng[]);
    VFile(TrayIconGrayName, "w").rawWriteExact(TrayIconGrayPng[]);
  } catch (Exception) {}
}


// ///////////////////////////////////////////////////////////////////////// //
public class TrayIcon {
private:
  enum {
    REQUEST_DOCK   = 0,
    BEGIN_MESSAGE  = 1,
    CANCEL_MESSAGE = 2,
  }

  Display *mDpy;
  int mScreen = -1;
  Atom mSTAtom;
  Atom netwmIconA;
  Atom cardinalA;
  Window mTrayWin;
  Window mWin;
  Imlib_Context mICtx;
  uint mWidth, mHeight; // of mWin
  string mIconFile;

private:
  void initialize () {
    if (mScreen < 0) {
      import std.string : format;
      mScreen = DefaultScreen(mDpy);
      auto name = format("_NET_SYSTEM_TRAY_S%s\0", mScreen);
      mSTAtom = XInternAtom(mDpy, name.ptr, False);
      netwmIconA = XInternAtom(mDpy, "_NET_WM_ICON", False);
      cardinalA = XInternAtom(mDpy, "CARDINAL", False);
      mTrayWin = XGetSelectionOwner(mDpy, mSTAtom);
      mICtx = imlib_context_new();
    }
  }

  void createWindow () {
    if (!mTrayWin) return;
    // create the icon window
    mWidth = 16;
    mHeight = 16;
    /+
    XSetWindowAttributes winAttr;
    uint winAttrMask = 0;
    mWin = XCreateWindow(
      mDpy,
      /*mTrayWin,*/DefaultRootWindow(mDpy),
      0, 0, mWidth, mHeight,
      0,
      DefaultDepth(mDpy, mScreen),
      InputOutput,
      cast(Visual*)CopyFromParent,
      winAttrMask,
      &winAttr);
    +/
    mWin = XCreateSimpleWindow(
      mDpy,
      DefaultRootWindow(mDpy), //parent window
      0, 0, mWidth, mHeight, 0, //x, y, width, height, border_width
      BlackPixel(mDpy, mScreen), //border_color
      BlackPixel(mDpy, mScreen), //back_color
    );
    XSelectInput(mDpy, mWin,
      ExposureMask|
      ButtonPressMask|ButtonReleaseMask|
      StructureNotifyMask|
      EnterWindowMask|LeaveWindowMask);

    // set WM_CLASS
    XClassHint* classHint = XAllocClassHint();
    classHint.res_name = "NotifyTrayIconServer";
    classHint.res_class = "NotifyTrayIconServer";
    XSetClassHint(mDpy, mWin, classHint);
    XFree(classHint);

    // set window name
    string name = "TestTrayIcon";
    XChangeProperty(
      mDpy, mWin,
      XInternAtom(mDpy, "_NET_WM_NAME", False),
      XInternAtom(mDpy, "UTF8_STRING", False),
      8, PropModeReplace,
      cast(ubyte*)name.ptr, cast(int)name.length);
  }

  void releaseImage () {
    imlib_context_push(mICtx);
    if (imlib_context_get_image()) imlib_free_image();
    imlib_context_pop();
  }

  void setIcon () {
    if (!mWin || !mIconFile.length) releaseImage();
    imlib_context_push(mICtx);
    scope(exit) imlib_context_pop();
    if (imlib_context_get_image()) imlib_free_image();
    imlib_set_color_usage(128);
    imlib_context_set_dither(1);
    imlib_context_set_display(mDpy);
    imlib_context_set_visual(DefaultVisual(mDpy, mScreen));
    imlib_context_set_colormap(DefaultColormap(mDpy, mScreen));
    imlib_context_set_drawable(cast(Drawable)mWin);
    Imlib_Image image = imlib_load_image(mIconFile.ptr);
    if (image) imlib_context_set_image(image);
  }

  void sendTrayOpcode (int opcode, c_long data1=0, c_long data2=0, c_long data3=0) {
    if (!mTrayWin) return;
    XEvent ev;
    //memset(&ev, 0, sizeof(ev));
    ev.xclient.type = ClientMessage;
    ev.xclient.window = mTrayWin;
    ev.xclient.message_type = XInternAtom(mDpy, "_NET_SYSTEM_TRAY_OPCODE", False);
    ev.xclient.format = 32;
    ev.xclient.data.l[0] = CurrentTime;
    ev.xclient.data.l[1] = opcode;
    ev.xclient.data.l[2] = data1;
    ev.xclient.data.l[3] = data2;
    ev.xclient.data.l[4] = data3;
    XSendEvent(mDpy, mTrayWin, False, NoEventMask, &ev);
  }

public:
  this (Display *adpy, string icontga=null) {
    mDpy = adpy;
    //mIcon = new Targa(icontga);
    initialize();
    if (icontga.length) mIconFile = icontga~"\0";
  }

  ~this () {
    releaseImage();
    hide();
    if (mICtx) imlib_context_free(mICtx);
  }

  void delegate (TrayIcon self) onLButtonDown;
  void delegate (TrayIcon self) onLButtonUp;
  void delegate (TrayIcon self) onRButtonDown;
  void delegate (TrayIcon self) onRButtonUp;
  void delegate (TrayIcon self) onMButtonDown;
  void delegate (TrayIcon self) onMButtonUp;
  void delegate (TrayIcon self) onEnter;
  void delegate (TrayIcon self) onLeave;

  @property bool valid () const @safe pure nothrow @nogc { return (mWin != 0); }
  @property Window window () const @safe pure nothrow @nogc { return mWin; }

  void show () {
    initialize();
    if (!mWin && mTrayWin) {
      createWindow();
      if (mWin) {
        setIcon();
        //XMapWindow(mDpy, mWin);
        sendTrayOpcode(REQUEST_DOCK, cast(c_long)mWin);
      }
    }
  }

  void hide () {
    releaseImage();
    if (mWin) {
      XUnmapWindow(mDpy, mWin);
      XDestroyWindow(mDpy, mWin);
      mWin = 0;
    }
  }

  void setImage (string fname) {
    releaseImage();
    mIconFile = fname~"\0";
    if (mWin) {
      setIcon();
      paintImage();
    }
  }

  private void paintImage () {
    if (!mWin) return;
    imlib_context_push(mICtx);
    scope(exit) imlib_context_pop();
    if (imlib_context_get_image()) {
      int wdt = imlib_image_get_width();
      int hgt = imlib_image_get_height();
      int xofs = (wdt < mWidth ? (mWidth-wdt)/2 : 0);
      int yofs = (wdt < mHeight ? (mHeight-hgt)/2 : 0);
      imlib_render_image_on_drawable_at_size(xofs, yofs, mWidth, mHeight);
    }
  }

  bool processEvent (in XEvent ev) {
    if (!mWin || ev.xany.window != mWin) return false;
    switch (ev.type) {
      case ConfigureNotify:
        //writeln("icon configure: wdt=", ev.xconfigure.width, "; hgt=", ev.xconfigure.height);
        mWidth = ev.xconfigure.width;
        mHeight = ev.xconfigure.height;
        break;
      case Expose:
        //writeln("icon expose!");
        paintImage();
        break;
      case DestroyNotify:
        break;
      case EnterNotify:
        //writeln("pointer enters!");
        if (onEnter !is null) try { onEnter(this); } catch (Exception) {}
        break;
      case LeaveNotify:
        //writeln("pointer leaves!");
        if (onLeave !is null) try { onLeave(this); } catch (Exception) {}
        break;
      case ButtonPress:
        //writeln("button down=", ev.xbutton.button);
        switch (ev.xbutton.button) {
          case 1: if (onLButtonDown !is null) try { onLButtonDown(this); } catch (Exception) {} break;
          case 2: if (onMButtonDown !is null) try { onMButtonDown(this); } catch (Exception) {} break;
          case 3: if (onRButtonDown !is null) try { onRButtonDown(this); } catch (Exception) {} break;
          default:
        }
        break;
      case ButtonRelease:
        //writeln("button up=", ev.xbutton.button);
        switch (ev.xbutton.button) {
          case 1: if (onLButtonUp !is null) try { onLButtonUp(this); } catch (Exception) {} break;
          case 2: if (onMButtonUp !is null) try { onMButtonUp(this); } catch (Exception) {} break;
          case 3: if (onRButtonUp !is null) try { onRButtonUp(this); } catch (Exception) {} break;
          default:
        }
        break;
      default:
    }
    return true;
  }
}
