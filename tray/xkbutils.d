/* coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://sam.zoy.org/wtfpl/COPYING for more details.
 */
module tray.xkbutils;

import core.stdc.config : c_long, c_ulong;
import core.stdc.string : memset;
import iv.x11;


////////////////////////////////////////////////////////////////////////////////
/*
void main () {
  import iv.vfs.io;
  auto dpy = XOpenDisplay(null);
  assert(dpy !is null);
  writeln("layouts: ", dpy.layoutCount);
  writeln("active layout: ", dpy.activeLayout);
  foreach (string n; dpy.layoutNames) writeln("  <", n, ">");
}
*/


////////////////////////////////////////////////////////////////////////////////
// get current layout
int activeLayout (Display* dpy) {
  XkbStateRec state = void;
  memset(&state, 0, state.sizeof);
  XkbGetState(dpy, XkbUseCoreKbd, &state);
  return cast(int)state.group;
}


// set current layout
void setActiveLayout (Display* dpy, int group) {
  XkbStateRec state = void;
  memset(&state, 0, state.sizeof);
  XkbLockGroup(dpy, XkbUseCoreKbd, group);
  // the following call is necessary
  XkbGetState(dpy, XkbUseCoreKbd, &state);
}


// get number of available layout names
int layoutCount (Display* dpy) {
  XkbDescRec desc = void;
  memset(&desc, 0, desc.sizeof);
  desc.device_spec = XkbUseCoreKbd;
  XkbGetControls(dpy, XkbGroupsWrapMask, &desc);
  return cast(int)desc.ctrls.num_groups;
}


// get available layout names
string[] layoutNames (Display* dpy) {
  import core.stdc.stdlib : malloc, free;
  XkbDescRec desc = void;
  memset(&desc, 0, desc.sizeof);
  desc.device_spec = XkbUseCoreKbd;
  XkbGetControls(dpy, XkbGroupsWrapMask, &desc);
  XkbGetNames(dpy, XkbGroupNamesMask, &desc);
  auto cnt = cast(int)desc.ctrls.num_groups;
  if (cnt == 0) return null;
  char** names = cast(char**)malloc((char*).sizeof*cnt);
  if (names is null) return null;
  scope(exit) free(names);
  string[] res;
  XGetAtomNames(dpy, desc.names.groups.ptr, cnt, names);
  scope(exit) for (; cnt > 0; --cnt) if (names[cnt-1] !is null) XFree(names[cnt-1]);
  foreach (immutable idx; 0..cnt) {
    if (names[idx] is null) {
      res ~= null;
    } else {
      import std.string : fromStringz;
      res ~= names[idx].fromStringz.idup;
    }
  }
  XkbFreeControls(&desc, XkbGroupsWrapMask, True);
  XkbFreeNames(&desc, XkbGroupNamesMask, True);
  return res;
}


////////////////////////////////////////////////////////////////////////////////
private nothrow @nogc:
extern(C):

enum XkbUseCoreKbd = 0x0100;
enum XkbGroupsWrapMask = 1<<27;
enum XkbGroupNamesMask = 1<<12;
enum XkbKeyNameLength = 4;
enum XkbNumVirtualMods = 16;
enum XkbNumIndicators = 32;
enum XkbNumKbdGroups = 4;

enum XkbMinLegalKeyCode = 8;
enum XkbMaxLegalKeyCode = 255;
enum XkbMaxKeyCount = (XkbMaxLegalKeyCode-XkbMinLegalKeyCode+1);
enum XkbPerKeyBitArraySize = ((XkbMaxLegalKeyCode+1)/8);


struct XkbStateRec {
  ubyte group;
  ubyte locked_group;
  ushort base_group;
  ushort latched_group;
  ubyte mods;
  ubyte base_mods;
  ubyte latched_mods;
  ubyte locked_mods;
  ubyte compat_state;
  ubyte grab_mods;
  ubyte compat_grab_mods;
  ubyte lookup_mods;
  ubyte compat_lookup_mods;
  ushort ptr_buttons;
}

struct XkbModsRec {
  ubyte mask; // effective mods
  ubyte real_mods;
  ushort vmods;
}

struct XkbControls {
  ubyte mk_dflt_btn;
  ubyte num_groups;
  ubyte groups_wrap;
  XkbModsRec internal;
  XkbModsRec ignore_lock;
  uint enabled_ctrls;
  ushort repeat_delay;
  ushort repeat_interval;
  ushort slow_keys_delay;
  ushort debounce_delay;
  ushort mk_delay;
  ushort mk_interval;
  ushort mk_time_to_max;
  ushort mk_max_speed;
  short mk_curve;
  ushort ax_options;
  ushort ax_timeout;
  ushort axt_opts_mask;
  ushort axt_opts_values;
  uint axt_ctrls_mask;
  uint axt_ctrls_values;
  ubyte[XkbPerKeyBitArraySize] per_key_repeat;
}

struct XkbKeyNameRec {
  char[XkbKeyNameLength] name;
}

struct XkbKeyAliasRec {
  char[XkbKeyNameLength] real_;
  char[XkbKeyNameLength] alias_;
}

struct XkbNames {
  Atom keycodes;
  Atom geometry;
  Atom symbols;
  Atom types;
  Atom compat;
  Atom[XkbNumVirtualMods] vmods;
  Atom[XkbNumIndicators] indicators;
  Atom[XkbNumKbdGroups] groups;
  /* keys is an array of (xkb->max_key_code + 1) XkbKeyNameRec entries */
  XkbKeyNameRec* keys;
  /* key_aliases is an array of num_key_aliases XkbKeyAliasRec entries */
  XkbKeyAliasRec* key_aliases;
  /* radio_groups is an array of num_rg Atoms */
  Atom* radio_groups;
  Atom phys_symbols;

  /* num_keys seems to be unused in libX11 */
  ubyte num_keys;
  ubyte num_key_aliases;
  ushort num_rg;
}

/+
typedef struct _XkbServerMapRec {
  /* acts is an array of XkbActions structs, with size_acts entries
     allocated, and num_acts entries used. */
  unsigned short     num_acts;
  unsigned short     size_acts;
  XkbAction    *acts;

  /* behaviors, key_acts, explicit, & vmodmap are all arrays with
     (xkb.max_key_code + 1) entries allocated for each. */
  XkbBehavior    *behaviors;
  unsigned short    *key_acts;
#if defined(__cplusplus) || defined(c_plusplus)
  /* explicit is a C++ reserved word */
  unsigned char    *c_explicit;
#else
  unsigned char    *explicit;
#endif
  unsigned char     vmods[XkbNumVirtualMods];
  unsigned short    *vmodmap;
} XkbServerMapRec, *XkbServerMapPtr;
+/

struct XkbServerMap {}
struct XkbClientMap {}
struct XkbIndicator {}
struct XkbCompatMap {}
struct XkbGeometry {}

struct XkbDescRec {
  Display* dpy;
  ushort flags;
  ushort device_spec;
  KeyCode min_key_code;
  KeyCode max_key_code;

  XkbControls* ctrls;
  XkbServerMap* server;
  XkbClientMap* map;
  XkbIndicator* indicators;
  XkbNames* names;
  XkbCompatMap* compat;
  XkbGeometry* geom;
}


Status XkbGetState (Display* dpy, uint deviceSpec, XkbStateRec* rtrnState);
Bool XkbLockGroup (Display* dpy, uint deviceSpec, uint group);
Status XkbGetControls (Display* dpy, c_ulong which, XkbDescRec* desc);
Status  XkbGetNames (Display* dpy, uint which, XkbDescRec* desc);
void XkbFreeControls (XkbDescRec* xkb, uint which, Bool freeMap);
void XkbFreeNames (XkbDescRec* xkb, uint which, Bool freeMap);
