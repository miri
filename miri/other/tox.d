/* Invisible Vector IRC client
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
module miri.other.tox;
private:

import core.thread;
import core.time;

import std.socket;
import std.uuid;

import iv.eventbus;
import iv.egtui.txtser;
import iv.tox;
import iv.vfs;

import miri.types;
import miri.cfg;
import miri.log;
import miri.util;
import miri.netio;


/*
following Unix spirit of "everything is file", Tox integration is designed
as "everything is IRC". all Tox connections lives in their own threads, and
just sends network packets when something happens. all network i/o are just
IRC commands, so other parts of Miri sees "just another IRC server".

Tox server will automatically create several channels:
  #online
    people who are currently online
  #offline
    people who are currently offline
  #all
    all people
  #authreq
    pending friend requests

also note that after sending friend request dest user becomes unnamed friend.
*/


// ///////////////////////////////////////////////////////////////////////// //
shared static this () { registerTox(); }

void registerTox () {
  if (TOX_VERSION_IS_ABI_COMPATIBLE()) {
    netRegisterProto("tox:", (string address) => newNetIOConnection(address));
  }
}


// ///////////////////////////////////////////////////////////////////////// //
Socket newNetIOConnection (string address) {
  logwritefln("creating new Tox connection [%s]", address);
  auto ti = new ToxImpl(address);
  try {
    auto sk = ti.createSocketPipe();
    if (sk !is null) {
      // start thread
      (new Thread(() {
        ubyte[TOX_ADDRESS_SIZE] toxid;
        tox_self_get_address(ti.tox, toxid.ptr);
        sysmsgf("started thread for Tox:%s %s", ti.srvalias, tox_hex(toxid));
        try {
          ti.bootstrap();
          ti.eventLoop();
        } catch (Throwable e) {
          if (cast(Exception)e) {
            logwritefln("\nTOX[%s] EXCEPTION: %s\n\n", ti.srvalias, e.toString());
            ti.dropNetIOConnection();
            if (ti.tox !is null) { tox_kill(ti.tox); ti.tox = null; }
          } else {
            // here, we are dead and fucked (the exact order doesn't matter)
            import core.stdc.stdlib : abort;
            import core.stdc.stdio : fprintf, stderr;
            import core.memory : GC;
            GC.disable(); // yeah
            thread_suspendAll(); // stop right here, you criminal scum!
            auto s = e.toString();
            fprintf(stderr, "\n=== FATAL ===\n%.*s\n", cast(uint)s.length, s.ptr);
            abort(); // die, you bitch!
          }
        }
        sysmsgf("exiting thread of Tox:%s %s", ti.srvalias, tox_hex(toxid));
      })).start;
    }
    return sk;
  } catch (Exception e) {
    ti.dropNetIOConnection();
    if (ti.tox !is null) { tox_kill(ti.tox); ti.tox = null; }
  }
  return null;
}


// ///////////////////////////////////////////////////////////////////////// //
class ToxImpl {
  static {
    uint unixnow () {
      import core.time;
      import std.datetime;
      return cast(uint)Clock.currTime.toUnixTime;
    }
  }

  string srvalias;
  Socket sock;
  SocketSet setRead, setWrite;
  LineCollector inbuf;
  char[8192] tmpbuf;
  ubyte[] wrdata;
  Tox* tox;
  bool isOnline;
  string myircnick = "guest";
  bool doSaveToxData;
  bool doRefreshNicks;
  bool gotNewReceipt;

  static struct Msg {
    ubyte[TOX_PUBLIC_KEY_SIZE] dest;
    string msg;
    uint unixtime;
    uint msgid;
    bool gotreceipt;
    bool offlineMsgWarningShown;
    uint unixtimeNextResend;
  }

  static struct FriendReq {
    ubyte[TOX_PUBLIC_KEY_SIZE] src;
    string msg;
    uint unixtime;
  }

  static struct FileSend {
    VFile fl;
    //TOX_FILE_KIND kind;
    uint flnum;
    string destname;
    string diskname;
    ubyte[TOX_HASH_LENGTH] hash;
    ubyte[TOX_PUBLIC_KEY_SIZE] dest;
    bool active = false;
  }

  static struct FileRecv {
    VFile fl;
    //TOX_FILE_KIND kind;
    uint flnum;
    string srcname;
    string diskname;
    //ubyte[TOX_HASH_LENGTH] hash;
    ulong size;
    ubyte[TOX_PUBLIC_KEY_SIZE] src;
  }

  static struct ChunkToSend {
    uint frnum;
    uint flnum;
    ulong pos;
    usize len;
  }

  static struct RejectEvent {
    uint frnum;
    uint flnum;
  }

  static class Friend {
    uint frnum;
    string nick;
    string ircnick;
    string ircnicklo; // lowercased
    ubyte[TOX_PUBLIC_KEY_SIZE] pubkey;
    TOX_USER_STATUS status;
    bool online;
    bool away;
  }

  Friend[string] friends; // keyed by ircnicklo
  Msg[] messages;
  FriendReq[] authreqs;
  FileSend[] fsendlist;
  FileRecv[] frecvlist;
  ChunkToSend[] fschunks;
  ubyte[] sendbuf;
  RejectEvent[] frejects;
  uint unixNextPing;
  bool forceOnline; // go online when offline set
  bool restoreOnline; // true: set status to online

  // loading and saving various data

  string diskResPath(T : cstring) (T fname) {
    import std.path;
    static if (is(T == typeof(null))) {
      string fn = null;
    } else static if (is(T == string)) {
      string fn = fname;
    } else {
      string fn = fname.idup;
    }
    if (fn.length) {
      return buildPath(configDir, ":tox:", normStr2FName(srvalias), fn);
    } else {
      return buildPath(configDir, ":tox:", normStr2FName(srvalias));
    }
  }

  void loadUnsentMessages () {
    import std.file : exists;
    messages = null;
    auto fname = diskResPath("unsent.miri");
    if (!fname.exists) return;
    try {
      VFile(fname).txtunser(messages);
      foreach (ref Msg msg; messages) msg.offlineMsgWarningShown = false;
    } catch (Exception e) {
    }
  }

  void saveUnsentMessages () {
    auto fname = diskResPath("unsent.miri");
    try {
      VFile(fname, "w").txtser(messages);
    } catch (Exception e) {
    }
  }

  void loadAuthRequests () {
    import std.file : exists;
    authreqs = null;
    auto fname = diskResPath("authreqs.miri");
    if (!fname.exists) return;
    try {
      VFile(fname).txtunser(authreqs);
    } catch (Exception e) {
    }
  }

  void saveAuthRequests () {
    auto fname = diskResPath("authreqs.miri");
    try {
      VFile(fname, "w").txtser(authreqs);
    } catch (Exception e) {
    }
  }

  // returns `null` if there is no such file or throws on error
  ubyte[] loadToxCoreData () {
    import std.file : exists;
    auto fname = diskResPath("toxcore.tox");
    if (!fname.exists) return null;
    logwritefln("trying to open '%s'", fname);
    auto fl = VFile(fname);
    logwritefln("opened '%s'", fname);
    if (fl.size > int.max/8) throw new Exception("toxcore data corrupted");
    auto res = new ubyte[](cast(int)fl.size);
    fl.rawReadExact(res[]);
    logwritefln("read %s bytes from '%s'", res.length, fname);
    return res[];
  }

  bool saveToxCoreData () {
    import core.stdc.stdio : FILE, fopen, fclose, fwrite, rename;
    import core.stdc.stdlib : malloc, free;
    import std.file : mkdirRecurse;
    import std.path : dirName;

    if (tox is null) return false;

    auto size = tox_get_savedata_size(tox);
    if (size > int.max/8) return false; //throw new Exception("save data too big");

    char* savedata = cast(char*)malloc(size);
    if (savedata is null) return false;
    scope(exit) if (savedata !is null) free(savedata);

    tox_get_savedata(tox, savedata);

    auto ofname = diskResPath("toxcore.tox");
    auto ofnametmp = ofname~".tmp";
    try {
      import std.string : toStringz;
      mkdirRecurse(ofnametmp.dirName);
      auto fo = VFile(ofnametmp, "w");
      fo.rawWriteExact(savedata[0..size]);
      if (rename(ofnametmp.toStringz, ofname.toStringz) != 0) throw new Exception("error renaming savedata file");
    } catch (Exception e) {
      return false;
    }
    return true;
  }

  ToxBootstrapServer[] loadBootNodes () {
    import std.path;
    auto bfname = buildPath(configDir, ":tox:", ".bootstrap.rc");
    ToxBootstrapServer[] bootnodes = null;
    synchronized(ToxImpl.classinfo) {
      try {
        VFile(bfname).txtunser(bootnodes);
        if (bootnodes.length > 0) return bootnodes;
      } catch (Exception e) {}
    }
    // try to download
    bootnodes = tox_download_bootstrap_list();
    if (bootnodes.length > 0) {
      synchronized(ToxImpl.classinfo) {
        try {
          VFile(bfname, "w").txtser(bootnodes);
        } catch (Exception e) {}
      }
    }
    return bootnodes;
  }

  // friendlist management

  // set (new) nick for friend, create IRC nick
  // returns `true` if irc nick was changed (and so `friends` AA)
  bool friendSetNick (Friend fr, cstring newnick) {
    if (fr is null) return false; // just in case
    if (newnick.length == 0) newnick = "unnamed";
    if (fr.nick == newnick) return false;
    fr.nick = newnick.idup;
    string nick = fr.nick; // irc nick
    bool ishex = true;
    {
      Recoder rc;
      auto ik = rc.uncodeBuf(nick);
      foreach (ref ch; ik) {
        if (!ch.ircisalnum) ch = '_';
        if (!ch.ircisxdigit) ishex = false;
      }
      nick = ik.idup;
    }
    // if this is pure hex nick which can be interpreted as public key, safeguard it
    if (ishex && nick.length == TOX_PUBLIC_KEY_SIZE*2) nick = "_"~nick;
    // transform nick, so it will be unique
    auto orignick = nick;
    int sfx = 0;
    for (;;) {
      import std.format : format;
      bool valid = !ircStrEquCI(nick, myircnick);
      if (valid) {
        foreach (Friend xfr; friends.byValue) {
          if (xfr !is fr && xfr.ircnick.ircStrEquCI(nick)) { valid = false; break; }
        }
      }
      if (valid) break;
      nick = "%s_%s".format(orignick, sfx++);
    }
    fr.ircnick = nick;
    // create lowercase nick
    auto lcn = new char[](nick.length);
    foreach (immutable cidx, char ch; nick) lcn[cidx] = ch.irc2lower;
    // remove old-nicked from list, add new-nicked
    if (fr.ircnicklo != lcn) {
      friends.remove(fr.ircnicklo);
      fr.ircnicklo = cast(string)lcn; // safe cast
      friends[fr.ircnicklo] = fr;
      return true;
    }
    return false;
  }

  void refreshNicks (bool sendmsg=true) {
    // returns `true` if irc nick was changed (and so `friends` AA)
    bool fixNick (Friend fr) {
      bool res = false;
      assert(fr !is null);
      auto namesz = tox_friend_get_name_size(tox, fr.frnum, null);
      string nick;
      if (namesz >= int.max/8) {
        // invalid (SIZE_MAX)
        nick = "invalid";
      } else if (namesz == 0) {
        nick = "unnamed";
      } else {
        auto restr = new char[](namesz);
        if (tox_friend_get_name(tox, fr.frnum, restr.ptr, null)) {
          nick = cast(string)restr; // safe cast
        } else {
          nick = "error";
        }
      }
      if (fr.nick != nick) {
        auto oik = fr.ircnick;
        res = friendSetNick(fr, nick);
        if (sendmsg) {
          if (oik.length && !oik.ircStrEquCI(fr.ircnick)) {
            sendcmdf(oik, "K8USERNOTICE %s :%s changed nick to %s", myircnick, oik, fr.ircnick);
            sendcmdf(oik, "NICK :%s", fr.ircnick);
            doSaveToxData = true; // save new nick
          } else if (!oik.length) {
            sendcmdf(fr.ircnick, "JOIN #all");
            sendcmdf(fr.ircnick, "JOIN #offline");
            sendcmdf(fr.ircnick, "K8USERNOTICE %s :%s is offline", myircnick, fr.ircnick);
          }
        }
      }
      if (removeAuthByKey(fr.pubkey[])) {
        if (sendmsg) sendcmdf(fr.pubkey[].tox_hex, "PART #authreqs");
        saveAuthRequests();
      }
      return res;
    }

    void fixStatuses (Friend fr) {
      if (fr is null) return;
      // fix status
      //auto online = (tox_friend_get_connection_status(tox, fr.frnum, null) != TOX_CONNECTION_NONE);
      bool online = (toxIdiotsCheckOnline(fr.pubkey) > 0);
      if (!isOnline) online = false;
      if (fr.online != online) {
        // move to the appropriate group
        if (sendmsg) {
          sendcmdf(fr.ircnick, "PART #o%sline", (fr.online ? "n" : "ff"));
          sendcmdf(fr.ircnick, "JOIN #o%sline", (online ? "n" : "ff"));
          sendcmdf(fr.ircnick, "K8USERNOTICE %s :%s is o%sline", myircnick, fr.ircnick, (online ? "n" :"ff"));
        }
        fr.online = online;
      }
      // fix away
      //auto away = (tox_friend_get_status(tox, fr.frnum, null) != TOX_USER_STATUS_NONE);
      bool away = (toxIdiotsCheckAway(fr.pubkey) > 0);
      if (!isOnline) away = false;
      if (fr.away != away) {
        //if (sendmsg) sendcmdf(fr.ircnick, "AWAY%s", (away ? " :gone away" : ""));
        logwritefln("TOX:%s: %s away changed: %s", myircnick, fr.ircnick, away);
        if (sendmsg) sendcmdf(null, "MODE %s :%sa", fr.ircnick, (away ? "+" : "-"));
        fr.away = away;
      }
    }

    doRefreshNicks = false;
    if (tox is null || sock is null) return;
    // remove dead friends, renumber alive
    for (;;) {
      bool again = false;
      foreach (Friend fr; friends.byValue) {
        auto tnum = tox_friend_by_public_key(tox, fr.pubkey.ptr, null);
        if (tnum == uint.max) {
          again = true;
          // remove this friend
          purgeFileTransfers(fr);
          if (sendmsg) {
            sendcmdf(fr.ircnick, "K8USERNOTICE %s :%s quited", myircnick, fr.ircnick);
            sendcmdf(fr.ircnick, "QUIT :removed");
          }
          if (removeAuthByKey(fr.pubkey[])) saveAuthRequests();
          friends.remove(fr.ircnicklo);
          break;
        }
        fr.frnum = tnum; // fix number
        if (fixNick(fr)) {
          // AA was changed
          again = true;
          break;
        }
        fixStatuses(fr);
      }
      if (!again) break;
    }
    // add new friends
    foreach (immutable idx; 0..tox_self_get_friend_list_size(tox)) {
      if (!tox_friend_exists(tox, idx)) continue;
      ubyte[TOX_PUBLIC_KEY_SIZE] pk;
      tox_friend_get_public_key(tox, idx, pk.ptr, null);
      bool found = false;
      foreach (Friend fr; friends.byValue) if (fr.pubkey[] == pk[]) { found = true; break; }
      if (!found) {
        auto fr = new Friend();
        fr.frnum = idx;
        fr.pubkey[] = pk[];
        fixNick(fr);
        logwritefln("%s ID: %s", fr.ircnick, fr.pubkey.tox_hex);
        fixStatuses(fr);
      }
    }
  }

  Friend findFriendByIRCNick (cstring ircnick) {
    foreach (Friend fr; friends.byValue) if (fr.ircnick.ircStrEquCI(ircnick)) return fr;
    return null;
  }

  Friend findFriendByNum (uint frnum) {
    foreach (Friend fr; friends.byValue) if (fr.frnum == frnum) return fr;
    return null;
  }

  Friend findFriendByKey (const(ubyte)[] pk) {
    if (pk.length > TOX_PUBLIC_KEY_SIZE) pk = pk[0..TOX_PUBLIC_KEY_SIZE];
    if (pk.length != TOX_PUBLIC_KEY_SIZE) return null;
    foreach (Friend fr; friends.byValue) if (fr.pubkey[] == pk[]) return fr;
    return null;
  }

  // auth reject events management

  void addRejectEvent (uint frnum, uint flnum) {
    foreach (ref RejectEvent rej; frejects) {
      if (rej.frnum == frnum && rej.flnum == flnum) return;
    }
    frejects.arrayAppend(RejectEvent(frnum, flnum));
  }

  void processRejectEvents () {
    foreach (ref RejectEvent rej; frejects) {
      tox_file_control(tox, rej.frnum, rej.flnum, TOX_FILE_CONTROL_CANCEL, null);
    }
    if (frejects.length) {
      frejects.length = 0;
      frejects.assumeSafeAppend;
    }
  }

  // file send/recv management

  void processFileSending () {
    foreach (ref ChunkToSend chunk; fschunks) {
      if (auto fs = findSendByNums(chunk.frnum, chunk.flnum)) {
        if (sendbuf.length < chunk.len) sendbuf.length = chunk.len;
        try {
          fs.fl.seek(chunk.pos);
          fs.fl.rawReadExact(sendbuf[0..chunk.len]);
          if (!tox_file_send_chunk(tox, chunk.frnum, chunk.flnum, chunk.pos, sendbuf.ptr, chunk.len, null)) {
            abortSendByNums(chunk.frnum, chunk.flnum);
          }
        } catch (Exception) {
          abortSendByNums(chunk.frnum, chunk.flnum);
        }
      }
    }
  }

  bool abortRecvByNums (uint frnum, uint flnum) {
    if (auto fr = findFriendByNum(frnum)) {
      foreach (immutable idx, ref FileRecv fs; frecvlist) {
        if (fs.flnum == flnum && fs.src[] == fr.pubkey[]) {
          // i found her!
          fs.fl.close();
          try { import std.file : remove; remove(fs.diskname); } catch (Exception) {}
          tox_file_control(tox, fr.frnum, fs.flnum, TOX_FILE_CONTROL_CANCEL, null);
          foreach (immutable c; idx+1..frecvlist.length) frecvlist[c-1] = frecvlist[c];
          frecvlist.length -= 1;
          frecvlist.assumeSafeAppend;
          return true;
        }
      }
    }
    return false;
  }

  bool abortSendByNums (uint frnum, uint flnum) {
    if (auto fr = findFriendByNum(frnum)) {
      foreach (immutable idx, ref FileSend fs; fsendlist) {
        if (fs.flnum == flnum && fs.dest[] == fr.pubkey[]) {
          // i found her!
          fs.fl.close();
          tox_file_control(tox, fr.frnum, fs.flnum, TOX_FILE_CONTROL_CANCEL, null);
          foreach (immutable c; idx+1..fsendlist.length) fsendlist[c-1] = fsendlist[c];
          fsendlist.length -= 1;
          fsendlist.assumeSafeAppend;
          return true;
        }
      }
    }
    return false;
  }

  bool pauseResumeSendByNums (uint frnum, uint flnum, bool doresume) {
    if (auto fr = findFriendByNum(frnum)) {
      foreach (immutable idx, ref FileSend fs; fsendlist) {
        if (fs.flnum == flnum && fs.dest[] == fr.pubkey[]) {
          fs.active = doresume;
          return true;
        }
      }
    }
    return false;
  }

  bool pauseSendByNums (uint frnum, uint flnum) { return pauseResumeSendByNums(frnum, flnum, false); }
  bool resumeSendByNums (uint frnum, uint flnum) { return pauseResumeSendByNums(frnum, flnum, true); }

  // can return `null`
  FileRecv* newRecvWithNums (uint frnum, uint flnum, string srcname, string diskname, ulong size) {
    if (auto fr = findFriendByNum(frnum)) {
      foreach (ref FileRecv fs; frecvlist) {
        if (fs.flnum == flnum && fs.src[] == fr.pubkey[]) return &fs;
      }
      FileRecv fs;
      fs.flnum = flnum;
      fs.src[] = fr.pubkey[];
      fs.srcname = srcname;
      fs.diskname = diskname;
      fs.size = size;
      try {
        fs.fl = VFile(diskname, "w");
        frecvlist ~= fs;
        return &frecvlist[$-1];
      } catch (Exception) {}
    }
    return null;
  }

  FileSend* findSendByFriend (Friend fr) {
    if (fr is null) return null;
    foreach (ref FileSend fs; fsendlist) {
      if (fs.dest[] == fr.pubkey[]) return &fs;
    }
    return null;
  }

  FileRecv* findRecvByFriend (Friend fr) {
    if (fr is null) return null;
    foreach (ref FileRecv fs; frecvlist) {
      if (fs.src[] == fr.pubkey[]) return &fs;
    }
    return null;
  }

  FileSend* findSendByNums (uint frnum, uint flnum) {
    if (auto fr = findFriendByNum(frnum)) {
      foreach (ref FileSend fs; fsendlist) {
        if (fs.flnum == flnum && fs.dest[] == fr.pubkey[]) return &fs;
      }
    }
    return null;
  }

  FileRecv* findRecvByNums (uint frnum, uint flnum) {
    if (auto fr = findFriendByNum(frnum)) {
      foreach (ref FileRecv fs; frecvlist) {
        if (fs.flnum == flnum && fs.src[] == fr.pubkey[]) return &fs;
      }
    }
    return null;
  }

  void purgeFileTransfers (Friend fr) {
    if (fr is null) return;
    // remove sends
    for (usize idx = 0; idx < fsendlist.length; ++idx) {
      if (fsendlist[idx].dest[] == fr.pubkey[]) {
        fsendlist[idx].fl.close();
        foreach (immutable c; idx+1..fsendlist.length) fsendlist[c-1] = fsendlist[c];
        fsendlist.length -= 1;
        fsendlist.assumeSafeAppend;
        --idx;
      }
    }
    // remove receives
    for (usize idx = 0; idx < frecvlist.length; ++idx) {
      if (frecvlist[idx].src[] == fr.pubkey[]) {
        frecvlist[idx].fl.close();
        try { import std.file : remove; remove(frecvlist[idx].diskname); } catch (Exception) {}
        foreach (immutable c; idx+1..frecvlist.length) frecvlist[c-1] = frecvlist[c];
        frecvlist.length -= 1;
        frecvlist.assumeSafeAppend;
        --idx;
      }
    }
  }

  // sent auth requests management

  FriendReq* findAuthByKey (const(ubyte)[] pk) {
    if (pk.length > TOX_PUBLIC_KEY_SIZE) pk = pk[0..TOX_PUBLIC_KEY_SIZE];
    if (pk.length != TOX_PUBLIC_KEY_SIZE) return null;
    foreach (immutable idx, ref FriendReq req; authreqs) if (req.src[] == pk[]) return &req;
    return null;
  }

  FriendReq* findAuthByNick (cstring nick) {
    foreach (immutable idx, ref FriendReq req; authreqs) if (req.src[].tox_hex.ircStrEquCI(nick)) return &req;
    return null;
  }

  bool removeAuthByKey (const(ubyte)[] pk) {
    if (pk.length > TOX_PUBLIC_KEY_SIZE) pk = pk[0..TOX_PUBLIC_KEY_SIZE];
    if (pk.length != TOX_PUBLIC_KEY_SIZE) return false;
    foreach (immutable idx, ref FriendReq req; authreqs) {
      if (req.src[] == pk[]) {
        foreach (immutable c; idx+1..authreqs.length) authreqs[c-1] = authreqs[c];
        authreqs.length -= 1;
        authreqs.assumeSafeAppend;
        return true;
      }
    }
    return false;
  }

  // low-level socket i/o

  // return `false` on error or on closed socket
  bool sockSend () {
    if (wrdata.length) {
      if (sock is null) return false;
      try {
        auto r = sock.send(wrdata[]);
        if (r == Socket.ERROR) return false;
        if (r == 0) return false;
        // sent some bytes
        assert(r <= wrdata.length);
        if (r < wrdata.length) {
          // partial, shift data
          import core.stdc.string : memmove;
          memmove(wrdata.ptr, wrdata.ptr+r, wrdata.length-r);
        }
        wrdata.length -= r;
        wrdata.assumeSafeAppend;
      } catch (Exception e) {
        return false;
      }
    }
    return true;
  }

  // return `false` on error or on closed socket
  bool sockRead () {
    if (sock is null) return false;
    try {
      auto r = sock.receive(tmpbuf[]);
      if (r == Socket.ERROR) return false;
      if (r == 0) return false;
      inbuf.onData(tmpbuf[0..r], (cstring line) { onLine(line); });
    } catch (Exception e) {
      return false;
    }
    return true;
  }

  // middle and high-level socket i/o
  // send
  //  ":<srcnick>!tox.<srvalias> ..."
  // or
  //  ":tox.<srvalias> ..." if srcnick is null
  void sendcmdf(A...) (cstring srcnick, cstring fmt, A args) {
    import std.format : formattedWrite;
    char[] sbuf;
    static struct Writer {
      ToxImpl srv;
      char[]* sbuf;
      void put (cstring s...) {
        char[8] buf = void;
        foreach (char ch; s) {
          //auto bb = srv.inbuf.cp.encodeCharInPlace(buf[], ch);
          import iv.utfutil;
          char[8] ubuf = void;
          int len = utf8Encode(ubuf[], koi2uni(ch));
          if (len < 1) { ubuf[0] = '?'; len = 1; }
          //logwritefln("  Tox:%s <%s:%s>", srv.srvalias, ch, bb);
          //(*sbuf) ~= ubuf[0..len];
          foreach (char uch; ubuf[0..len]) (*sbuf).arrayAppend(uch);
        }
      }
    }
    auto wr = Writer(this, &sbuf);
    if (srcnick) {
      formattedWrite(wr, ":%s!tox.%s ", srcnick, srvalias);
    } else {
      formattedWrite(wr, ":tox.%s ", srvalias);
    }
    formattedWrite(wr, fmt, args);
    //sbuf ~= "\r\n";
    sbuf.arrayAppend('\r');
    sbuf.arrayAppend('\n');
    foreach (ubyte bt; cast(const(ubyte)[])sbuf) wrdata.arrayAppend(bt);
    delete sbuf;
  }

  void bootstrap () {
    logwritefln("Tox[%s]: loading bootstrap nodes...", srvalias);
    auto nodes = loadBootNodes();
    logwritefln("Tox[%s]: %s nodes loaded", srvalias, nodes.length);
    if (nodes.length == 0) {
      dropNetIOConnection();
      return;
    }
    foreach (const ref ToxBootstrapServer srv; nodes) {
      if (sock is null) break;
      if (srv.ipv4.length < 2) continue;
      assert(srv.ipv4[$-1] == 0);
      logwritefln("node ip: %s:%u (maintainer: %s)", srv.ipv4[0..$-1], srv.port, srv.maintainer);
      tox_bootstrap(tox, srv.ipv4.ptr, srv.port, srv.pubkey.ptr, null);
      tox_iterate(tox, cast(void*)this);
    }
    logwritefln("Tox[%s]: %s nodes added", srvalias, nodes.length);
  }

  this (string aalias) {
    srvalias = aalias;
    setRead = new SocketSet();
    setWrite = new SocketSet();
    //inbuf.norecoding = true;
    inbuf.utfucked = true;

    TOX_ERR_OPTIONS_NEW xerr;
    auto options = tox_options_new(&xerr);
    if (options is null) throw new Exception("can't create Tox options");
    scope(exit) tox_options_free(options);

    tox_options_default(options);
    tox_options_set_ipv6_enabled(options, false);
    tox_options_set_udp_enabled(options, true);

    auto data = loadToxCoreData();
    scope(exit) delete data;
    if (data.length) {
      tox_options_set_savedata_type(options, TOX_SAVEDATA_TYPE_TOX_SAVE);
      tox_options_set_savedata_data(options, data.ptr, cast(int)data.length);
    }
    tox = tox_new(options, null);
    if (tox is null) throw new Exception("can't create Tox instance");
    tox_self_set_status_message(tox, null, 0, null);
    tox_self_set_status(tox, TOX_USER_STATUS_NONE);
    loadAuthRequests();
    refreshNicks(false); // don't send any messages
    loadUnsentMessages();
    if (data.length == 0) saveToxCoreData();
  }

  ~this () {
    if (tox !is null) { tox_kill(tox); tox = null; }
  }

  Socket createSocketPipe () {
    if (sock !is null || tox is null) throw new Exception("something is very wrong");

    logwritefln("creating socked pipe for Tox:%s", srvalias);

    // now do a trick with unix domain sockets
    auto sks = new Socket(AddressFamily.UNIX, SocketType.STREAM);
    logwritefln("Tox:%s: created server socket", srvalias);
    sks.setSocketOptions();
    sks.setTimeouts(5); // 5 seconds to i/o
    logwritefln("Tox:%s: created server options set", srvalias);

    auto skc = new Socket(AddressFamily.UNIX, SocketType.STREAM);
    logwritefln("Tox:%s: created client socket", srvalias);
    skc.setSocketOptions();
    skc.setTimeouts(5); // 5 seconds to i/o
    logwritefln("Tox:%s: client socket options set", srvalias);

    string udname = "/k8/miri/tox/"~randomUUID.toString;
    logwritefln("Tox:%s: udname: <%s>", srvalias, udname);

    sks.bind(new UnixAddressEx(udname));
    sks.listen(1);
    logwritefln("Tox:%s: server socket bound", srvalias);

    skc.connect(new UnixAddressEx(udname));
    logwritefln("Tox:%s: client socket connecting", srvalias);

    // wait until both sockets are ready
    for (;;) {
      setRead.reset();
      setRead.add(sks);
      setWrite.reset();
      setWrite.add(skc);
      auto res = Socket.select(setRead, setWrite, null);
      if (res < 0) {
        import core.stdc.errno;
        if (errno != EINTR) {
          logwritefln("Tox:%s: select() error", srvalias);
          throw new Exception("something is very wrong");
        }
      }
      if (res == 0) {
        logwritefln("Tox:%s: select() error (timeout)", srvalias);
        throw new Exception("something is very wrong");
      }
      logwritefln("Tox:%s: select() res=%s", srvalias, res);
      if (res == 2) break; // both sockets are ready
    }

    // accept "connected" socket -- this is what i will pass to netio
    auto rsk = sks.accept;
    if (rsk is null) throw new Exception("something is very wrong");
    rsk.setSocketOptions();

    rsk.setTimeouts(60*5); // 5 minutes to i/o
    skc.setTimeouts(60*5); // 5 minutes to i/o

    // close accepting socket: i don't need it anymore
    sks.close();

    // and "client" socket is mine
    sock = skc;

    loadAuthRequests();
    refreshNicks(false); // don't send any messages
    loadUnsentMessages();

    this.connectListeners();
    forceOnline = true;

    tox_callback_self_connection_status(tox, &connectionCB);

    tox_callback_friend_connection_status(tox, &friendConnectionCB);
    tox_callback_friend_status(tox, &friendStatusCB);
    tox_callback_friend_name(tox, &friendNameCB);
    tox_callback_friend_request(tox, &friendReqCB);
    tox_callback_friend_message(tox, &friendMsgCB);
    tox_callback_friend_read_receipt(tox, &friendReceiptCB);

    tox_callback_file_recv(tox, &fileRecvCB);
    tox_callback_file_recv_control(tox, &fileRecvCtlCB);
    tox_callback_file_chunk_request(tox, &fileChunkReqCB);

    return rsk;
  }

  // toxcore callbacks

  static extern(C) void connectionCB (Tox* tox, TOX_CONNECTION status, void* udata) nothrow {
    import iv.rawtty;
    auto timp = *cast(ToxImpl*)&udata;
    if (timp is null || timp.tox is null || timp.sock is null) return;
    try {
      logwritefln("Tox:%s: connection status is %s", timp.srvalias, status);
      if (status == TOX_CONNECTION_NONE) {
        // disconnected
        if (!timp.forceOnline) {
          timp.dropNetIOConnection();
        } else {
          timp.restoreOnline = true;
          timp.isOnline = false;
        }
        return;
      }
      if (!timp.isOnline) timp.toxConnectionEstablished();
    } catch (Exception e) {
      try {
        logwritefln("\nTOX[%s] CB EXCEPTION: %s\n\n", timp.srvalias, e.toString);
        timp.dropNetIOConnection();
      } catch (Exception) {}
    }
  }

  static struct ToxAuthorsAreIdiots {
    uint frnum;
    ubyte[TOX_PUBLIC_KEY_SIZE] pubkey;
    bool online;
    bool away;
  }
  ToxAuthorsAreIdiots[] toxAuthorsAreIdiots;

  void fixFriendStatusFromCB (Tox* tox, in ref ubyte[TOX_PUBLIC_KEY_SIZE] apubkey, uint frnum, int online, int away) nothrow {
    ubyte[TOX_PUBLIC_KEY_SIZE] pubkey = apubkey[];
    if (frnum == uint.max) {
      int err = 0;
      frnum = tox_friend_by_public_key(tox, pubkey.ptr, &err);
      if (/*frnum == uint.max*/err) return;
    } else {
      if (!tox_friend_exists(tox, frnum)) return;
      int err = 0;
      if (!tox_friend_get_public_key(tox, frnum, pubkey.ptr, &err)) return;
      if (err) return;
    }
    foreach (ref ToxAuthorsAreIdiots fr; toxAuthorsAreIdiots) {
      if (fr.pubkey[] == pubkey[]) {
        fr.frnum = frnum;
        if (online >= 0) fr.online = (online > 0);
        if (away >= 0) fr.away = (away > 0);
        return;
      }
    }
    // add it
    ToxAuthorsAreIdiots fr;
    fr.frnum = frnum;
    fr.pubkey[] = pubkey[];
    fr.online = (fr.online > 0);
    fr.away = (fr.away > 0);
    toxAuthorsAreIdiots ~= fr;
  }

  int toxIdiotsCheckOnline (in ref ubyte[TOX_PUBLIC_KEY_SIZE] pubkey) nothrow @nogc {
    foreach (ref ToxAuthorsAreIdiots fr; toxAuthorsAreIdiots) if (fr.pubkey[] == pubkey[]) return (fr.online ? 1 : 0);
    return -1;
  }

  int toxIdiotsCheckAway (in ref ubyte[TOX_PUBLIC_KEY_SIZE] pubkey) nothrow @nogc {
    foreach (ref ToxAuthorsAreIdiots fr; toxAuthorsAreIdiots) if (fr.pubkey[] == pubkey[]) return (fr.away ? 1 : 0);
    return -1;
  }

  int toxIdiotsCheckOnline (uint frnum) nothrow @nogc {
    foreach (ref ToxAuthorsAreIdiots fr; toxAuthorsAreIdiots) if (fr.frnum == frnum) return (fr.online ? 1 : 0);
    return -1;
  }

  int toxIdiotsCheckAway (uint frnum) nothrow @nogc {
    foreach (ref ToxAuthorsAreIdiots fr; toxAuthorsAreIdiots) if (fr.frnum == frnum) return (fr.away ? 1 : 0);
    return -1;
  }

  static extern(C) void friendConnectionCB (Tox* tox, uint frnum, TOX_CONNECTION status, void* udata) nothrow {
    auto timp = *cast(ToxImpl*)&udata;
    if (timp is null || timp.tox is null || timp.sock is null) return;
    ubyte[TOX_PUBLIC_KEY_SIZE] apubkey = 0;
    timp.fixFriendStatusFromCB(tox, apubkey, frnum, (status == TOX_CONNECTION_NONE ? 0 : 1), -1);
    timp.doRefreshNicks = true;
  }

  static extern(C) void friendStatusCB (Tox* tox, uint frnum, TOX_USER_STATUS status, void* udata) nothrow {
    auto timp = *cast(ToxImpl*)&udata;
    if (timp is null || timp.tox is null || timp.sock is null) return;
    ubyte[TOX_PUBLIC_KEY_SIZE] apubkey = 0;
    timp.fixFriendStatusFromCB(tox, apubkey, frnum, 1, (status == TOX_USER_STATUS_NONE ? 0 : 1));
    timp.doRefreshNicks = true;
  }

  static extern(C) void friendNameCB (Tox* tox, uint frnum, const(char)* name, usize length, void* udata) nothrow {
    auto timp = *cast(ToxImpl*)&udata;
    if (timp is null || timp.tox is null || timp.sock is null) return;
    timp.doRefreshNicks = true;
  }

  static extern(C) void friendReqCB (Tox* tox, const(ubyte)* pk, const(char)* msg, usize msglen, void* udata) nothrow {
    auto timp = *cast(ToxImpl*)&udata;
    if (timp is null || timp.tox is null || timp.sock is null) return;
    try {
      timp.toxFriendReqest(pk[0..TOX_PUBLIC_KEY_SIZE], msg[0..msglen]);
    } catch (Exception e) {
      try {
        logwritefln("\nTOX[%s] CB EXCEPTION: %s\n\n", timp.srvalias, e.toString);
        timp.dropNetIOConnection();
      } catch (Exception) {}
    }
  }

  static extern(C) void friendMsgCB (Tox* tox, uint frnum, TOX_MESSAGE_TYPE type, const(char)* msg, usize msglen, void* udata) nothrow {
    auto timp = *cast(ToxImpl*)&udata;
    if (timp is null || timp.tox is null || timp.sock is null) return;
    try {
      timp.toxFriendMessage(frnum, type, msg[0..msglen]);
    } catch (Exception e) {
      try {
        logwritefln("\nTOX[%s] CB EXCEPTION: %s\n\n", timp.srvalias, e.toString);
        timp.dropNetIOConnection();
      } catch (Exception) {}
    }
  }

  static extern(C) void friendReceiptCB (Tox* tox, uint frnum, uint msgid, void* udata) nothrow {
    //logwritefln("got receipt for msg with id #%s (friend #%s)", msgid, frnum);
    auto timp = *cast(ToxImpl*)&udata;
    if (timp is null || timp.tox is null || timp.sock is null) return;
    ubyte[TOX_PUBLIC_KEY_SIZE] fpk;
    tox_friend_get_public_key(tox, frnum, fpk.ptr, null);
    foreach (ref msg; timp.messages) {
      if (msg.msgid == msgid && !msg.gotreceipt && msg.dest[] == fpk[]) {
        msg.gotreceipt = true;
        timp.gotNewReceipt = true;
      }
    }
  }

  static extern(C) void fileRecvCB (Tox* tox, uint frnum, uint flnum, TOX_FILE_KIND kind, ulong flsize, const(char)* name, usize namelen, void* udata) nothrow {
    logwritefln("got recv request from friend #%s, file #%s, kind=%s, size=%s, name=<%s>", frnum, flnum, kind, flsize, name[0..namelen]);
    auto timp = *cast(ToxImpl*)&udata;
    if (timp is null || timp.tox is null || timp.sock is null) return;
    try {
      if (kind == TOX_FILE_KIND_AVATAR) {
        if (flsize < 16 || flsize > 1024*1024) {
          timp.addRejectEvent(frnum, flnum);
          return;
        }
      }
      timp.addRejectEvent(frnum, flnum);
    } catch (Exception e) {
      try {
        logwritefln("\nTOX[%s] CB EXCEPTION: %s\n\n", timp.srvalias, e.toString);
        timp.dropNetIOConnection();
      } catch (Exception) {}
    }
  }

  static extern(C) void fileRecvCtlCB (Tox* tox, uint frnum, uint flnum, TOX_FILE_CONTROL ctl, void* udata) nothrow {
    logwritefln("got recv ctl for friend #%s, file #%s, ctl:%s", frnum, flnum, ctl);
    auto timp = *cast(ToxImpl*)&udata;
    if (timp is null || timp.tox is null || timp.sock is null) return;
    try {
      final switch (ctl) {
        case TOX_FILE_CONTROL_RESUME: timp.resumeSendByNums(frnum, flnum); break;
        case TOX_FILE_CONTROL_PAUSE: timp.pauseSendByNums(frnum, flnum); break;
        case TOX_FILE_CONTROL_CANCEL: timp.abortSendByNums(frnum, flnum); break;
      }
    } catch (Exception e) {
      try {
        logwritefln("\nTOX[%s] CB EXCEPTION: %s\n\n", timp.srvalias, e.toString);
        timp.dropNetIOConnection();
      } catch (Exception) {}
    }
  }

  static extern(C) void fileChunkReqCB (Tox* tox, uint frnum, uint flnum, ulong pos, usize len, void* udata) nothrow {
    //logwritefln("got chunk req recv ctl for friend #%s, file #%s, ctl:%s", frnum, flnum, ctl);
    auto timp = *cast(ToxImpl*)&udata;
    if (timp is null || timp.tox is null || timp.sock is null) return;
    try {
      timp.fschunks ~= ChunkToSend(frnum, flnum, pos, len);
    } catch (Exception e) {
      try {
        logwritefln("\nTOX[%s] CB EXCEPTION: %s\n\n", timp.srvalias, e.toString);
        timp.dropNetIOConnection();
      } catch (Exception) {}
    }
  }

  // send "#all" users
  void sendJoinAll () {
    import std.format : format;
    sendcmdf(myircnick, "JOIN #all");
    auto cmd = "353 %s @ #all :".format(myircnick);
    foreach (Friend fr; friends.byValue) cmd ~= fr.ircnick~" ";
    sendcmdf(null, "%s", cmd);
    sendcmdf(null, "366 %s @ #all :End of /NAMES list.", myircnick);
  }

  // send "#online" users
  void sendJoinOnline () {
    import std.format : format;
    sendcmdf(myircnick, "JOIN #online");
    auto cmd = "353 %s @ #online :".format(myircnick);
    foreach (Friend fr; friends.byValue) if (fr.online) cmd ~= fr.ircnick~" ";
    sendcmdf(null, "%s", cmd);
    sendcmdf(null, "366 %s @ #online :End of /NAMES list.", myircnick);
  }

  // send "#offline" users
  void sendJoinOffline () {
    import std.format : format;
    sendcmdf(myircnick, "JOIN #offline");
    auto cmd = "353 %s @ #offline :".format(myircnick);
    foreach (Friend fr; friends.byValue) if (!fr.online) cmd ~= fr.ircnick~" ";
    sendcmdf(null, "%s", cmd);
    sendcmdf(null, "366 %s @ #offline :End of /NAMES list.", myircnick);
  }

  // send "#offline" users
  void sendJoinAuthReqs () {
    import std.format : format;
    sendcmdf(myircnick, "JOIN #authreqs");
    auto cmd = ":tox.%s 353 %s @ #authreqs :".format(srvalias, myircnick);
    foreach (const ref FriendReq req; authreqs) cmd ~= req.src[].tox_hex~" ";
    sendcmdf(null, "%s", cmd);
    sendcmdf(null, "366 %s @ #authreqs :End of /NAMES list.", myircnick);
    // and repeat messages
    foreach (ref FriendReq req; authreqs) {
      import std.algorithm : splitter;
      import std.utf : byChar;
      Recoder rc;
      rc.utfucked = true;
      auto nick = req.src[].tox_hex;
      foreach (/*auto*/ part; rc.uncodeBuf(req.msg).byChar.splitter('\n')) {
        sendcmdf(nick, "PRIVMSG #authreqs :%s", part);
      }
    }
  }

  // i am online
  void toxConnectionEstablished () {
    if (isOnline) return;
    isOnline = true;
    doRefreshNicks = true;
    loadUnsentMessages();
    loadAuthRequests();
    sendJoinAll();
    sendJoinOnline();
    sendJoinOffline();
    sendJoinAuthReqs();
  }

  void toxFriendReqest (in ref ubyte[TOX_PUBLIC_KEY_SIZE] pk, cstring msg) {
    import std.algorithm : splitter;
    import std.utf : byChar;
    doRefreshNicks = true;
    serverSayf("*** authorisation request comes from %s", pk[].tox_hex);
    auto nk = pk[].tox_hex;
    bool sendJoin = true;
    if (removeAuthByKey(pk[])) sendJoin = false;
    FriendReq req;
    req.src[] = pk[];
    req.msg = msg.idup;
    req.unixtime = unixnow();
    authreqs.arrayAppend(req);
    saveAuthRequests();
    // join group
    if (sendJoin) sendcmdf(nk, "JOIN #authreqs");
    // send group message with request
    Recoder rc;
    rc.utfucked = true;
    foreach (/*auto*/ part; rc.uncodeBuf(msg).byChar.splitter('\n')) {
      sendcmdf(nk, "PRIVMSG #authreqs :%s", part);
    }
  }

  void toxFriendMessage (uint frnum, TOX_MESSAGE_TYPE type, cstring msg) {
    auto fr = findFriendByNum(frnum);
    if (fr !is null) {
      // this is private message, send PRIVMSG
      import std.algorithm : splitter;
      import std.utf : byChar;
      Recoder rc;
      rc.utfucked = true;
      foreach (/*auto*/ part; rc.uncodeBuf(msg).byChar.splitter('\n')) {
        if (type == TOX_MESSAGE_TYPE_ACTION) {
          sendcmdf(fr.ircnick, "PRIVMSG %s :\x01ACTION %s\x01", myircnick, part);
        } else {
          sendcmdf(fr.ircnick, "PRIVMSG %s :%s", myircnick, part);
        }
      }
    }
  }

  void eventLoop () {
    while (tox !is null && sock !is null && sock.isAlive) {
      if (restoreOnline) {
        logwritefln("TOX:%s *** dropped offline, wtf?!");
        restoreOnline = false;
      }
      // this will call callbacks
      tox_iterate(tox, cast(void*)this);
      // some callback may abort connection
      if (sock is null || tox is null || !sock.isAlive) return;

      // callbacks may set flags
      if (doRefreshNicks) refreshNicks();

      // got some new msg receipts?
      if (gotNewReceipt) {
        for (;;) {
          bool again = false;
          foreach (immutable midx, ref msg; messages) {
            if (msg.gotreceipt) {
              again = true;
              if (auto fr = findFriendByKey(msg.dest[])) {
                // dump it as sent
                if (msg.offlineMsgWarningShown) {
                  import std.string : replace;
                  Recoder rc;
                  rc.utfucked = true;
                  sendcmdf(fr.ircnick, "K8USERNOTICE %s :\x01ACTION \x1d(resent success)\x0f %s\x01", myircnick, rc.uncodeBuf(msg.msg.replace("\n", " ")));
                }
              }
              // remove message from list
              foreach (immutable c; midx+1..messages.length) messages[c-1] = messages[c];
              messages.length -= 1;
              messages.assumeSafeAppend;
              break;
            }
          }
          if (!again) break;
        }
        saveUnsentMessages();
      }

      // warn about undelivered messages
      if (messages.length) {
        auto cutm = unixnow();
        // remove too old messages
        enum TooOldTO = 60*60*24*7; // one week
        for (usize idx = 0; idx < messages.length; ++idx) {
          if (cutm-messages[idx].unixtime >= TooOldTO) {
            foreach (immutable c; idx+1..messages.length) messages[c-1] = messages[c];
            messages.length -= 1;
            messages.assumeSafeAppend;
            --idx;
          }
        }
        // resent
        enum OfflineShowWarningTO = 30; // seconds
        bool saveMessages = false;
        foreach (ref msg; messages) {
          if (sock is null || tox is null || !sock.isAlive) break;
          if (!msg.offlineMsgWarningShown && cutm-msg.unixtime >= OfflineShowWarningTO) {
            saveMessages = true;
            msg.offlineMsgWarningShown = true;
            // send private action
            if (auto fr = findFriendByKey(msg.dest[])) {
              import std.string : replace;
              Recoder rc;
              rc.utfucked = true;
              sendcmdf(fr.ircnick, "K8USERNOTICE %s :\x01ACTION \x1d(send timeout)\x0f %s\x01", myircnick, rc.uncodeBuf(msg.msg.replace("\n", " ")));
            }
            msg.unixtimeNextResend = cutm+10;
          }
          // resend messages
          if (msg.offlineMsgWarningShown && msg.unixtimeNextResend <= cutm) {
            //TODO: forget about week old messages
            msg.unixtimeNextResend = cutm+30;
            if (auto fr = findFriendByKey(msg.dest[])) {
              //msg.offlineMsgWarningShown = false;
              {
                import std.datetime;
                import std.format : format;
                auto time = SysTime.fromUnixTime(msg.unixtime);
                auto rr = "RR:[%04s/%02s/%02s %02s:%02s] %s".format(time.year, cast(int)time.month, time.day, time.hour, time.minute, msg.msg);
                msg.msgid = tox_friend_send_message(tox, fr.frnum, TOX_MESSAGE_TYPE_NORMAL, rr.ptr, rr.length, null);
              }
              //logwritefln("resent msg with id #%s (friend #%s)", msg.msgid, fr.frnum);
              saveMessages = true;
            }
          }
        }
        if (saveMessages) saveUnsentMessages();
      }

      // just in case
      if (sock is null || tox is null || !sock.isAlive) return;

      processFileSending();
      processRejectEvents();

      if (doSaveToxData) { doSaveToxData = false; saveToxCoreData(); }

      {
        auto ctt = unixnow();
        if (ctt >= unixNextPing) {
          unixNextPing = ctt+100;
          sendcmdf(null, "PING :tox.%s", srvalias);
        }
      }

      setRead.reset();
      setWrite.reset();
      setRead.add(sock);
      if (wrdata.length) setWrite.add(sock);

      int res;
      auto ebdelay = tox_iteration_interval(tox);
      if (ebdelay >= 0) {
        import core.time : dur;
        if (ebdelay < 0) ebdelay = int.max/2;
        if (ebdelay < 10) ebdelay = 10;
        if (ebdelay > 32000) ebdelay = 32000;
        res = Socket.select(setRead, setWrite, null, dur!"msecs"(ebdelay));
      } else {
        res = Socket.select(setRead, setWrite, null, 1.seconds);
      }

      if (res < 0) {
        import core.stdc.errno;
        if (errno == EINTR) continue;
        //logwriteln("select FAILED!");
        return;
      }

      if (res == 0) continue; // timeout

      // read commands
      if (setRead.isSet(sock)) {
        if (!sockRead()) {
          // some error occured
          dropNetIOConnection();
          break;
        }
      }

      // write buffers
      if (setWrite.isSet(sock)) {
        if (!sockSend()) {
          // some error occured
          dropNetIOConnection();
          break;
        }
      }
    }
  }

  void dropNetIOConnection () {
    restoreOnline = false;
    if (tox !is null) {
      saveToxCoreData();
      foreach (Friend fr; friends.byValue) purgeFileTransfers(fr);
    }
    isOnline = false;
    if (sock !is null) {
      if (sock.isAlive) {
        sock.shutdown(SocketShutdown.BOTH);
        sock.close();
      }
      sock = null;
    }
  }

  void onEvent (EventQuited evt) {
    dropNetIOConnection();
    //if (tox !is null) { tox_kill(tox); tox = null; }
    //throw new Exception("quited"); //FIXME
  }

  void serverSayf(A...) (cstring fmt, A args) {
    import std.string : format;
    auto txt = fmt.format(args);
    //logwritefln("!!! <%s>", txt);
    sendcmdf(null, "NOTICE %s :%s", myircnick, txt);
  }

  void chanSayf(A...) (cstring cname, cstring fmt, A args) {
    /*
    import std.string : format;
    auto txt = fmt.format(args);
    if (cname is null) {
      sendcmdf(null, "NOTICE %s :%s", myircnick, txt);
    } else {
      sendcmdf("TOX", "PRIVMSG %s :%s", cname, txt);
    }
    */
    serverSayf(fmt, args);
  }

  void killFriend (Friend fr) {
    if (fr !is null) {
      if (tox_friend_delete(tox, fr.frnum, null)) {
        saveToxCoreData();
        refreshNicks();
        doRefreshNicks = true;
      }
    }
  }

private:
  // commands

  // MYID
  void oncmdMyId (cstring entity) {
    ubyte[TOX_ADDRESS_SIZE] myaddr;
    //tox_self_get_public_key(tox, mypubkey.ptr);
    tox_self_get_address(tox, myaddr.ptr);
    serverSayf("my id: %s", myaddr.tox_hex);
  }

  // MYPK
  void oncmdMypk (cstring entity) {
    ubyte[TOX_PUBLIC_KEY_SIZE] mypubkey;
    tox_self_get_public_key(tox, mypubkey.ptr);
    serverSayf("my public key: %s", mypubkey.tox_hex);
  }

  // NICK newnick
  void on2userNick (cstring entity, cstring newnick) {
    import iv.strex : xstrip;
    newnick = newnick.xstrip;
    if (newnick.length == 0) return;
    myircnick = newnick.idup;
    tox_self_set_name(tox, newnick.ptr, newnick.length, null);
  }

  // /forcefriend #chan TOXID
  void on2chanForceFriend (cstring entity, cstring cname, cstring tid) {
    on2userForceFriend(entity, tid);
  }

  // /forcefriend TOXID
  void on2userForceFriend (cstring entity, cstring tid) {
    // remove chan
    if (!isOnline) { serverSayf("NOT ONLINE: /forcefriend %s", tid); return; }
    ubyte[TOX_PUBLIC_KEY_SIZE] pk;
    if (!tox_unhex(pk[], tid)) { serverSayf("INVALID ID: /forcefriend %s", tid); return; }
    if (auto fr = findFriendByKey(pk[])) { serverSayf("ALREADY FRIEND: /forcefriend %s", tid); return; }
    auto tr = tox_friend_add_norequest(tox, pk.ptr, null);
    if (tr != uint.max) {
      saveToxCoreData();
      refreshNicks();
      doRefreshNicks = true;
    } else {
      serverSayf("ERROR: /forcefriend %s", tid);
    }
  }

  // /friend #chan TOXID msg
  void on2chanFriend (cstring entity, cstring cname, cstring tid, cstring[] rest) {
    on2userFriend(entity, tid, rest);
  }

  // /friend TOXID msg
  void on2userFriend (cstring entity, cstring tid, cstring[] rest) {
    import std.array : join;
    if (rest.length < 1) { serverSayf("/friend TOXID msg"); return; }
    auto msg = rest.join(" ");
    if (!isOnline) { serverSayf("NOT ONLINE: /friend %s %s", tid, msg); return; }
    ubyte[TOX_ADDRESS_SIZE] pk;
    if (!tox_unhex(pk[], tid)) { serverSayf("INVALID ID: /friend %s %s", tid, msg); return; }
    if (auto fr = findFriendByKey(pk[0..TOX_PUBLIC_KEY_SIZE])) { serverSayf("ALREADY FRIEND: /friend %s %s", tid, msg); return; }
    // has friend request from him? force friending
    if (findAuthByKey(pk[0..TOX_PUBLIC_KEY_SIZE])) {
      auto tr = tox_friend_add_norequest(tox, pk.ptr, null);
      if (tr != uint.max) {
        saveToxCoreData();
        refreshNicks();
        doRefreshNicks = true;
      }
      removeAuthByKey(pk[0..TOX_PUBLIC_KEY_SIZE]);
      saveAuthRequests();
      return;
    }
    // send it
    if (msg.length == 0) msg = "I just want to make some friends...";
    {
      Recoder rc;
      rc.utfucked = true;
      msg = rc.encodeBuf(msg);
    }
    // and really send it
    auto tr = tox_friend_add(tox, pk.ptr, msg.ptr, msg.length, null);
    //logwritefln("ask, frnum #%s, msg: <%s>", tr, msg);
    refreshNicks();
    doRefreshNicks = true;
    saveToxCoreData();
  }

  // /unfriend #chan TOXID msg
  void on2chanUnfriend (cstring entity, cstring cname, cstring tid) {
    on2userUnfriend(entity, tid);
  }

  // /unfriend TOXID/nick
  void on2userUnfriend (cstring entity, cstring tid) {
    if (!isOnline) { serverSayf("NOT ONLINE: /unfriend %s", tid); return; }
    foreach (Friend fr; friends.byValue) {
      if (fr.ircnick.ircStrEquCI(tid)) { killFriend(fr); return; }
    }
    usize pklen;
    ubyte[TOX_ADDRESS_SIZE] pk;
    if (tox_unhex(pk[], tid)) {
      pklen = TOX_ADDRESS_SIZE;
    } else if (tox_unhex(pk[0..TOX_PUBLIC_KEY_SIZE], tid)) {
      pklen = TOX_PUBLIC_KEY_SIZE;
    } else {
      serverSayf("INVALID ID: /unfriend %s", tid);
      return;
    }
    foreach (Friend fr; friends.byValue) {
      if (fr.pubkey == pk[0..TOX_PUBLIC_KEY_SIZE]) { killFriend(fr); return; }
    }
  }

  // /fkey #chan nick
  void on2chanFKey (cstring entity, cstring cname, cstring unick) {
    if (auto fr = findFriendByIRCNick(unick)) {
      chanSayf(cname, "%s ID: %s", fr.ircnick, fr.pubkey.tox_hex);
    }
  }

  // /fkey nick
  void on2userFKey (cstring entity, cstring unick) {
    on2chanFKey(entity, null, unick);
  }

  // /accept #chan nick
  void on2chanAccept (cstring entity, cstring cname, cstring unick) {
    logwritefln("on2chanAccept: ent=%s; cname=%s; unick=%s", entity, cname, unick);
    on2userAccept(entity, unick);
  }

  // /accept nick
  void on2userAccept (cstring entity, cstring unick) {
    logwritefln("oncmdAccept: ent=%s; unick=%s", entity, unick);
    if (auto req = findAuthByNick(unick)) {
      logwritefln("  oncmdAccept found request: ent=%s; unick=%s", entity, unick);
      //serverSayf("%s ID: %s", fr.ircnick, fr.pubkey.tox_hex);
      tox_friend_add_norequest(tox, req.src.ptr, null);
      if (removeAuthByKey(req.src[])) sendcmdf(req.src.tox_hex, "PART #authreqs");
      saveToxCoreData();
      saveAuthRequests();
      refreshNicks();
      doRefreshNicks = true;
    }
  }

  // /reject #chan nick
  void on2chanReject (cstring entity, cstring cname, cstring unick) {
    on2userReject(entity, unick);
  }

  // /reject nick
  void on2userReject (cstring entity, cstring unick) {
    if (auto req = findAuthByNick(unick)) {
      //serverSayf("%s ID: %s", fr.ircnick, fr.pubkey.tox_hex);
      if (removeAuthByKey(req.src[])) sendcmdf(req.src.tox_hex, "PART #authreqs");
      saveToxCoreData();
      saveAuthRequests();
      refreshNicks();
      doRefreshNicks = true;
    }
  }

  // /forget #chan nick
  void on2chanForget (cstring entity, cstring cname, cstring unick) {
    on2userForget(entity, unick);
  }

  // /forget nick
  void on2userForget (cstring entity, cstring unick) {
    if (auto fr = findFriendByIRCNick(unick)) {
      bool doSaveMsg = false;
      for (usize idx = 0; idx < messages.length; ++idx) {
        if (messages[idx].dest[] == fr.pubkey[]) {
          foreach (immutable c; idx+1..messages.length) messages[c-1] = messages[c];
          messages.length -= 1;
          messages.assumeSafeAppend;
          --idx;
          doSaveMsg = true;
        }
      }
      if (doSaveMsg) saveUnsentMessages();
    }
  }

  // PRIVMSG dest :text
  void on2userPrivMsg (cstring entity, cstring unick, cstring msgtext) {
    auto fr = findFriendByIRCNick(unick);
    if (fr is null) { serverSayf("unknown user: '%s'", unick); return; }
    if (msgtext.length == 0) return;
    // add to unsent list
    Msg msg;
    msg.dest[] = fr.pubkey[];
    msg.unixtime = unixnow();
    {
      Recoder rc;
      rc.utfucked = true;
      auto text = rc.encodeBuf(msgtext);
      msg.msg = text.idup;
      msg.msgid = tox_friend_send_message(tox, fr.frnum, TOX_MESSAGE_TYPE_NORMAL, text.ptr, text.length, null);
    }
    logwritefln("queued msg with id #%s (friend #%s)", msg.msgid, fr.frnum);
    messages.arrayAppend(msg);
    saveUnsentMessages();
  }

  // immediately rejoin important channels on part
  // PART #chan [msg]
  void on2chanPart (cstring entity, cstring cname, cstring[] rest) {
    if (cname.ircStrEquCI("#all")) sendJoinAll();
    if (cname.ircStrEquCI("#online")) sendJoinOnline();
    if (cname.ircStrEquCI("#offline")) sendJoinOffline();
    if (cname.ircStrEquCI("#authreqs")) sendJoinAuthReqs();
  }

  // /ustatus #chan nick
  void on2chanUStatus (cstring entity, cstring cname, cstring unick) {
    on2userUStatus(entity, unick);
  }

  // /ustatus nick
  void on2userUStatus (cstring entity, cstring unick) {
    if (auto fr = findFriendByIRCNick(unick)) {
      auto sz = tox_friend_get_status_message_size(tox, fr.frnum, null);
      if (sz > 0 && sz < int.max/8) {
        import core.stdc.stdlib : malloc, free;
        auto stext = cast(char*)malloc(sz);
        if (stext !is null) {
          scope(exit) free(stext);
          if (tox_friend_get_status_message(tox, fr.frnum, stext, null)) {
            serverSayf("%s status: %s", fr.ircnick, stext[0..sz]);
          }
        }
      } else if (sz == 0) {
        serverSayf("%s has no status", fr.ircnick);
      } else {
        serverSayf("error getting status from %s", fr.ircnick);
      }
    } else {
      serverSayf("user '%s' not found", unick);
    }
  }

  // /status my status
  void oncmdStatus (cstring entity, cstring[] rest) {
    import std.array : join;
    // remove possible user nick
    if (rest.length > 0) {
      foreach (Friend fr; friends.byValue) {
        if (fr.ircnick.ircStrEquCI(rest[0])) {
          rest = rest[1..$];
          break;
        }
      }
    }
    auto stt = rest.join(" ").ircstrip;
    if (stt.length > 256) {
      serverSayf("status too long: %s", stt);
    } else {
      tox_self_set_status_message(tox, stt.ptr, stt.length, null);
    }
    //tox_self_set_status(tox, TOX_USER_STATUS.NONE);
  }

  void on2chanStatus (cstring entity, cstring cname, cstring[] rest) {
    oncmdStatus(entity, rest);
  }

  // /away [msg]
  void oncmdAway (cstring entity, cstring[] rest) {
    import std.array : join;
    // remove possible user nick
    if (rest.length > 0) {
      foreach (Friend fr; friends.byValue) {
        if (fr.ircnick.ircStrEquCI(rest[0])) {
          rest = rest[1..$];
          break;
        }
      }
    }
    if (rest.length) {
      auto stt = rest.join(" ").ircstrip;
      if (stt.length > 256) {
        serverSayf("status too long: %s", stt);
      } else {
        tox_self_set_status(tox, TOX_USER_STATUS_AWAY);
        tox_self_set_status_message(tox, stt.ptr, stt.length, null);
      }
    } else {
      // unaway
      tox_self_set_status(tox, TOX_USER_STATUS_NONE);
      tox_self_set_status_message(tox, null, 0, null);
    }
  }

  void on2chanAway (cstring entity, cstring cname, cstring[] rest) {
    oncmdAway(entity, rest);
  }

  void oncmdHelp (cstring entity, cstring[] rest) {
    enum Help = q{
      /myid
        show my id
      /mypk
        show my public key
      /nick <newnick>
        change my nick
      /forcefriend TOXID
        add friend without sending request
      /friend TOXID msg
        send friend request
      /unfriend TOXID|nick
        remove friend
      /fkey nick
        show friend pubkey
      /accept nick
        accept friend request
      /reject nick
        reject friend request
      /forget nick
        forget pending chat messages for nick
        Miri will try to resend messages for a week,
        and then she will forget them
    };
    import std.algorithm : splitter;
    import std.utf : byChar;
    string[] lines;
    foreach (/*auto*/ ss; Help.byChar.splitter("\n")) {
      import std.array : array;
      auto s = ss.array;
      while (s.length && s[$-1] <= ' ') s = s[0..$-1];
      lines.arrayAppend(s.idup);
    }
    while (lines.length && lines[$-1].length == 0) lines = lines[0..$-1];
    if (lines.length == 0) return;
    auto anchor = lines;
    int spdel = int.max;
    foreach (string s; lines) {
      if (s.length == 0) continue;
      int spc = 0;
      while (spc < s.length && s[spc] <= ' ') ++spc;
      if (spc < spdel) spdel = spc;
    }
    foreach (string s; lines) {
      if (s.length < spdel) s = null; else s = s[spdel..$];
      serverSayf("%s", s);
    }
  }

private:
  // dispatcher
  void dispatch (cstring entity, cstring[] args) {
    bool isArgMyNick (cstring s) { return false; }
    mixin(DispatcherMixin);
  }

  void onLine (cstring line) {
    if (line.length == 0) return;
    if (tox is null || sock is null || !sock.isAlive) return;
    logwritefln("Tox:%s LINE: [%s]", srvalias, line);
    auto pl = ircParseLine(line);
    if (pl.cmd == "PING") {
      //logwritefln("PING RECEIVED: <%s>", pl.argtext);
      sendcmdf(null, "PONG %s", pl.argtext);
      return;
    }
    dispatch(pl.entity, pl.allargs);
  }
}
