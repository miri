/* Invisible Vector IRC client
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
module miri.irc;
private:

import std.datetime;

import iv.rawtty;
import iv.strex : indexOf, xstrip, startsWith;
import iv.utfutil;

import iv.vfs.io;

import iv.sq3;

import iv.eventbus;
import iv.egtui.txtser;

import miri.types;
import miri.cfg;
import miri.log;
import miri.util;
import miri.netio;

// request capabilities?
// currently, there is a little reason to do this
//version = send_caps;


// ////////////////////////////////////////////////////////////////////////// //
// reconnect to server, scheduled event
public class EventReconnect : Event {
  IRCServer server;
  this (IRCServer aserver) { server = aserver; }
}


public class EventMyNickChanged : Event {
  IRCServer server;
  this (IRCServer aserver) { server = aserver; }
}

public class EventUserNickChanged : Event {
  IRCUser user;
  string oldnick;
  this (IRCUser auser, string aoldnick) { user = auser; oldnick = aoldnick; }
}


// this is for self
public class EventJoinPart : Event {
  IRCChannel chan;
  this (IRCChannel achan) { chan = achan; }
}

public class EventJoin : EventJoinPart {
  this (IRCChannel achan) { super(achan); }
}

public class EventPart : EventJoinPart {
  this (IRCChannel achan) { super(achan); }
}


// this is both for self and others
public class EventChanEnterLeave : Event {
  IRCChannel chan;
  IRCUser user;
  string msg;
  this (IRCChannel achan, IRCUser auser, string amsg) { chan = achan; user = auser; msg = amsg; }
}

public class EventChanEnter : EventChanEnterLeave {
  this (IRCChannel achan, IRCUser auser, string amsg) { super(achan, auser, amsg); }
}

public class EventChanLeave : EventChanEnterLeave {
  this (IRCChannel achan, IRCUser auser, string amsg) { super(achan, auser, amsg); }
}


// user oped/deoped, etc
public class EventUserChanStatusChanged : Event {
  IRCChannel chan;
  IRCUser user;
  SysTime time;
  this (IRCChannel achan, IRCUser auser) { time = Clock.currTime; chan = achan; user = auser; }
}

// online/offline/away status changed
public class EventUserStatusChanged : Event {
  IRCUser user;
  SysTime time;
  this (IRCUser auser) { time = Clock.currTime; user = auser; }
}


public class EventNickServNotice : Event {
  IRCServer server;
  SysTime time;
  string msg;
  this (IRCServer aserver, string amsg) { time = Clock.currTime; server = aserver; msg = amsg; }
}

public class EventOtherNotice : Event {
  IRCServer server;
  SysTime time;
  string msg;
  bool hasme; // message has my nick in it?
  bool unimportant;
  this (IRCServer aserver, string amsg, bool ahasme, bool aunimportant=false) { time = Clock.currTime; server = aserver; msg = amsg; hasme = ahasme; unimportant = aunimportant; }
}

public class EventUserNotice : Event {
  IRCUser user;
  SysTime time;
  string msg;
  bool hasme; // message has my nick in it?
  this (IRCUser auser, string amsg, bool ahasme) { time = Clock.currTime; user = auser; msg = amsg; hasme = ahasme; }
}


public class EventPrivChat : Event {
  IRCUser user;
  SysTime time;
  string msg;
  bool hisname; // use his name as source?
  this (IRCUser auser, string amsg, bool ahisname) { time = Clock.currTime; user = auser; msg = amsg; hisname = ahisname; }
}

public class EventChanChat : Event {
  IRCChannel chan;
  IRCUser user;
  SysTime time;
  string msg;
  bool hasme; // message has my nick in it?
  this (IRCChannel achan, IRCUser auser, string amsg, bool ahasme) { time = Clock.currTime; chan = achan; user = auser; msg = amsg; hasme = ahasme; }
}


public class EventPrivSysNotice : Event {
  IRCUser user;
  SysTime time;
  string msg;
  this (IRCUser auser, string amsg) { time = Clock.currTime; user = auser; msg = amsg; }
}

public class EventChanSysNotice : Event {
  IRCChannel chan;
  IRCUser user;
  SysTime time;
  string msg;
  this (IRCChannel achan, IRCUser auser, string amsg) { time = Clock.currTime; chan = achan; user = auser; msg = amsg; }
}


public class EventChanTopic : Event {
  IRCChannel chan;
  SysTime time;
  string topic; // null: none/empty
  this (IRCChannel achan, string atopic) { time = Clock.currTime; chan = achan; topic = atopic; }
}


public class EventUserQuits : Event {
  IRCUser user;
  SysTime time;
  string msg;
  this (IRCUser auser, string amsg) { time = Clock.currTime; user = auser; msg = amsg; }
}


// ////////////////////////////////////////////////////////////////////////// //
private __gshared Database dbLog;

shared static ~this () {
  dbLog.close();
}

private void ensureLogDatabase () {
  if (dbLog.isOpen) return;
  import std.file : mkdirRecurse;
  import std.path : buildPath, dirName;
  auto fname = buildPath(configDir, "log.db");
  mkdirRecurse(fname.dirName);
  dbLog = Database(fname, Database.Mode.ReadWriteCreate, `
PRAGMA page_size = 512;
PRAGMA case_sensitive_like = OFF;
PRAGMA foreign_keys = OFF;
PRAGMA locking_mode = NORMAL; /*EXCLUSIVE;*/
PRAGMA secure_delete = OFF;
PRAGMA threads = 3;
PRAGMA trusted_schema = OFF;
PRAGMA writable_schema = OFF;
PRAGMA auto_vacuum = NONE;
PRAGMA encoding = "UTF-8";
PRAGMA temp_store = DEFAULT;
PRAGMA journal_mode = WAL; /*OFF;*/
--PRAGMA journal_mode = DELETE; /*OFF;*/
PRAGMA synchronous = NORMAL; /*OFF;*/
`, `
CREATE TABLE IF NOT EXISTS log (
    time INTEGER             /* unixepoch */
  , type TEXT DEFAULT 'irc'  /* "irc" or "tox" */
  , server TEXT              /* server host, w/o connection type and port */
  , chan TEXT                /* NULL means "system log", otherwise either room name, or user */
  , src TEXT                 /* NULL means "system log", '' means "me" (empty line, not the same as NULL!) */
  , text TEXT
);

CREATE INDEX IF NOT EXISTS log_by_srv ON log(server);
CREATE INDEX IF NOT EXISTS log_by_srv_chan_time ON log(server, chan, time);
`);
}

private void logAppend (SysTime time, const(char)[] serverAddress, const(char)[] chan, const(char)[] src, const(char)[] text) nothrow {
  static bool isServerName (const(char)[] s) nothrow @trusted @nogc {
    pragma(inline, true);
    return (s.length > 4 && (s.startsWith("tcp:") || s.startsWith("tls:") || s.startsWith("ssl:") || s.startsWith("tox:")));
  }

  static T extractServerName(T:const(char)[]) (T s) {
    static if (is(T == typeof(null))) {
      return null;
    } else {
      if (!isServerName(s)) return null;
      s = s[4..$];
      assert(s.length);
      auto colpos = s.indexOf(':');
      if (colpos > 0) s = s[0..colpos];
      return s;
    }
  }

  static T recodeKoi2Utf(T:const(char)[]) (T s) {
    static if (is(T == typeof(null))) {
      return null;
    } else {
      bool needWork = false;
      foreach (immutable char ch; s) if (ch >= 128) { needWork = true; break; }
      if (!needWork) return s;
      if (utf8Valid(s)) return s;
      char[] res;
      res.reserve(s.length*2);
      foreach (immutable char ch; s) {
        if (ch < 128) { res ~= ch; continue; }
        wchar uch = koi2uni(ch);
        char[4] utf = void;
        auto len = utf8Encode(utf[], uch);
        assert(len > 0);
        res ~= utf[0..len];
      }
      return cast(T)res; // it is safe to cast here
    }
  }

  try {
    auto server = extractServerName(serverAddress);
    string type = (serverAddress.startsWith("tox:") ? "tox" : "irc");

    chan = recodeKoi2Utf(chan);
    src = recodeKoi2Utf(src);
    text = recodeKoi2Utf(text);

    ensureLogDatabase();
    auto stIns = dbLog.statement(`
      INSERT INTO log
              ( server, type, chan, src, text, time)
        VALUES(:server,:type,:chan,:src,:text,:time)
    ;`);
    if (type.length == 0) type = "irc";
    stIns
      .bindConstText(":server", server)
      .bindConstText(":type", type)
      .bindConstText(":chan", chan, allowNull:true)
      .bindConstText(":src", src, allowNull:true)
      .bindConstText(":text", text)
      .bind(":time", time.toUnixTime)
      .doAll();
  } catch (Exception) {
    // sorry
  }
}


// ////////////////////////////////////////////////////////////////////////// //
public void logSrvMessage (IRCServer srv, const(char)[] src, const(char)[] text) {
  if (srv is null) return;
  logAppend(Clock.currTime, serverAddress:srv.address, chan:null, src:src, text);
}


public void logChanMessage (IRCChannel chan, IRCUser user, SysTime time, const(char)[] text) {
  if (chan is null || chan.server is null) return;
  if (user is null) return;
  logAppend(time, chan.server.address, chan.name, user.nick, text);
}


public void logPrivMessage (IRCUser user, SysTime time, bool hisname, const(char)[] text) {
  if (user is null) return;
  if (hisname) {
    // use his name as source
    logAppend(time, user.server.address, user.nick, user.nick, text);
  } else {
    // use my name as source
    logAppend(time, user.server.address, user.nick, user.server.self.nick, text);
  }
}


// ////////////////////////////////////////////////////////////////////////// //
public void setupChatLogger () {
  addEventListener((EventPrivChat evt) {
    evt.user.logPrivMessage(evt.time, evt.hisname, evt.msg);
  });
  addEventListener((EventChanChat evt) {
    //evt.chan.logChatMessageF(evt.time, "<%s> %s", evt.user.nick, evt.msg);
    evt.chan.logChanMessage(evt.user, evt.time, evt.msg);
  });
}


// ////////////////////////////////////////////////////////////////////////// //
public class IRCUser {
  enum Status {
    Offline,
    Online,
    Away,
  }

  IRCServer server;
  string nick;
  Status status = Status.Online;
  bool isme; // my user?
  bool ignored; // is this user ignored?
  bool nonotify; // skip notifiers for this user?
  string ignoreShort; // short ignore text
  bool dead; // true if the user quits and no longer on server

  this () {}

  @property string visNick () {
    if (!ignored || ignoreShort.length == 0) return nick;
    return nick~"<"~ignoreShort~">";
  }

  bool say (cstring msg, bool doWrap) {
    if (server is null || !server.alive) return false;
    server.sayTo(nick, msg, doWrap);
    return true;
  }
}


public class IRCChannel {
  IRCServer server;
  string name;
  string topic;
  bool joined;
  IRCUser[] users; // opers and mods are here too
  bool[IRCUser] opers;
  bool[IRCUser] voiced;
  bool blink; // should tray icon blink on new message to this chan?
  bool popup; // should we show libnotify popup for this channel?
  bool rejoin; // after reconnect
  bool userlistComplete;

  this () { blink = true; popup = true; }

  bool isOp (IRCUser xuser) {
    if (xuser is null) return false;
    return ((xuser in opers) !is null);
  }

  bool isVoiced (IRCUser xuser) {
    if (xuser is null) return false;
    return ((xuser in voiced) !is null);
  }

  bool say (cstring msg, bool doWrap) {
    if (server is null || !server.alive || !joined) return false;
    server.sayTo(name, msg, doWrap);
    return true;
  }

  bool hasUser (IRCUser xuser) {
    foreach (IRCUser user; users) if (user is xuser) return true;
    return false;
  }

  void setTopic (cstring atopic) {
    atopic = atopic.ircstrip;
    if (atopic == topic) return;
    topic = atopic.idup;
    (new EventChanTopic(this, topic)).post;
  }

  void removeEveryone () {
    if (users.length) {
      while (users.length) {
        (new EventChanLeave(this, users[$-1], null)).post;
        users[$-1] = null;
        users.length -= 1;
      }
    }
    users.assumeSafeAppend;
    opers.clear;
    voiced.clear;
  }

  // mode: '@' for op, '+' for mod, others are ignored for now
  void changeUserMode (IRCUser xuser, char mode, bool doset) {
    if (xuser is null) return;
    //if (mode != '@' && mode != '+') return;

    bool found = false;
    foreach (IRCUser user; users) {
      if (user is xuser) {
        found = true;
        break;
      }
    }
    if (!found) return;

    if (mode == '@') {
      if (doset) {
        if (xuser in opers) return;
        voiced.remove(xuser);
        opers[xuser] = true;
      } else {
        if (xuser !in opers) return;
        opers.remove(xuser);
      }
    } else if (mode == '+') {
      if (doset) {
        if (xuser in voiced) return;
        opers.remove(xuser);
        voiced[xuser] = true;
      } else {
        if (xuser !in voiced) return;
        voiced.remove(xuser);
      }
    } else {
      return;
    }

    (new EventUserChanStatusChanged(this, xuser)).post;
  }

  // user entered
  // mode: '@' for op, '+' for mod, '?' for "don't change", other for deop and demod
  void enter(bool totop=false) (IRCUser xuser, char mode, string msg, bool sendEvent=true) {
    if (xuser is null) return;

    bool sendStatusChanged = false;

    opers.remove(xuser);
    voiced.remove(xuser);

    if (mode == '@') {
      if (xuser !in opers) {
        sendStatusChanged = true;
        voiced.remove(xuser);
        opers[xuser] = true;
      }
    } else if (mode == '+') {
      if (xuser !in voiced) {
        sendStatusChanged = true;
        opers.remove(xuser);
        voiced[xuser] = true;
      }
    } else if (mode != '?') {
      if (xuser in voiced) {
        sendStatusChanged = true;
        voiced.remove(xuser);
      }
      if (xuser in opers) {
        sendStatusChanged = true;
        opers.remove(xuser);
      }
    }

    foreach (IRCUser user; users) {
      if (user is xuser) {
        if (sendStatusChanged) (new EventUserChanStatusChanged(this, xuser)).post;
        return;
      }
    }

    // new user
    static if (totop) {
      //users.arrayAppend(null);
      users.arrayGrow(1);
      foreach_reverse (immutable c; 0..users.length-1) users[c+1] = users[c];
      users[0] = xuser;
    } else {
      users.arrayAppend(xuser);
    }
    if (sendEvent) (new EventChanEnter(this, xuser, msg)).post;
    if (sendStatusChanged) (new EventUserChanStatusChanged(this, xuser)).post;
  }

  void leave (IRCUser xuser, string msg, bool sendEvent=true) {
    if (xuser is null) return;

    voiced.remove(xuser);
    opers.remove(xuser);

    foreach (immutable idx, IRCUser user; users) {
      if (user is xuser) {
        foreach (immutable c; idx+1..users.length) users.ptr[c-1] = users.ptr[c];
        users[$-1] = null;
        users.length -= 1;
        users.assumeSafeAppend;
        if (sendEvent) (new EventChanLeave(this, xuser, msg)).post;
        return;
      }
    }
  }
}


// ////////////////////////////////////////////////////////////////////////// //
public class IRCServer {
private import core.time : MonoTime;

public:
  alias CodePage = Recoder.CodePage;

  @property CodePage codepage () const pure nothrow @safe @nogc { return inbuf.codepage; }
  @property void codepage (CodePage cp) pure nothrow @safe @nogc { inbuf.codepage = cp; }

  @property bool utfucked () const pure nothrow @safe @nogc { return inbuf.utfucked; }
  @property void utfucked (bool v) pure nothrow @safe @nogc { inbuf.utfucked = v; }

private:
  static struct ServerCfg {
    string address;
    Recoder.CodePage codepage = Recoder.CodePage.koi8u;
    bool utfucked;
    bool sendCodepage;
    string nick;
    string realname;
    string hostname;
    string password;
    string srvpassword;
    string[] afterConnect;
    bool reconnect = true;
    int forceXKbLayout = -1;
    string authtype; // "freenode", "libera"
    string certbase;
  }

  static struct IgnoreInfo {
    string nick;
    string ignoreShort;
    string ignoreLong;
    bool allowprivchat; // for ignored user
    bool nonotify;
    bool ignore = true;
    @property bool valid () pure const nothrow @safe @nogc { return (nick.length > 0); }
  }

  static struct ChanOption {
    bool noblink;
    bool nopopup;
  }

  LineCollector inbuf;
  bool connecting;

//TODO: make accessors
public:
  uint connid;
  string srvalias;
  string address;
  string[] afterConnect;
  IRCUser self;
  string realnick; // we wanted this
  string realname;
  string hostname;
  string password;
  string srvpassword;
  bool sendCodepage;
  IRCChannel[] chans; // opened channels
  IRCUser[] users; // all known users
  bool reconnect;
  bool skipreconnect;
  int forceXKbLayout;
  string authtype; // "freenode", "libera"
  string certbase;
  string newNickOnConflict;
  bool awaitAuth; // waiting for auth reply (initial commands pending)

protected:
  IgnoreInfo[] ignores;
  ChanOption[string] chanopts;

public:
  this (cstring aserver, ushort aport) {
    import std.format : format;
    if (aserver.length == 0) throw new Exception("server without name");
    self = new IRCUser();
    self.server = this;
    self.isme = true;
    self.status = IRCUser.Status.Offline;
    srvalias = aserver.idup;
    if (aport == 0) {
      //throw new Exception("server with invalid port");
      address = srvalias;
    } else {
      address = "tcp:%s:%s".format(aserver, aport);
    }
  }

  this (cstring aserveralias) {
    if (aserveralias.length == 0) throw new Exception("empty server alias");
    srvalias = aserveralias.idup;
    self = new IRCUser();
    self.server = this;
    self.isme = true;
    self.status = IRCUser.Status.Offline;
    loadConfig();
    loadIgnores();
    loadChanOptions();
  }

  @property bool alive () { return (connid != 0); }
  @property bool dead () { return skipreconnect; } //FIXME

  void markDead () { skipreconnect = true; } //FIXME

  private void loadConfig () {
    import std.file, std.path;
    auto fname = buildPath(configDir, normStr2FName(srvalias)~".config.rc");
    auto fl = VFile(fname, "r");
    ServerCfg cfg;
    fl.txtunser(cfg);
    if (cfg.address.length == 0) throw new Exception("invalid server address");
    if (cfg.nick.length == 0) throw new Exception("invalid default nick");
    address = cfg.address;
    codepage = cfg.codepage;
    inbuf.utfucked = cfg.utfucked;
    sendCodepage = cfg.sendCodepage;
    realname = cfg.realname;
    hostname = cfg.hostname;
    self.nick = cfg.nick;
    password = cfg.password;
    srvpassword = cfg.srvpassword;
    afterConnect = cfg.afterConnect;
    reconnect = cfg.reconnect;
    forceXKbLayout = cfg.forceXKbLayout;
    authtype = cfg.authtype;
    certbase = cfg.certbase;
  }

  private void loadIgnores () {
    import std.file, std.path;
    try {
      IgnoreInfo[] ilist;
      auto fname = buildPath(configDir, normStr2FName(srvalias)~".ignore.rc");
      auto fl = VFile(fname, "r");
      fl.txtunser(ilist);
      ignores = ilist;
    } catch (Exception e) {}
  }

  private void loadChanOptions () {
    import std.file, std.path;
    try {
      ChanOption[string] opts;
      auto fname = buildPath(configDir, normStr2FName(srvalias)~".chanopts.rc");
      auto fl = VFile(fname, "r");
      fl.txtunser(opts);
      chanopts = opts;
    } catch (Exception e) {}
  }

  bool saveConfig () {
    import std.file, std.path;
    try {
      ServerCfg cfg;
      cfg.address = address;
      cfg.codepage = codepage;
      cfg.utfucked = inbuf.utfucked;
      cfg.sendCodepage = sendCodepage;
      cfg.realname = realname;
      cfg.hostname = hostname;
      cfg.nick = self.nick;
      cfg.password = password;
      cfg.srvpassword = srvpassword;
      cfg.afterConnect = afterConnect;
      cfg.reconnect = reconnect;
      cfg.forceXKbLayout = forceXKbLayout;
      cfg.authtype = authtype;
      cfg.certbase = certbase;
      auto fname = buildPath(configDir, normStr2FName(srvalias)~".config.rc");
      auto fl = VFile(fname, "w");
      fl.txtser(cfg);
      return true;
    } catch (Exception e) {}
    return false;
  }

  bool saveIgnores () {
    try {
      import std.file, std.path;
      auto fname = buildPath(configDir, normStr2FName(srvalias)~".ignore.rc");
      auto fl = VFile(fname, "w");
      fl.txtser(ignores);
      return true;
    } catch (Exception e) {}
    return false;
  }

  bool saveChanOptions () {
    // update chan flags
    foreach (IRCChannel chan; chans) {
      if (auto opts = findChanOptions(chan.name)) {
        chan.blink = !opts.noblink;
        chan.popup = !opts.nopopup;
      }
    }
    try {
      import std.file, std.path;
      auto fname = buildPath(configDir, normStr2FName(srvalias)~".chanopts.rc");
      auto fl = VFile(fname, "w");
      fl.txtser(chanopts);
      return true;
    } catch (Exception e) {}
    return false;
  }

  private ChanOption* findChanOptions (cstring name) {
    if (auto copt = name in chanopts) return copt;
    foreach (string key; chanopts.byKey) {
      if (key.ircStrEquCI(name)) return (key in chanopts);
    }
    return null;
  }

public:
  bool hasMyName (cstring msg) {
    if (self.nick.length == 0) return false;
    while (msg.length >= self.nick.length) {
      if (!msg.ptr[0].ircisalnum) { msg = msg[1..$]; continue; }
      if (self.nick.ircStrEquCI(msg[0..self.nick.length])) {
        if (msg.length == self.nick.length) return true;
        if (!msg.ptr[self.nick.length].ircisalnum) return true;
      }
      while (msg.length && msg.ptr[0].ircisalnum) msg = msg[1..$];
    }
    return false;
  }

  cstring nickFromMask (cstring mask) {
    import iv.strex : indexOf;
    auto wowpos = mask.indexOf('!');
    return (wowpos > 0 ? mask[0..wowpos] : null);
  }

  private void updateIgnores () {
    foreach (IRCUser user; users) {
      user.ignored = false;
      user.nonotify = false;
      user.ignoreShort = null;
      foreach (ref IgnoreInfo ii; ignores) {
        if (ii.nick.ircMaskEquCI(user.nick)) {
          user.ignored = ii.ignore;
          user.nonotify = ii.nonotify;
          user.ignoreShort = ii.ignoreShort;
          break;
        }
      }
    }
    saveIgnores();
  }

  void ignoreUser (cstring anick, cstring ishort, cstring ilong=null) {
    if (anick.length == 0) return;
    foreach (ref IgnoreInfo ii; ignores) {
      if (ii.nick.ircMaskEquCI(anick)) {
        if (ii.ignore && ii.ignoreShort == ishort && ii.ignoreLong == ilong) return; // nothing was changed
        ii.ignore = true;
        ii.ignoreLong = ilong.idup;
        //if (ii.ignoreShort == ishort) { saveIgnores(); return; } // no need to update users
        ii.ignoreShort = ishort.idup;
        updateIgnores();
        return;
      }
    }
    ignores.arrayAppend(IgnoreInfo(anick.idup, ishort.idup, ilong.idup));
    updateIgnores();
  }

  bool unignoreUser (cstring anick) {
    if (anick.length == 0) return false;
    bool wasFound = false;
    for (;;) {
      bool repeat = false;
      foreach (immutable idx, ref IgnoreInfo ii; ignores) {
        if (ii.ignore && ii.nick.ircMaskEquCI(anick)) {
          ii.ignore = false;
          if (!ii.nonotify) {
            foreach (immutable c; idx+1..ignores.length) ignores.ptr[c-1] = ignores[c];
            ignores[$-1] = IgnoreInfo.init;
            ignores.length -= 1;
            ignores.assumeSafeAppend;
            updateIgnores();
            wasFound = true;
            repeat = true;
            break;
          } else {
            updateIgnores();
            wasFound = true;
          }
        }
      }
      if (!repeat) break;
    }
    return wasFound;
  }

  bool allowIngoredPrivChat (cstring anick, bool allowed) {
    if (anick.length == 0) return false;
    foreach (immutable idx, ref IgnoreInfo ii; ignores) {
      if (ii.nick.ircMaskEquCI(anick)) {
        if (ii.allowprivchat != allowed) {
          //TODO: send event
          ii.allowprivchat = true;
          updateIgnores();
          return true;
        }
      }
    }
    return false;
  }

  // if returnted nick is empty, nothing was found
  IgnoreInfo findFlagsInfo (cstring anick) {
    if (anick.length == 0) return IgnoreInfo.init;
    foreach (immutable idx, ref IgnoreInfo ii; ignores) {
      if (ii.nick.ircMaskEquCI(anick)) return ii;
    }
    return IgnoreInfo.init;
  }

  // if returnted nick is empty, nothing was found
  IgnoreInfo findIgnoreInfo (cstring anick) {
    if (anick.length == 0) return IgnoreInfo.init;
    foreach (immutable idx, ref IgnoreInfo ii; ignores) {
      if (ii.ignore && ii.nick.ircMaskEquCI(anick)) return ii;
    }
    return IgnoreInfo.init;
  }

  void nonotifyUser (cstring anick) {
    if (anick.length == 0) return;
    foreach (ref IgnoreInfo ii; ignores) {
      if (ii.nick.ircMaskEquCI(anick)) {
        if (ii.nonotify) return; // nothing was changed
        ii.nonotify = true;
        updateIgnores();
        return;
      }
    }
    ignores.arrayAppend(IgnoreInfo(anick.idup, null, null, true, true, false));
    updateIgnores();
  }

  bool donotifyUser (cstring anick) {
    if (anick.length == 0) return false;
    bool wasFound = false;
    for (;;) {
      bool repeat = false;
      foreach (immutable idx, ref IgnoreInfo ii; ignores) {
        if (ii.nonotify && ii.nick.ircMaskEquCI(anick)) {
          ii.nonotify = false;
          if (!ii.ignore) {
            foreach (immutable c; idx+1..ignores.length) ignores.ptr[c-1] = ignores[c];
            ignores[$-1] = IgnoreInfo.init;
            ignores.length -= 1;
            ignores.assumeSafeAppend;
            updateIgnores();
            wasFound = true;
            repeat = true;
            break;
          } else {
            updateIgnores();
            wasFound = true;
          }
        }
      }
      if (!repeat) break;
    }
    return wasFound;
  }

  // if returnted nick is empty, nothing was found
  IgnoreInfo findNonotifyInfo (cstring anick) {
    if (anick.length == 0) return IgnoreInfo.init;
    foreach (immutable idx, ref IgnoreInfo ii; ignores) {
      if (ii.nonotify && ii.nick.ircMaskEquCI(anick)) return ii;
    }
    return IgnoreInfo.init;
  }

  // check ignore mask (starts with '^')
  // ignore leading '^' in visnick
  // ignore trailing '^' in visnick
  bool checkIgnoreMask (cstring visnick) {
    while (visnick.length && visnick[0] == '^') visnick = visnick[1..$];
    while (visnick.length && visnick[$-1] == '_') visnick = visnick[0..$-1];
    if (visnick.length == 0) return false;
    foreach (immutable idx, ref IgnoreInfo ii; ignores) {
      if (!ii.ignore) continue;
      cstring mask = ii.nick;
      if (mask.length < 2 || mask[0] != '^') continue;
      mask = mask[1..$];
      if (visnick.length != mask.length) continue;
      if (mask.ircStrEquCI(visnick[0..mask.length])) return true;
    }
    return false;
  }

  IgnoreInfo[] allIgnores () {
    IgnoreInfo[] res;
    foreach (const ref ii; ignores) if (ii.ignore) res ~= ii;
    return res;
  }

  IgnoreInfo[] allNonitify () {
    IgnoreInfo[] res;
    foreach (const ref ii; ignores) if (ii.nonotify) res ~= ii;
    return res;
  }

  void setChanBlink (IRCChannel chan, bool v) {
    if (chan is null || chan.name.length == 0) return;
    if (auto opts = chan.name in chanopts) {
      if (opts.noblink == !v) return;
      opts.noblink = !v;
    } else {
      if (v) return;
      ChanOption opts;
      opts.noblink = !v;
      opts.nopopup = !chan.popup;
      chanopts[chan.name] = opts;
    }
    saveChanOptions();
  }

  void setChanPopup (IRCChannel chan, bool v) {
    if (chan is null || chan.name.length == 0) return;
    if (auto opts = chan.name in chanopts) {
      if (opts.nopopup == !v) return;
      opts.nopopup = !v;
    } else {
      if (v) return;
      ChanOption opts;
      opts.noblink = !chan.blink;
      opts.nopopup = !v;
      chanopts[chan.name] = opts;
    }
    saveChanOptions();
  }

  IRCUser findUser(bool addit=false) (cstring anick) {
    if (anick.length == 0) return null;
    if (ircStrEquCI(self.nick, anick)) return self;
    foreach (IRCUser user; users) {
      if (ircStrEquCI(user.nick, anick)) return user;
    }
    static if (addit) {
      auto user = new IRCUser();
      user.server = this;
      user.nick = anick.idup;
      // ignored?
      if (!user.isme) {
        auto ign = findFlagsInfo(anick);
        if (ign.nick.length) {
          user.ignored = ign.ignore;
          user.nonotify = ign.nonotify;
          user.ignoreShort = ign.ignoreShort;
        }
      }
      users.arrayAppend(user);
      return user;
    } else {
      return null;
    }
  }

  IRCChannel findChannel(bool addit=false) (cstring name) {
    if (name.length == 0) return null;
    foreach (IRCChannel chan; chans) if (chan.name.ircStrEquCI(name)) return chan;
    static if (addit) {
      auto jc = new IRCChannel();
      jc.server = this;
      jc.name = name.idup;
      if (auto opts = jc.name in chanopts) {
        jc.blink = !opts.noblink;
        jc.popup = !opts.nopopup;
      }
      chans.arrayAppend(jc);
      return jc;
    } else {
      return null;
    }
  }

public:
  void onEvent (EventNetConnected evt) {
    if (evt.id == connid) onConnected();
  }

  void onEvent (EventNetConnectFailed evt) {
    if (evt.id == connid) onDisconnected();
  }

  void onEvent (EventNetDisconnected evt) {
    if (evt.id == connid) onDisconnected();
  }

  void onEvent (EventNetReceived evt) {
    if (evt.id == connid) onDataReceived(evt.data);
  }

  void onEvent (EventReconnect evt) {
    if (evt.server is this) {
      if (connid == 0) {
        if (!dead) {
          logwritefln("reconnecting to [%s]", address);
          sysmsgf("reconnecting to [%s]", address);
        }
        connect();
      } else {
        logwritefln("reconnecting to [%s] is impossible, we are still connected", address);
        sysmsgf("reconnecting to [%s], we are still connected", address);
      }
    }
  }

  // force disconnect
  void ForceReconnect () {
    connecting = false;
    if (connid != 0) {
      (new EventNetDisconnect(connid)).post;
      connid = 0;
      onDisconnected();
    }
    inbuf.clear();
  }

protected:
  // force disconnect
  void close () {
    connecting = false;
    if (connid != 0) {
      (new EventNetDisconnect(connid)).post;
      connid = 0;
      onDisconnected();
    }
    inbuf.clear();
  }

  protected void sendRaw (const(void)[] data) {
    if (!alive || data.length == 0) return;
    (new EventNetSend(connid, data)).post;
  }

  void onConnected () {
    connecting = false;
    if (self.status != IRCUser.Status.Online) {
      self.status = IRCUser.Status.Online;
      (new EventUserStatusChanged(self)).post;
    }
    sysmsgf("connected to server [%s]", address);
    if (self.nick.length == 0) self.nick = "guest";
    if (realnick.length == 0) realnick = self.nick;

    logwritefln("connected to [%s]...", address);
    newNickOnConflict = null;
    awaitAuth = false;
    if (srvpassword.length) sendf("PASS %s", srvpassword);
    logwritefln("...nick: %s", self.nick);
    sendf("NICK %s", self.nick);
    logwritefln("...user: %s", realnick);
    logwritefln("...host: %s", (hostname.length ? hostname : "*"));
    logwritefln("...realname: %s", (realname.length ? realname : realnick));
    // nobody knows what to send as 2nd and 3rd arg anyway
    sendf("USER %s 0 %s :%s", realnick, (hostname.length ? hostname : "*"), (realname.length ? realname : realnick)); //"Miri"
    // "invisible" mode (only channel buddies can see you)
    //sendf("MODE %s +i", self.nick);
    // if we have no password, send initial commands
    if (!password.length && authtype != "oftc") {
      sendinitialcmds();
    }
  }

  void onDisconnected () {
    connecting = false;
    if (self.status != IRCUser.Status.Offline) {
      self.status = IRCUser.Status.Offline;
      (new EventUserStatusChanged(self)).post;
    }
    connid = 0;
    logwritefln("disconnected from [%s]", address);
    sysmsgf("disconnected from [%s]", address);
    if (!skipreconnect && reconnect) {
      logwritefln("reconnection scheduled for [%s]", address);
      sysmsgf("reconnection scheduled for [%s]", address);
      (new EventReconnect(this)).later(15000);
    }
  }

  // chanlist: list
  void onGotChannelList () {
  }

  void onNoticeNickServ (cstring msg) {
    import iv.strex : startsWith;
    this.logSrvMessage("NickServ", msg);
    //textpane.addLine("NickServ", msg);
    (new EventNickServNotice(this, msg.idup)).post;

    bool sendAuth = false;
    bool gotAuth = false;

    if (authtype == "libera" || authtype == "freenode") {
      if (msg.startsWith("This nickname is registered. Please choose a different nickname, or identify via")) {
        sendAuth = true;
      } else if (msg.startsWith("You are now identified for ")) {
        gotAuth = true;
      }
    } else if (authtype == "oftc") {
      if (msg.startsWith("This nickname is registered and protected.")) {
        //sendAuth = true;
        awaitAuth = true;
      } else if (msg.startsWith("You are connected using SSL and have provided a matching client certificate")) {
        gotAuth = true;
      } else if (msg.startsWith("You are successfully identified as ")) {
        gotAuth = true;
      }
    }

    if (sendAuth) {
      authorize();
      return;
    }

    if (gotAuth && awaitAuth) {
      sendinitialcmds();
      return;
    }
  }

  void onNotice (cstring entity, cstring msg) {
    if (auto user = findUser!true(nickFromMask(entity))) {
      // from some user
      user.logPrivMessage(Clock.currTime, true, msg);
      //user.textpane.addLine(user.visNick, msg, hasMyName(msg) ? TextLine.Hi.ToMe : user.hiType);
      (new EventUserNotice(user, msg.idup, hasMyName(msg))).post;
    } else {
      // from server
      this.logSrvMessage(entity, msg);
      //textpane.addLine(entity, msg);
      (new EventOtherNotice(this, msg.idup, hasMyName(msg))).post;
      //(new EventOtherNotice(this, "XXXXXX", hasMyName(msg))).post;
      if (authtype == "oftc") {
        if (msg.startsWith("*** Your client certificate fingerprint is ")) {
          (new EventOtherNotice(this, "...sending authorization...", hasMyName(msg))).post;
          authorize();
        }
      }
    }
  }

public:
  string getServerEntity () {
    import iv.strex : indexOf;
    if (address.length == 0) return "irc.server";
    auto cpos = address.indexOf(':');
    if (cpos < 0 || cpos >= address.length-1) return "irc.server";
    auto res = address[cpos+1..$];
    cpos = res.indexOf(':');
    if (cpos >= 0) res = res[0..cpos];
    res = res.xstrip;
    if (res.length == 0) return "irc.server";
    return res;
  }

  void connect () {
    this.connectListeners(); // it is safe to do that more than once
    auto medead = dead();
    logwritefln("MEDEAD: %s", medead);
    close();
    skipreconnect = false;
    connecting = false;
    awaitAuth = false;
    if (realnick.length == 0) realnick = self.nick;
    self.nick = realnick;
    newNickOnConflict = null;
    connid = 0;
    if (medead) return;
    import std.format : format;
    import std.file, std.path;
    string cbase = null;
    if (certbase.length) cbase = buildPath(configDir, "certs", certbase);
    auto evt = new EventNetConnect(address, cbase);
    evt.post;
    connid = evt.id;
    connecting = true;
    if (cbase.length) {
      logwritefln("connecting to [%s] (certbase: %s)...", address, cbase);
      sysmsgf("connecting to [%s] (certbase: %s)...", address, cbase);
    } else {
      logwritefln("connecting to [%s]...", address);
      sysmsgf("connecting to [%s]...", address);
    }
  }

  void disconnect () {
    logwritefln("DISCONNECT!");
    skipreconnect = true;
    awaitAuth = false;
    close();
  }

  void sendf(A...) (cstring fmt, A args) {
    import std.format : formattedWrite;
    char[] buf;
    static struct Writer {
      IRCServer srv;
      char[]* sbuf;
      void put (cstring s...) {
        char[8] buf = void;
        foreach (char ch; s) {
          auto sl = srv.inbuf.cp.encodeCharInPlace(buf[], ch);
          foreach (char sch; sl) (*sbuf).arrayAppend(sch);
        }
      }
    }
    auto wr = Writer(this, &buf);
    formattedWrite(wr, fmt, args);
    buf.arrayAppend('\r');
    buf.arrayAppend('\n');
    //logwritefln("xSEND[%s]: <%s>", srvalias, buf);
    sendRaw(buf);
  }

  //FIXME: add separate thing for ACTION CTCP
  void sayTo (cstring unick, cstring msg, bool doWrap) {
    import std.array : split;
    if (unick.length == 0 || msg.length == 0) return;
    enum MaxLen = 240;
    auto duser = findUser!false(unick);
    auto dchan = findChannel!false(unick);
    foreach (/*auto*/ str; msg.split('\n')) {
      if (doWrap && str.length > MaxLen) {
        while (str.length > MaxLen) {
          int sppos = MaxLen;
          while (sppos > 0 && str.ptr[sppos-1] > ' ') --sppos;
          cstring tt;
          if (sppos == 0) {
            tt = str[0..MaxLen];
            str = str[MaxLen..$];
          } else {
            tt = str[0..sppos];
            str = str[sppos..$];
          }
          if (dchan !is null) (new EventChanChat(dchan, self, tt.idup, false)).post;
          if (duser !is null) (new EventPrivChat(duser, tt.idup, ahisname:false)).post;
          sendf("PRIVMSG %s :%s", unick, tt);
        }
        if (str.length) {
          if (dchan !is null) (new EventChanChat(dchan, self, str.idup, false)).post;
          if (duser !is null) (new EventPrivChat(duser, str.idup, ahisname:false)).post;
          sendf("PRIVMSG %s :%s", unick, str);
        }
      } else {
        if (dchan !is null) (new EventChanChat(dchan, self, str.idup, false)).post;
        if (duser !is null) (new EventPrivChat(duser, str.idup, ahisname:false)).post;
        sendf("PRIVMSG %s :%s", unick, str);
      }
    }
  }

protected:
  void parseSpacedList (cstring list, scope void delegate (cstring s) dg) {
    if (dg is null) return;
    while (list.length > 0) {
      if (list.ptr[0] <= ' ') { list = list[1..$]; continue; }
      int ep = 0;
      while (ep < list.length && list.ptr[ep] > ' ') ++ep;
      auto s = list[0..ep];
      assert(s.length);
      dg(s);
      list = list[ep..$];
    }
  }

protected:
  // private k8 commands

  // :nick!xxx K8USERNOTICE me :msg
  // show this message as system notice in opened private chats (but don't open new privchat)
  void on2meK8UserNotice (cstring entity, cstring msg) {
    if (auto ufrom = findUser!false(nickFromMask(entity))) {
      (new EventPrivSysNotice(ufrom, msg.idup)).post;
    }
  }

  // :nick!xxx K8USERNOTICE #chan :msg
  // show this message as system notice in #chan chat (if opened)
  void on2chanK8UserNotice (cstring entity, cstring cname, cstring msg) {
    if (auto ufrom = findUser!false(nickFromMask(entity))) {
      if (auto chan = findChannel!false(cname)) {
        (new EventChanSysNotice(chan, ufrom, msg.idup)).post;
      }
    }
  }

  void sendinitialcmds () {
    import std.string : format;
    logwritefln("sending initial commands at [%s] for '%s'", address, realnick);
    string msg = "*** sending initial commands for '%s'".format(realnick);
    (new EventOtherNotice(this, msg, false/*hasme*/, true/*unimportant*/)).post;
    awaitAuth = false;
    // send other params
    version(send_caps) {
      // just in case
      sendf("CAP REQ :away-notify");
      //sendf("CAP REQ :echo-message"); // freenode cannot into this
      sendf("CAP END");
    }
    if (sendCodepage) sendf("CODEPAGE KOI8-R");
    foreach (string cmd; afterConnect) sendf("%s", cmd);
    foreach (IRCChannel chan; chans) {
      if (chan.rejoin) sendf("JOIN %s", chan.name);
    }
  }

  public void authorize () {
    import std.string : format;
    enum {
      AuthUnknown,
      AuthFreeNode,
      AuthLibera,
      AuthOftc,
    }
    if (password.length) {
      //sysmsgf("authorising '%s' at [%s]", realnick, address);
      int at = AuthUnknown;
           if (authtype == "freenode") at = AuthFreeNode;
      else if (authtype == "libera") at = AuthLibera;
      else if (authtype == "oftc") at = AuthLibera;
      if (at == AuthUnknown) {
        string msg = "*** cannot authorize '%s' at [%s], because authtype is unknown (%s)".format(realnick, address, authtype);
        logwritefln("%s", msg);
        sysmsgf("%s", msg);
        (new EventOtherNotice(this, msg, false/*hasme*/, true/*unimportant*/)).post;
      } else {
        string msg = "*** sending registration info for '%s' (%s)".format(realnick, authtype);
        (new EventOtherNotice(this, msg, false/*hasme*/, true/*unimportant*/)).post;
        logwritefln("sending registration info to [%s] for '%s' (%s)", address, realnick, authtype);
        self.nick = realnick;
        // change nick again (just in case)
        final switch (at) {
          case AuthUnknown: assert(0);
          case AuthFreeNode:
          case AuthLibera:
            // ghost old one
            sendf("PRIVMSG NickServ :ghost %s %s", realnick, password);
            // just in case
            sendf("NICK %s", realnick);
            // identify
            sendf("PRIVMSG NickServ :identify %s", password);
            break;
          case AuthOftc:
            // ghost old one
            sendf("PRIVMSG NickServ :ghost %s %s", realnick, password);
            // identify
            sendf("PRIVMSG NickServ :identify %s %s", realnick, password);
            break;
        }
        // change nick again (just in case)
        sendf("NICK %s", realnick);
        //sendf("MODE %s +i", realnick);
        awaitAuth = true;
      }
    } else {
      logwritefln("no registration info (password) for '%s'", realnick);
      sendinitialcmds();
    }
  }

  // IRC commands

  // :srv 451 * :You have not registered
  void oncmd451 (cstring entity, cstring from, cstring[] args) {
    logwritefln("(451) NOT sending registration info for '%s'", realnick);
    version(none) {
      if (authtype == "freenode") {
        authorize();
      }
    }
  }

  // ERR_NICKNAMEINUSE
  // :srv 433 * nick :in use
  void oncmd433 (cstring entity, cstring from, cstring nick, cstring[] args) {
    if (nick == self.nick && password.length == 0) {
      if (newNickOnConflict.length == 0) newNickOnConflict = self.nick;
      //self.nick ~= "_";
      newNickOnConflict ~= "_";
      logwriteln("nick is used, trying another: <", newNickOnConflict, ">");
      this.logSrvMessage(null, "nick is used, trying another: <"~newNickOnConflict~">");
      sysmsgf("[%s] nick is used, trying another: <%s>", address, newNickOnConflict);
      if (args.length > 1) onNotice(entity, args[1]);
      sendf("NICK %s", newNickOnConflict);
    }
  }

  // RPL_WHOREPLY
  // :srv 352 <me> *<channel> <user> <host> <server> <nick> <H|G>[*][@|+] :<hopcount> <real name>
  void on2me352 (cstring entity, cstring[] args) {
    import std.format : format;
    if (args.length < 5) return;
    // get real name
    cstring s = (args.length > 6 ? args[6] : "???");
    usize spos = 0;
    while (spos < s.length && s[spos] > ' ') ++spos;
    while (spos < s.length && s[spos] <= ' ') ++spos;
    if (spos < s.length) s = s[spos..$];
    // build string
    string msg = format("WHO %s: user <%s>(%s) from host <%s>, on server <%s>, real name is <%s>",
      args[4],/*nick*/
      args[1],/*user*/
      (args.length > 5 ? args[5] : ""),/*flags*/
      args[2],/*host*/
      args[3],/*server*/
      s);/*real name*/
    this.logSrvMessage(null, msg);
    (new EventOtherNotice(this, msg, /*hasMyName(msg)*/false, true/*unimportant*/)).post;
  }

  // RPL_WHOISUSER
  // :srv 311 <me> <nick> <user> <host> * :<real name>
  void on2me311 (cstring entity, cstring[] args) {
    import std.format : format;
    if (args.length < 3) return;
    // build string
    string msg = format("WHOIS %s: user <%s> from host <%s>, real name is <%s>",
      args[0],/*nick*/
      args[1],/*user*/
      args[2],/*host*/
      (args.length > 4 ? args[4] : "???"));/*real name*/
    this.logSrvMessage(null, msg);
    (new EventOtherNotice(this, msg, false, true/*unimportant*/)).post;
  }

  // RPL_WHOISSERVER
  // :srv 312 <me> <nick> <server> :<server info>
  void on2me312 (cstring entity, cstring[] args) {
    import std.format : format;
    if (args.length < 2) return;
    // build string
    string msg = format("SERVER %s: <%s>, info is <%s>",
      args[0],/*nick*/
      args[1],/*server*/
      (args.length > 2 ? args[2] : ""));/*server info*/
    this.logSrvMessage(null, msg);
    (new EventOtherNotice(this, msg, false, true/*unimportant*/)).post;
  }

  // :src NOTICE <me> :msg
  void on2meNotice (cstring entity, cstring msg) {
    if (entity.ircStartsWithCI("NickServ!")) {
      onNoticeNickServ(msg);
    } else {
      onNotice(entity, msg);
    }
  }

  // :src NOTICE <me> :msg
  void oncmdNotice (cstring entity, cstring dest, cstring msg) {
    if (entity.ircStartsWithCI("NickServ!")) {
      onNoticeNickServ(msg);
    } else {
      onNotice(entity, msg);
    }
  }

  // unaway
  // :srv 305 <me> :msg
  void on2me305 (cstring entity, cstring[] args) {
    if (self.status != IRCUser.Status.Online) {
      self.status = IRCUser.Status.Online;
      (new EventUserStatusChanged(self)).post;
    }
  }

  // now away
  // :srv 306 <me> :msg
  void on2me306 (cstring entity, cstring[] args) {
    if (self.status != IRCUser.Status.Away) {
      self.status = IRCUser.Status.Away;
      (new EventUserStatusChanged(self)).post;
    }
  }

  // my mode
  // :srv 221 <me> :modestr
  void on2me221 (cstring entity, cstring mode) {
    if (mode.length < 2 || (mode[0] != '+' && mode[0] != '-')) return;
    bool doset = (mode[0] == '+');
    foreach (char ch; mode[1..$]) {
      if (ch == 'a') {
        if (doset) {
          if (self.status != IRCUser.Status.Away) {
            self.status = IRCUser.Status.Away;
            (new EventUserStatusChanged(self)).post;
          }
        } else {
          if (self.status == IRCUser.Status.Away) {
            self.status = IRCUser.Status.Online;
            (new EventUserStatusChanged(self)).post;
          }
        }
      }
    }
  }

  // :srv MODE <me> :modestr
  void on2meMode (cstring entity, cstring mode) {
    on2me221(entity, mode);
  }

  // :srv MODE #channel rights nick [nick...]
  void on2chanMode (cstring entity, cstring cname, cstring[] args) {
    static char translateModeChar (char ch) @safe nothrow @nogc {
      switch (ch) {
        case 'o': return '@';
        case 'v': return '+';
        default: break;
      }
      return ch;
    }

    if (args.length < 2) return;
    string aa; foreach (cstring s; args) { if (aa.length) aa ~= "|"; aa ~= s; }
    //logwritefln("MODE: ent=<%s>; chan=<%s>; args(%d)=<%s>", entity, cname, cast(int)args.length, aa);
    //sysmsgf("MODE: ent=<%s>; chan=<%s>; args(%d)=<%s>", entity, cname, cast(int)args.length, aa);
    cstring mode = args[0];
    if (mode.length < 2 || (mode[0] != '+' && mode[0] != '-')) return;
    auto chan = findChannel!false(cname);
    if (!chan) return;
    bool doset = (mode[0] == '+');
    usize usridx = 1;
    char lastch = ' ';
    foreach (char ch; mode[1..$]) {
      lastch = ch;
      if (usridx >= args.length) break;
      auto usr = findUser!false(args[usridx]);
      ++usridx;
      if (!usr) continue;
      chan.changeUserMode(usr, translateModeChar(lastch), doset);
    }
    if (lastch != ' ') {
      while (usridx < args.length) {
        auto usr = findUser!false(args[usridx]);
        ++usridx;
        if (!usr) continue;
        chan.changeUserMode(usr, translateModeChar(lastch), doset);
      }
    }
  }

  // :srv MODE <nick> :modestr
  void on2userMode (cstring entity, cstring unick, cstring mode) {
    //logwritefln("*** MODE %s :%s", unick, mode);
    if (mode.length < 2 || (mode[0] != '+' && mode[0] != '-')) return;
    if (auto user = findUser!false(unick)) {
      //logwritefln("MODE for '%s': '%s'", unick, mode);
      bool doset = (mode[0] == '+');
      foreach (char ch; mode[1..$]) {
        if (ch == 'a') {
          if (doset) {
            if (user.status != IRCUser.Status.Away) {
              logwritefln("MODE: '%s' is now away", unick);
              user.status = IRCUser.Status.Away;
              (new EventUserStatusChanged(user)).post;
            }
          } else {
            if (user.status == IRCUser.Status.Away) {
              logwritefln("MODE: '%s' is now not away", unick);
              user.status = IRCUser.Status.Online;
              (new EventUserStatusChanged(user)).post;
            }
          }
        }
      }
    }
  }

  // http://ircv3.net/specs/extensions/away-notify-3.1.html
  // TODO: 2) Execute WHO when joining a channel to capture the current away state of all users in that channel.
  // :who AWAY [:msg]
  void oncmdAway (cstring entity, cstring[] rest) {
    if (auto user = findUser!true(nickFromMask(entity))) {
      bool doset = (rest.length > 0 && rest[0].length);
      if (doset) {
        if (user.status != IRCUser.Status.Away) {
          logwritefln("AWAY: '%s' is now away", user.nick);
          user.status = IRCUser.Status.Away;
          (new EventUserStatusChanged(user)).post;
        }
      } else {
        if (user.status == IRCUser.Status.Away) {
          logwritefln("AWAY: '%s' is now not away", user.nick);
          user.status = IRCUser.Status.Online;
          (new EventUserStatusChanged(user)).post;
        }
      }
    }
  }

  // MOTD text
  // :srv 372 <me> :msg
  void on2me372 (cstring entity, cstring msg) {
    (new EventOtherNotice(this, msg.idup, false, true/*unimportant*/)).post;
  }

  // chan userlist
  // :srv 353 nick = #chan @nick
  void on2me353 (cstring entity, cstring mytype, cstring cname, cstring list) {
    if (mytype != "=" && mytype != "@" && mytype != "+" && mytype != "*") return;
    auto chan = findChannel(cname);
    if (chan is null) return; // we aren't requested this channel
    if (chan.userlistComplete) {
      chan.removeEveryone();
      chan.userlistComplete = false;
    }
    parseSpacedList(list, (cstring s) {
      char mode = ' ';
      if (s[0] == '@' || s[0] == '+') {
        if (s.length < 2) return;
        mode = s[0];
        s = s[1..$];
      }
      if (auto user = findUser!true(s)) chan.enter(user, mode, null, false);
    });
  }

  // chan userlist ends
  // :srv 366 <me> #chan :...
  void on2me366 (cstring entity, cstring cname, cstring[] args) {
    if (auto chan = findChannel(cname)) chan.userlistComplete = true;
  }

  // channel: no topic
  // :srv 331 <me> #chan [:msg]
  void on2me331 (cstring entity, cstring cname, cstring[] args) {
    foreach (IRCChannel chan; chans) {
      if (chan.name.ircStrEquCI(cname)) chan.setTopic(null);
    }
  }

  // channel: topic
  // :srv 332 <me> #chan :msg
  void on2me332 (cstring entity, cstring cname, cstring topic) {
    topic = topic.ircstrip;
    foreach (IRCChannel chan; chans) {
      if (chan.name.ircStrEquCI(cname)) chan.setTopic(topic);
    }
  }

  // channel: topic
  // TOPIC #chan :msg
  void on2chanTopic (cstring entity, cstring cname, cstring topic) {
    topic = topic.ircstrip;
    foreach (IRCChannel chan; chans) {
      if (chan.name.ircStrEquCI(cname)) chan.setTopic(topic);
    }
  }

  // WHOIS channels reply
  // :srv 319 <me> him :#chan @#chan +#chan
  void on2me319 (cstring entity, cstring unick, cstring list) {
    auto user = findUser!true(unick);
    if (user is null) return;
    parseSpacedList(list, (cstring s) {
      char mode = ' ';
      if (s[0] == '@' || s[0] == '+') {
        if (s.length < 2) return;
        mode = s[0];
        s = s[1..$];
      }
      if (s.length < 1 || s[0] != '#') return;
      foreach (IRCChannel chan; chans) {
        if (chan.name.ircStrEquCI(s)) chan.enter!true(user, mode, null, true);
      }
    });
  }

  // :who JOIN what [:msg]
  void on2chanJoin (cstring entity, cstring cname, cstring[] args) {
    auto user = findUser!true(nickFromMask(entity));
    if (user is null) {
      logwriteln("UNKNOWN JOIN: ent<", entity, "> : [", nickFromMask(entity), "]");
      return;
    }
    auto xmsg = (args.length > 0 ? args[0].ircstrip : null);
    // find channel
    if (auto jc = findChannel!true(cname)) {
      if (user.isme) {
        jc.rejoin = true;
        if (!jc.joined) {
          jc.joined = true;
          (new EventJoin(jc)).post;
        }
      } else if (jc.joined) {
        jc.enter!true(user, '?', xmsg.idup);
        // send WHOIS to this user
        sendf("WHOIS %s", user.nick);
      }
    }
  }

  // :who PART what [:msg]
  void on2chanPart (cstring entity, cstring cname, cstring[] args) {
    //logwritefln("<%s> PARTed from %s", entity, cname);
    auto user = findUser!false(nickFromMask(entity));
    if (user is null) {
      logwriteln("UNKNOWN PART: ent<", entity, "> : [", nickFromMask(entity), "]");
      return;
    }
    auto xmsg = (args.length > 0 ? args[0].ircstrip : null);
    // find channel
    if (auto jc = findChannel!false(cname)) {
      if (user.isme) {
        jc.rejoin = false;
        if (jc.joined) {
          jc.joined = false;
          (new EventPart(jc)).post;
        }
      } else if (jc.joined) {
        jc.leave(user, xmsg.idup);
      }
    }
  }

  // :who QUIT [:msg]
  void oncmdQuit (cstring entity, cstring[] args) {
    auto user = findUser!false(nickFromMask(entity));
    if (user is null) {
      logwriteln("UNKNOWN QUIT: ent<", entity, "> : [", nickFromMask(entity), "]");
      return;
    }
    auto xmsg = (args.length > 0 ? args[0].ircstrip : null).idup;
    // remove from all channels
    foreach (IRCChannel chan; chans) chan.leave(user, xmsg);
    // change status to offline
    if (user.status != IRCUser.Status.Offline) {
      user.status = IRCUser.Status.Offline;
      (new EventUserStatusChanged(user)).post;
    }
    user.dead = true;
    if (user.isme) return;
    (new EventUserQuits(user, xmsg)).post;
    // remove from known users
    foreach (immutable idx, IRCUser u; users) {
      if (u is user) {
        foreach (immutable c; idx+1..users.length) users.ptr[c-1] = users.ptr[c];
        users[$-1] = null;
        users.length -= 1;
        users.assumeSafeAppend();
      }
    }
  }

  // :who NICK :newnick
  void oncmdNick (cstring entity, cstring newnick) {
    if (newnick.length == 0) return; // just in case
    auto user = findUser!false(nickFromMask(entity));
    if (user is null) return;
    if (user.isme) {
      assert(user is self);
      if (self.nick == newnick) return;
      self.nick = newnick.idup;
      (new EventMyNickChanged(this)).post;
    } else if (user.nick != newnick) {
      auto old = user.nick;
      user.nick = newnick.idup;
      (new EventUserNickChanged(user, old)).post;
    }
  }

  // :who PRIVMSG <me> :msg
  void on2mePrivMsg (cstring entity, cstring msg) {
    if (msg.length == 0) return; // ignore empty messages
    auto unick = nickFromMask(entity);
    if (unick.length == 0) {
      sysmsgf("PRIVMSG2ME<%s>: %s", entity, msg);
      return;
    }
    // from him
    if (auto ufrom = findUser!true(unick)) {
      (new EventPrivChat(ufrom, msg.idup, ahisname:true)).post;
    }
  }

  // :who PRIVMSG #chan :msg
  void on2chanPrivMsg (cstring entity, cstring cname, cstring msg) {
    if (msg.length == 0) return; // ignore empty messages
    auto unick = nickFromMask(entity);
    if (unick.length == 0) {
      sysmsgf("PRIVMSG2CHAN<%s>: %s %s", entity, cname, msg);
      return;
    }
    // to channel
    if (auto chan = findChannel!false(cname)) {
      if (auto ufrom = findUser!true(unick)) {
        (new EventChanChat(chan, ufrom, msg.idup, hasMyName(msg))).post;
      }
    }
  }

  // :who PRIVMSG dest :msg
  void oncmdPrivMsg (cstring entity, cstring dest, cstring msg) {
    auto unick = nickFromMask(entity);
    if (unick.length == 0) {
      sysmsgf("PRIVMSG<%s>: %s %s", entity, dest, msg);
      return;
    }
    // this should be private message from me (echo; servers doesn't send that, but other protocols can)
    if (auto ufrom = findUser!true(unick)) {
      if (ufrom.isme) {
        if (auto chan = findChannel!false(dest)) {
          // to channel
          (new EventChanChat(chan, ufrom, msg.idup, hasMyName(msg))).post;
        } else if (auto uto = findUser!true(dest)) {
          // to privchat from me
          (new EventPrivChat(uto, msg.idup, ahisname:false)).post;
        }
      }
    }
  }

protected:
  void dispatch (cstring entity, cstring[] args) {
    bool isArgMyNick (cstring s) { return s.ircStrEquCI(self.nick); }
    mixin(DispatcherMixin);
  }

  void onLine (cstring line) {
    if (line.length == 0) return;
    logwritefln("LINE: [%s]", line);
    auto pl = ircParseLine(line);
    if (pl.cmd.length == 0) return;
    if (pl.cmd == "PING") {
      logwritefln("PING RECEIVED: <%s>", pl.argtext);
      sendf("PONG %s", pl.argtext);
      return;
    }
    // "no source" means "from server"
    if (pl.entity.length == 0) pl.entity = getServerEntity();
    dispatch(pl.entity, pl.allargs);
  }

protected:
  void onDataReceived (const(void)[] data) {
    assert(connid != 0);
    inbuf.onData(data, (cstring line) { onLine(line); });
  }
}
