/* Invisible Vector IRC client
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
module miri.log;

import iv.vfs.io;

import miri.types;


// ////////////////////////////////////////////////////////////////////////// //
// hack around "has scoped destruction, cannot build closure"
public void logwrite(A...) (scope lazy A args) { logwriteImpl!(false)(args); }
public void logwriteln(A...) (scope lazy A args) { logwriteImpl!(true)(args); }

public void logwritef(Char:dchar, A...) (const(Char)[] fmt, scope lazy A args) { logwritefImpl!(false, Char)(fmt, args); }
public void logwritefln(Char:dchar, A...) (const(Char)[] fmt, scope lazy A args) { logwritefImpl!(true, Char)(fmt, args); }


// ////////////////////////////////////////////////////////////////////////// //
private class LogLocker {}
private __gshared int logfilefd = -1;
public __gshared bool logtostderr = false;


// ////////////////////////////////////////////////////////////////////////// //
public void logOpenFile (cstring fname) {
  import core.sys.posix.fcntl;
  //import core.stdc.stdio : SEEK_SET, SEEK_END;
  //import core.sys.posix.unistd : lseek, read;
  static if (is(typeof(O_CLOEXEC))) {
    synchronized(LogLocker.classinfo) {
      if (logfilefd >= 0) {
        import core.sys.posix.unistd : close;
        close(logfilefd);
      }
      import std.string : toStringz;
      logfilefd = open(fname.toStringz, O_WRONLY|O_CREAT|O_CLOEXEC|O_APPEND, 0x180/*0o600*/);
    }
  } else {
    pragma(msg, "dmd fcntl is fucked. use aliced instead.");
  }
}


// ////////////////////////////////////////////////////////////////////////// //
private void logwriteImpl(bool donl, A...) (scope lazy A args) {
  try {
    import core.sys.posix.unistd : write;
    import std.format : formattedWrite;
    synchronized(LogLocker.classinfo) {
      if (logfilefd < 0 && !logtostderr) return;
      static struct Writer {
        void put (cstring s...) {
          if (s.length) {
            if (logfilefd >= 0) write(logfilefd, s.ptr, s.length);
            if (logtostderr) write(2, s.ptr, s.length);
          }
        }
      }
      Writer wr;
      foreach (/*auto*/ a; args) formattedWrite(wr, "%s", a);
      static if (donl) wr.put("\n");
    }
  } catch (Exception e) {
    logwritefImpl!true("LOG ERROR: %s", e.msg);
  }
}


private void logwritefImpl(bool donl, Char, A...) (const(Char)[] fmt, scope lazy A args) {
  try {
    import core.sys.posix.unistd : write;
    import std.format : formattedWrite;
    synchronized(LogLocker.classinfo) {
      if (logfilefd < 0 && !logtostderr) return;
      static struct Writer {
        void put (cstring s...) {
          if (s.length) {
            if (logfilefd >= 0) write(logfilefd, s.ptr, s.length);
            if (logtostderr) write(2, s.ptr, s.length);
          }
        }
      }
      Writer wr;
      formattedWrite(wr, fmt, args);
      static if (donl) wr.put("\n");
    }
  } catch (Exception e) {
    logwritefImpl!true("LOG ERROR: %s", e.msg);
  }
}
