/* Invisible Vector IRC client
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
module miri.cfg;

import miri.types;


// ////////////////////////////////////////////////////////////////////////// //
enum ChanTabWidth = 24;
enum NickTabWidth = 24;
int TextPaneWidth;


// ////////////////////////////////////////////////////////////////////////// //
__gshared string configDir;
//__gshared string logFileName;
__gshared string tempBlockFileName;
__gshared bool defaultUtfuck = false;
__gshared bool changeTermTitle = true;


// ////////////////////////////////////////////////////////////////////////// //
void initConfig () {
  import std.file, std.path;
  configDir = expandTilde("~/.local/share/miri");
  mkdirRecurse(configDir);
  tempBlockFileName = buildPath(configDir, ";block.bin");
}


// ////////////////////////////////////////////////////////////////////////// //
string normalizedAbsolutePath (string path) {
  import std.path;
  return buildNormalizedPath(absolutePath(path));
}


// ////////////////////////////////////////////////////////////////////////// //
T normStr2FName(T : cstring) (T str) {
  static if (is(T == typeof(null))) {
    return str;
  } else {
    foreach (char ch; str) {
      if (ch == '/' || ch < ' ') {
        // fucked
        char[] res;
        foreach (char cc; str) {
          if (cc == '/' || cc < ' ') cc = '_';
          res ~= cc;
        }
        return cast(T)res; // it is safe to cast it here
      }
    }
    return str;
  }
}
