/* Invisible Vector IRC client
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
module miri.rndcolor;
private:

import iv.rawtty;

import miri.types;
import miri.util;


////////////////////////////////////////////////////////////////////////////////
// prng
struct RandGen {
nothrow @safe @nogc:
  uint seed = 0x0badf00d;
  uint x = 0x0badf00d;

  this (cstring key) { newSeed(key); }

  void newSeed (uint seed) {
    if (seed == 0) seed = 0x0badf00d;
  }

  void newSeed (cstring key) {
    // jenkins hash
    uint hash = cast(uint)key.length; // seed
    foreach (char ch; key) {
      ubyte c = cast(ubyte)(ch.irc2lower);
      //if (c >= 65 && c <= 90) c += 32; // poor man's locase
      hash = (hash+c)&0xffffffff;
      hash = (hash+((hash<<10)&0xffffffff))&0xffffffff;
      hash ^= (hash>>6);
    }
    hash = (hash+((hash<<3)&0xffffffff))&0xffffffff;
    hash ^= (hash>>11);
    hash = (hash+((hash<<15)&0xffffffff))&0xffffffff;
    // make unsigned
    x = seed = (hash ? hash : 0x0badf00d);
  }

  void reset () {
    x = seed;
  }

  double next () {
    if (x == 0) x = 0x0badf00d;
    x = cast(uint)(cast(long)x*279470273L%4294967291L);
    auto res = x/4294967291.0;
    if (x == 0) x = (seed ? seed : 0x0badf00d);
    return res;
  }
}


////////////////////////////////////////////////////////////////////////////////
// generate random foreground color
//
// Params:
//  bg = {r,g,b} [0..255]
//  nick
//
// Returns:
//  {r,g,b}
public ubyte randomNickColor (cstring nick, ubyte bgcol) nothrow @safe @nogc {
  ubyte br, bg, bb;
  ttyColor2rgb(bgcol, br, bg, bb);
  double bgint = (0.2126*br+0.7152*bg+0.0722*bb)/255.0;
  // set threshold to compare colors. you can tweak this to your liking
  enum threshold = 0.2;
  auto prng = RandGen(nick);
  int count = 42;
  for (;;) {
    import std.math : abs;
    // assuming rnd returns a random float 0-1 inclusive
    auto r = cast(ubyte)(prng.next*255);
    auto g = cast(ubyte)cast(int)(prng.next*255);
    auto b = cast(ubyte)(prng.next*255);
    ubyte cc = ttyRgb2Color(r, g, b);
    ttyColor2rgb(cc, r, g, b);
    auto its = (0.2126*r+0.7152*g+0.0722*b)/255.0;
    if (--count < 1 || abs(its-bgint) > threshold) return cc;
  }
}


/*
import iv.vfs.io;

void main () {
  auto clr = randomNickColor("ketmar", ttyRgb2Color(0x1a, 0x1a, 0x1a));
  ubyte r, g, b;
  ttyColor2rgb(clr, r, g, b);
  writefln("%3s #%02x%02x%02x", clr, r, g, b);
}
*/
