/* Invisible Vector IRC client
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
module miri.netio;
private:

/*
network layer
=============
EventNetXXX

invalid id:
  wait for:
    EventNetInvalidId(UUID uid)

connect to server:
  send:
    EventNetConnect(string addr, string certbase)
    // id field will be set to connection id
  wait for:
    EventNetConnected(UUID uid)
    EventNetConnectFailed(UUID uid)
  addr:
    tcp:addr[:port]

disconnect:
  send:
    EventNetDisconnect(UUID uid)
  wait for:
    EventNetDisconnected(UUID uid)

send data:
  send:
    EventNetSend(UUID uid, const(void)[] data)

receive data:
  wait for:
    EventNetReceived(UUID uid, const(void)[] data)

errors:
  wait for:
    EventNetErrorSend(UUID uid)
    EventNetErrorRecv(UUID uid)
  after error, connection will be closed, and EventNetDisconnected will be sent
*/


// ////////////////////////////////////////////////////////////////////////// //
import core.time;
import std.datetime : SysTime, Clock;
import std.socket;
import iv.sslsocket;

import iv.egtui.tty;
import iv.eventbus;
import iv.rawtty;
import iv.strex;
import iv.utfutil;

import miri.types;
import miri.util;
import miri.log;

import tray.trayicon;
import tray.xkbutils;


// ////////////////////////////////////////////////////////////////////////// //
struct NetChan {
  uint id;
  string address;
  Socket sock;
  bool connecting;
  ubyte[] wrdata;
  SysTime lastiotime;
}

__gshared NetChan[uint] usedIds;
__gshared uint lastid;
__gshared bool wrapped = false;

uint getId () nothrow {
  if (!wrapped) {
    auto res = ++lastid;
    if (res != 0) { usedIds[res] = NetChan(res); return res; }
    wrapped = true;
    lastid = 1;
  }
  // wrapped
  foreach (long count; 0..cast(long)uint.max+1) {
    if (lastid !in usedIds) { usedIds[lastid] = NetChan(lastid); return lastid; }
    if (++lastid == 0) lastid = 1;
  }
  assert(0, "too many connections!");
}


// ////////////////////////////////////////////////////////////////////////// //
public class UnixAddressEx : Address {
private:
  import core.sys.posix.sys.socket : sockaddr, socklen_t, AF_UNIX;
  import core.sys.posix.sys.un : sockaddr_un;

private:
  sockaddr_un sun;

private:
  this () {}

public:
  override @property sockaddr* name () { return cast(sockaddr*)&sun; }
  override @property const(sockaddr)* name () const { return cast(const(sockaddr)*)&sun; }
  override @property socklen_t nameLen () const { return cast(socklen_t)sun.sizeof; }

  this (in char[] path) {
    import core.stdc.string : memset;
    if (path.length == 0) throw new AddressException("empty path not allowed", 0);
    if (path.length > 100) throw new AddressException("path too long", 0);
    memset(&sun, 0, sun.sizeof);
    sun.sun_family = AF_UNIX;
    // create domain socket without FS inode (first byte of path buffer should be zero)
    sun.sun_path[1..1+path.length] = cast(byte[])path[];
  }

  @property string path () const @trusted { import std.string : fromStringz; return (cast(const(char)*)sun.sun_path.ptr+1).fromStringz.idup; }

  override string toString() const { return path; }
}


// ////////////////////////////////////////////////////////////////////////// //
public class EventXKbSetLayout : Event {
  int lay;
  this (int alay) { lay = alay; }
}


// ////////////////////////////////////////////////////////////////////////// //
public class EventNetXXX : Event {
  uint id; // unique network connection id
  this () {}
}

public class EventNetQuery : EventNetXXX { this () {} }
public class EventNetReply : EventNetXXX { this () {} }
public class EventNetError : EventNetReply { this () {} }
public class EventNetInvalidId : EventNetError { this (uint aid) { id = aid; } }

public class EventNetConnect : EventNetQuery {
  string addr;
  string certbase;
  this () {}
  this(TA : cstring, TC : cstring) (TA aaddr, TC acertbase=null) {
    id = getId;
    static if (is(TA == typeof(null))) {
      addr = null;
    } else static if (is(TA == string)) {
      addr = aaddr;
    } else {
      addr = aaddr.idup;
    }
    static if (is(TC == typeof(null))) {
      certbase = null;
    } else static if (is(TC == string)) {
      certbase = acertbase;
    } else {
      certbase = acertbase.idup;
    }
  }
}

public class EventNetConnected : EventNetReply {
  this (uint aid) { id = aid; }
}

public class EventNetConnectFailed : EventNetError {
  this (uint aid) { id = aid; }
}

public class EventNetDisconnect : EventNetQuery {
  this (uint aid) { id = aid; }
}

public class EventNetDisconnected : EventNetReply {
  this (uint aid) { id = aid; }
}

public class EventNetSend : EventNetQuery {
  void[] data;
  this (uint aid, const(void)[] adata) {
    id = aid;
    data = adata.dup;
    /*
    try {
      logwritefln("EventNetSend[%s]: <%s>", id, cast(cstring)data);
    } catch (Exception) {}
    */
  }
}

public class EventNetReceived : EventNetReply {
  void[] data;
  this (uint aid, const(void)[] adata) {
    id = aid;
    data = adata.dup;
    /*
    try {
      logwritefln("EventNetReceived[%s]: <%s>", id, cast(cstring)data);
    } catch (Exception) {}
    */
  }
}


public class EventNetErrorSend : EventNetError {
  this (uint aid) { id = aid; }
}

public class EventNetErrorRecv : EventNetError {
  this (uint aid) { id = aid; }
}


// ////////////////////////////////////////////////////////////////////////// //
// various funny events

// posted just after `netioRun()` is about to enter main loop
public class EventNetStarted : Event {}

// posted just before `netioRun()` is about to terminate
public class EventQuited : Event {}

// post this to ask `netioRun()` to terminate
public class EventQuitRun : Event {}

public class EventTtyResized : Event {}

public class EventTtyKey : Event {
  TtyEvent key;
  this (TtyEvent akey) { key = akey; }
}

// tray icon messages
public class EventTrayBlink : Event {}
public class EventTrayUnBlink : Event {}


// internal tray icon message
private class EventRunTrayBlinkPostp : Event {}


// ////////////////////////////////////////////////////////////////////////// //
// WARNING! DON'T CHANGE DEFAULT VALUES! ALOT OF CODE DEPENDS ON THEM!
public struct Recoder {
  enum CodePage : ubyte {
    koi8u,
    cp1251,
    cp866,
  }
  CodePage codepage = CodePage.koi8u;
  bool utfucked = true;

  @property bool norecoding () const pure nothrow @safe @nogc { return (!utfucked && codepage == CodePage.koi8u); }
  @property void norecoding (bool v) pure nothrow @safe @nogc { utfucked = false; codepage = CodePage.koi8u; }

  // from koi to codepage
  char recodeCharTo (char ch) const pure nothrow @safe @nogc {
    pragma(inline, true);
    return
      codepage == CodePage.cp1251 ? uni2cp1251(koi2uni(ch)) :
      codepage == CodePage.cp866 ? uni2cp866(koi2uni(ch)) :
      ch;
  }

  // from codepage to koi
  char recodeCharFrom (char ch) const pure nothrow @safe @nogc {
    pragma(inline, true);
    return
      codepage == CodePage.cp1251 ? uni2koi!'\x90'(cp12512uni(ch)) :
      codepage == CodePage.cp866 ? uni2koi!'\x90'(cp8662uni(ch)) :
      ch;
  }

  char[] uncodeBuf (cstring buf) {
    return uncodeBufInPlace(buf.dup);
  }

  // buffer will be reused
  char[] uncodeBufInPlace (char[] buf) {
    if (!utfucked && codepage == CodePage.koi8u) {
      // nothing to do
      return buf;
    } else if (utfucked) {
      // recode; reuse inbuf
      int spos = 0, dpos = 0;
      while (spos < buf.length) {
        if (buf.ptr[spos] < 128) {
          buf.ptr[dpos++] = buf.ptr[spos++];
        } else {
          Utf8Decoder udc;
          dchar dch = '?';
          while (spos < buf.length) {
            dch = udc.decode(cast(ubyte)buf.ptr[spos++]);
            if (dch <= dchar.max) break;
          }
          // ltr, rtl, shy
          if (dch != 0x200e && dch != 0x200f && dch != 0x00ad) {
            if (dch == 0x2022) buf.ptr[dpos++] = '\x9e'; // bullet -> middle dot
            else if (dch == 0x2026) { buf.ptr[dpos++] = '\x9e'; buf.ptr[dpos++] = '\x9e'; } // ellipsis; can't add 3rd dot, alas
            else buf.ptr[dpos++] = uni2koi!'\x90'(dch);
          }
        }
      }
      return buf[0..dpos];
    } else {
      // recode; reuse inbuf
      int spos = 0, dpos = 0;
      while (spos < buf.length) {
        if (buf.ptr[spos] < 128) {
          buf.ptr[dpos++] = buf.ptr[spos++];
        } else {
          buf.ptr[dpos++] = recodeCharFrom(buf.ptr[spos++]);
        }
      }
      return buf[0..dpos];
    }
  }

  char[] encodeBuf (cstring buf) {
    char[] res;
    char[8] tmp;
    foreach (char ch; buf) {
      res ~= encodeCharInPlace(tmp[], ch);
    }
    return res;
  }

  // from koi; dest should be at least 4 chars -- for utfuck
  char[] encodeCharInPlace (char[] dest, char ch) {
    if (dest.length < 1) return dest;
    if (ch < 128) { dest.ptr[0] = ch; return dest[0..1]; }
    if (utfucked) {
      char[8] ubuf = void;
      int len = utf8Encode(ubuf[], koi2uni(ch));
      if (len < 1) { ubuf[0] = '?'; len = 1; }
      if (len > dest.length) len = cast(int)dest.length;
      dest[0..len] = ubuf[0..len];
      return dest[0..len];
    } else {
      dest.ptr[0] = (codepage != CodePage.koi8u ? recodeCharTo(ch) : ch);
      return dest[0..1];
    }
  }
}


// ////////////////////////////////////////////////////////////////////////// //
public struct LineCollector {
private:
  char[] inbuf;

public:
  Recoder cp;

  @property Recoder.CodePage codepage () const pure nothrow @safe @nogc { return cp.codepage; }
  @property void codepage (Recoder.CodePage v) pure nothrow @safe @nogc { cp.codepage = v; }

  @property bool utfucked () const pure nothrow @safe @nogc { return cp.utfucked; }
  @property void utfucked (bool v) pure nothrow @safe @nogc { cp.utfucked = v; }

  @property bool norecoding () const pure nothrow @safe @nogc { return cp.norecoding; }
  @property void norecoding (bool v) pure nothrow @safe @nogc { cp.norecoding = v; }

public:
  @disable this (this);

  void clear () {
    delete inbuf;
    inbuf = null;
  }

  void onData (const(void)[] data, scope void delegate (cstring line) linecb) {
    assert(linecb !is null);
    if (data.length) {
      //logwritefln("DATA(%s): [%s]", data.length, cast(cstring)data);
      auto olen = inbuf.length;
      inbuf.arrayGrow(data.length);
      inbuf[olen..$] = cast(cstring)data;
    }
    // split to lines
    while (inbuf.length > 0) {
      int pos = 0;
      bool inCTCP = false;
      while (pos < inbuf.length) {
        auto ch = inbuf.ptr[pos];
        if (ch == 0x01) {
          inCTCP = !inCTCP;
          ++pos;
          continue;
        }
        // process CTCP quoting
        if (inCTCP && ch == 0x10) {
          pos += 2; // skip quote and quoted chars
          continue;
        }
        if (ch == '\n') break;
        ++pos;
      }
      if (pos >= inbuf.length) break;
      // got line
      auto line = inbuf[0..pos];
      ++pos; // skip newline char
      if (line.length && line[$-1] == '\r') line = line[0..$-1];
      // raw dump
      logwritefln("RECV: [%s]", line);
      linecb(cp.uncodeBufInPlace(line));
      assert(pos <= inbuf.length);
      // remove processed line from input buffer
      if (pos < inbuf.length) {
        // partial, shift data
        import core.stdc.string : memmove;
        memmove(inbuf.ptr, inbuf.ptr+pos, inbuf.length-pos);
      }
      inbuf.length -= pos;
      inbuf.assumeSafeAppend;
    }
  }
}


// ////////////////////////////////////////////////////////////////////////// //
// custom protocols
static struct CustomProto {
  // connection callback returns initialized and connected socket or null
  // it also can throw
  // should not pause
  // so connection is successfull even if the underlying protocol implementation
  // is still "in progress"; it should just send IRC commands when it completed
  // it's connection bussiness
  // address is striped of prefix
  alias ConnectDg = Socket delegate (string address);
  string prefix;
  ConnectDg cdg;
}

__gshared CustomProto[string] customprotos; // keyed by prefix (with trailing colon)


public void netRegisterProto (cstring prefix, CustomProto.ConnectDg cdg) {
  import iv.strex : indexOf;
  if (prefix.length == 0) throw new Exception("empty custom protocol prefix");
  if (cdg is null) throw new Exception("empty custom protocol connection callback");
  string pfx = prefix.idup;
  if (pfx[$-1] != ':') {
    if (pfx.indexOf(':') >= 0) throw new Exception("invalid custom protocol prefix");
    pfx ~= ':';
  } else {
    if (pfx.length == 1) throw new Exception("empty custom protocol prefix");
    if (pfx.indexOf(':') != pfx.length-1) throw new Exception("invalid custom protocol prefix");
  }
  customprotos[pfx] = CustomProto(pfx, cdg);
}


// ////////////////////////////////////////////////////////////////////////// //
private __gshared int csigfd = -1;

private void setupSignals () {
  import core.sys.linux.sys.signalfd : signalfd, SFD_NONBLOCK;
  import core.sys.posix.signal : sigset_t, sigemptyset, sigaddset, sigprocmask;
  import core.sys.posix.signal : SIG_BLOCK, SIGTERM, SIGHUP, SIGQUIT, SIGINT, SIGPIPE;
  sigset_t mask;
  sigemptyset(&mask);
  sigaddset(&mask, SIGTERM);
  sigaddset(&mask, SIGHUP);
  sigaddset(&mask, SIGQUIT);
  sigaddset(&mask, SIGINT);
  sigaddset(&mask, SIGPIPE);
  sigprocmask(SIG_BLOCK, &mask, null); //we block the signals
  csigfd = cast(int)signalfd(-1, &mask, SFD_NONBLOCK); // sorry
  if (csigfd < 0) {
    logwriteln("can't create sigfd socket!");
    assert(0, "fuck");
  }
}


// ////////////////////////////////////////////////////////////////////////// //
import iv.x11 : Display, XOpenDisplay, ConnectionNumber, XPending, XNextEvent, XEvent, XFlush;

Display* dpy;
TrayIcon ticon;
bool ticonIsHi = false;
bool ticonBlinking = false;

public @property void trayBlink (bool v) { ticonBlinking = v; }

public @property bool trayBlink () { return ticonBlinking; }

private void prepareTrayIcon () {
  if (dpy !is null) return;

  dpy = XOpenDisplay();
  if (dpy is null) return;

  //logwriteln("xfd: ", ConnectionNumber(dpy));

  writeTrayIcons();
  ticon = new TrayIcon(dpy, TrayIconGrayName);
  /*
  ticon.onMButtonUp = (TrayIcon self) {
    self.hide;
    doQuit = true;
  };
  ticon.show();
  */
}


// ////////////////////////////////////////////////////////////////////////// //
// non-blocking, keepalive packets, one second immediate shutdown
public void setSocketOptions (Socket sock) {
  if (sock !is null) {
    sock.blocking(false);
    try {
      {
        Linger l;
        l.on = 0; // close()/shutdown() should return immediately
        l.time = 1; // background for one second, not more
        sock.setOption(SocketOptionLevel.SOCKET, SocketOption.LINGER, l);
      }
      sock.setOption(SocketOptionLevel.SOCKET, SocketOption.KEEPALIVE, 1);
    } catch (Exception e) {}
  }
}


public void setTimeouts (Socket sock, int seconds) {
  if (sock !is null) {
    if (seconds < 1) seconds = 1;
    import core.sys.posix.sys.time : timeval;
    try {
      timeval timeout;
      timeout.tv_sec = seconds;
      timeout.tv_usec = 0;
      sock.setOption(SocketOptionLevel.SOCKET, SocketOption.RCVTIMEO, (&timeout)[0..1]);
      timeout.tv_sec = seconds;
      timeout.tv_usec = 0;
      sock.setOption(SocketOptionLevel.SOCKET, SocketOption.SNDTIMEO, (&timeout)[0..1]);
      int msecs = seconds*1000;
      sock.setOption(cast(SocketOptionLevel)6/*TCP*/, cast(SocketOption)18/*TCP_USER_TIMEOUT*/, (&msecs)[0..1]);
    } catch (Exception e) {}
  }
}


public bool isSocketError (Socket sock) {
  if (sock !is null) {
    int res;
    sock.getOption(SocketOptionLevel.SOCKET, SocketOption.ERROR, res);
    return (res != 0);
  }
  return true;
}


// ////////////////////////////////////////////////////////////////////////// //
public void netioRun (scope void delegate () drawcb) {
  scope(exit) { import core.memory : GC; GC.collect; GC.minimize; }

  setupSignals();

  auto ttymode = ttyGetMode();
  scope(exit) {
    if (ticon !is null) { ticon.hide; delete ticon; }
    normalScreen();
    ttySetMode(ttymode);
  }
  ttySetRaw();
  altScreen();

  prepareTrayIcon();
  if (ticon !is null) {
    ticonIsHi = false;
    ticonBlinking = false;
    ticon.setImage(TrayIconGrayName);
    ticon.show();
  }
  scope(exit) delete ticon;

  auto setRead = new SocketSet();
  auto setWrite = new SocketSet();
  logwriteln("entering main loop...");

  bool doQuit = false;
  bool doBlinkNow = false;

  uint[] nc2kill;
  scope(exit) delete nc2kill;

  auto qid = addEventListener((EventQuitRun evt) { doQuit = true; });
  scope(exit) removeEventListener(qid);

  auto qidBlink = addEventListener((EventTrayBlink evt) {
    //logwritefln("EventTrayBlink: ticonBlinking=%s; doBlinkNow=%s", ticonBlinking, doBlinkNow);
    if (!ticonBlinking) {
      ticonBlinking = true;
      doBlinkNow = true;
    }
  });
  scope(exit) removeEventListener(qidBlink);

  auto qidUnBlink = addEventListener((EventTrayUnBlink evt) {
    //logwritefln("EventTrayUnBlink: ticonBlinking=%s; doBlinkNow=%s", ticonBlinking, doBlinkNow);
    if (ticonBlinking) {
      ticonBlinking = false;
      doBlinkNow = true;
    }
  });
  scope(exit) removeEventListener(qidUnBlink);

  auto qidTBP = addEventListener((EventRunTrayBlinkPostp evt) {
    //logwritefln("EventRunTrayBlinkPostp: ticonBlinking=%s; doBlinkNow=%s", ticonBlinking, doBlinkNow);
    doBlinkNow = true;
  });
  scope(exit) removeEventListener(qidTBP);

  // connect request
  auto qidConnect = addEventListener((EventNetConnect evt) {
    auto ncp = evt.id in usedIds;
    if (ncp is null) { (new EventNetInvalidId(evt.id)).post; return; }
    ncp.address = evt.addr;
    auto addr = evt.addr;
    auto certbase = evt.certbase;
    Address ia;
    try {
      import std.conv : to;
      // check prefix
      if (addr.startsWithCI("tcp:")) {
        addr = addr[4..$];
        // get port
        auto lcol = addr.lastIndexOf(':');
        if (lcol <= 0) throw new Exception("bad");
        ushort port = addr[lcol+1..$].to!ushort;
        if (port == 0) throw new Exception("bad");
        addr = addr[0..lcol];
        if (addr.length == 0) throw new Exception("bad");
        ia = new InternetAddress(addr, port);
        ncp.sock = new TcpSocket(AddressFamily.INET);
      } else if (addr.startsWithCI("ssl:") || addr.startsWithCI("tls:")) {
        addr = addr[4..$];
        // get port
        auto lcol = addr.lastIndexOf(':');
        if (lcol <= 0) throw new Exception("bad");
        ushort port = addr[lcol+1..$].to!ushort;
        if (port == 0) throw new Exception("bad");
        addr = addr[0..lcol];
        if (addr.length == 0) throw new Exception("bad");
        ia = new InternetAddress(addr, port);
        auto xsk = new SSLClientSocket(AddressFamily.INET, SocketType.STREAM, certbase);
        xsk.manualHandshake = true;
        xsk.sslhostname = addr;
        ncp.sock = xsk;
      } else if (addr.startsWithCI("unixex:")) {
        addr = addr[7..$];
        ia = new UnixAddressEx(addr);
        ncp.sock = new Socket(AddressFamily.UNIX, SocketType.STREAM);
      } else {
        import iv.strex : indexOf;
        auto cpos = addr.indexOf(':');
        if (cpos <= 0 || addr.length-cpos < 2) throw new AddressException("invalid address", 0);
        if (auto cpp = addr[0..cpos+1] in customprotos) {
          ncp.sock = cpp.cdg(addr[cpos+1..$]);
          if (ncp.sock && !ncp.sock.isAlive) ncp.sock = null;
          if (ncp.sock is null) throw new AddressException("connection failed", 0);
          ncp.sock.setSocketOptions();
          ncp.sock.setTimeouts(20); // 20 seconds to connect
          ncp.connecting = false;
          ncp.sock.setTimeouts(60*5); // 5 minutes to i/o
          ncp.lastiotime = Clock.currTime;
          (new EventNetConnected(ncp.id)).post;
          return;
        }
        throw new AddressException("invalid address", 0);
      }
      ncp.sock.setSocketOptions();
      ncp.sock.setTimeouts(20); // 20 seconds to connect
      ncp.connecting = true;
      ncp.lastiotime = Clock.currTime;
      ncp.sock.connect(ia);
    } catch (Exception e) {
      logwritefln("NETIO: connection to [%s] error: %s", evt.addr, e.msg);
      (new EventNetConnectFailed(evt.id)).post;
      nc2kill.arrayAppend(evt.id);
    }
  });
  scope(exit) removeEventListener(qidConnect);

  // disconnect request
  auto qidDisconnect = addEventListener((EventNetDisconnect evt) {
    auto ncp = evt.id in usedIds;
    if (ncp is null) { (new EventNetInvalidId(evt.id)).post; return; }
    if (ncp.sock is null) return; // nothing to do
    // connection in progress? abort it
    if (ncp.connecting) {
      (new EventNetConnectFailed(evt.id)).post;
    } else {
      (new EventNetDisconnected(evt.id)).post;
    }
    if (ncp.sock.isAlive) {
      ncp.sock.shutdown(SocketShutdown.BOTH);
      ncp.sock.close();
    }
    ncp.sock = null;
    nc2kill.arrayAppend(evt.id);
  });
  scope(exit) removeEventListener(qidDisconnect);

  // send data
  auto qidSend = addEventListener((EventNetSend evt) {
    auto ncp = evt.id in usedIds;
    if (ncp is null) { (new EventNetInvalidId(evt.id)).post; return; }
    if (ncp.sock is null || !ncp.sock.isAlive) { (new EventNetErrorSend(ncp.id)).post; return; }
    if (evt.data.length) {
      auto bts = cast(const(ubyte)[])evt.data;
      foreach (ubyte b; bts) ncp.wrdata.arrayAppend(b);
    }
  });
  scope(exit) removeEventListener(qidSend);

  int layCount = -1;
  if (dpy !is null) {
    layCount = dpy.layoutCount;
    if (layCount < 2) layCount = -666;
  }
  int laySwitchTo = -1;

  auto qidXkbSetLay = addEventListener((EventXKbSetLayout evt) {
    if (evt.lay < 0) return;
    if (dpy is null) return;
    if (layCount < 0 || evt.lay >= layCount) return;
    //dpy.setActiveLayout(evt.lay);
    laySwitchTo = evt.lay;
  });
  scope(exit) removeEventListener(qidXkbSetLay);

  void doX11 () {
    // X11?
    if (dpy !is null) {
      if (laySwitchTo >= 0) {
        //logwritefln("doX11: laySwitchTo=%s", laySwitchTo);
        dpy.setActiveLayout(laySwitchTo);
        laySwitchTo = -1;
      }
      bool doFlush = false;
      while (XPending(dpy)) {
        XEvent xev;
        //logwritefln("doX11: event pending...");
        XNextEvent(dpy, &xev);
        //logwritefln("doX11: got event..");
        if (ticon !is null) {
          if (ticon.processEvent(xev)) doFlush = true;
        }
      }
      //if (doFlush) XFlush(dpy);
    }
  }

  ubyte[] rdbuf;
  rdbuf.length = 65536;
  scope(exit) delete rdbuf;

  void sockClose (ref NetChan nc) {
    if (nc.sock is null) return;
    if (nc.sock.isAlive) {
      nc.sock.shutdown(SocketShutdown.BOTH);
      nc.sock.close();
    }
    if (nc.wrdata.length) delete nc.wrdata;
    nc.sock = null;
    nc2kill.arrayAppend(nc.id);
    if (nc.connecting) {
      (new EventNetConnectFailed(nc.id)).post;
    } else {
      (new EventNetDisconnected(nc.id)).post;
    }
  }

  auto nextCollectTime = MonoTime.currTime+5.minutes;

  (new EventNetStarted()).post;

  for (;;) {
    processEvents();
    if (drawcb !is null) drawcb();
    if (doQuit) break;

    // remove dead channels
    if (nc2kill.length) {
      foreach (uint id; nc2kill) {
        if (auto ncp = id in usedIds) {
          if (ncp.sock !is null) {
            if (ncp.sock.isAlive) {
              ncp.sock.shutdown(SocketShutdown.BOTH);
              ncp.sock.close();
            }
            ncp.sock = null;
          }
          if (ncp.wrdata.length) delete ncp.wrdata;
          usedIds.remove(id);
        }
      }
      nc2kill.length = 0;
      nc2kill.assumeSafeAppend; // reuse it
      { import core.memory : GC; GC.collect; GC.minimize; }
      nextCollectTime = MonoTime.currTime+5.minutes;
    } else {
      // do periodical GC
      auto tm = MonoTime.currTime;
      if (tm >= nextCollectTime) {
        { import core.memory : GC; GC.collect; GC.minimize; }
        nextCollectTime = tm+5.minutes;
      }
    }

    setRead.reset();
    setWrite.reset();

    if (csigfd >= 0) setRead.add(cast(socket_t)csigfd); // signals
    setRead.add(cast(socket_t)0); // stdin
    // network channels
    foreach (ref NetChan nc; usedIds.byValue) {
      if (nc.sock !is null && nc.sock.isAlive) {
        if (nc.connecting) {
          setWrite.add(nc.sock);
        } else {
          setRead.add(nc.sock);
          if (nc.wrdata.length) setWrite.add(nc.sock);
        }
      }
    }

    // add X11 fd to set if necessary
    if (dpy !is null) {
      doX11();
      // fix blinking
      if (ticonBlinking) {
        if (doBlinkNow) {
          ticonIsHi = !ticonIsHi;
          if (ticon !is null) {
            ticon.setImage(ticonIsHi ? TrayIconColorName : TrayIconGrayName);
            //XFlush(dpy);
          }
          (new EventRunTrayBlinkPostp()).later(300);
          doBlinkNow = false;
          //logwritefln("doX11: blinked; doBlinkNow=%s", doBlinkNow);
        } else {
          //logwritefln("doX11: NOT blinked; doBlinkNow=%s", doBlinkNow);
        }
      } else if (ticonIsHi) {
        //logwritefln("doX11: no blinking, making icon gray");
        ticonIsHi = false;
        if (ticon !is null) {
          ticon.setImage(TrayIconGrayName);
          //XFlush(dpy);
        }
        doBlinkNow = false;
      }
      doX11();
      setRead.add(cast(socket_t)ConnectionNumber(dpy));
    }
    //if (dpy !is null) XFlush(dpy);

    auto ebdelay = ebusSafeDelay();
    int res;
    if (ebdelay >= 0) {
      import core.time : dur;
      if (ebdelay < 0) ebdelay = int.max/2;
      if (ebdelay < 10) ebdelay = 10;
      if (ebdelay > 32000) ebdelay = 32000;
      res = Socket.select(setRead, setWrite, null, dur!"msecs"(ebdelay));
    } else {
      res = Socket.select(setRead, setWrite, null, 1.seconds);
    }

    if (res < 0) {
      import core.stdc.errno;
      if (errno == EINTR) {
        // select may fail due to WINCH signal, check it
        if (xtNeedReinit) {
          //logwriteln("SIGNAL: WINCH");
          xtReinit();
          (new EventTtyResized()).post;
        }
        continue;
      }
      logwriteln("select FAILED!");
      return;
    }

    if (res == 0) continue; // timeout

    // stdin?
    if (setRead.isSet(cast(socket_t)0)) {
      auto key = ttyReadKey(0, 50);
      if (key.key == TtyEvent.Key.Error) {
        //logwriteln("got TtyEvent.Key.Error");
        // this may be WINCH
        if (xtNeedReinit) {
          xtReinit();
          (new EventTtyResized()).post;
        } else {
          break; // error
        }
      } else if (key.key != TtyEvent.Key.Unknown && key.key != TtyEvent.Key.None) {
        (new EventTtyKey(key)).post;
      }
    }

    // signal?
    if (setRead.isSet(cast(socket_t)csigfd)) {
      import core.sys.posix.unistd : read;
      import core.sys.linux.sys.signalfd : signalfd_siginfo;
      // signal arrived
      bool fatalsignal = true;
      signalfd_siginfo si = void;
      while (read(csigfd, &si, si.sizeof) > 0) {
        //logwriteln("signal: ", si.ssi_signo);
        import core.sys.posix.signal : SIGPIPE;
        if (si.ssi_signo == SIGPIPE) fatalsignal = false;
      }
      if (fatalsignal) break; // just exit
    }

    auto curst = Clock.currTime;
    // process chans
    foreach (ref NetChan nc; usedIds.byValue) {
      if (nc.sock is null) continue;
      if (!nc.sock.isAlive) {
        // the thing that should not be, but...
        (new EventNetDisconnected(nc.id)).post;
        nc.sock = null;
        nc2kill.arrayAppend(nc.id);
        continue;
      }
      // connecting?
      if (nc.connecting) {
        if (setWrite.isSet(nc.sock)) {
          // either connected or error
          if (nc.sock.isSocketError) {
            (new EventNetConnectFailed(nc.id)).post;
            nc.sock = null;
            if (nc.wrdata.length) delete nc.wrdata;
            nc2kill.arrayAppend(nc.id);
          } else if (auto xsk = cast(SSLClientSocket)nc.sock) {
            //FIXME: make this non-blocking
            try {
              logwritefln("doing ssl handshake for [%s]", nc.address);
              xsk.blocking(true);
              xsk.sslHandshake();
              xsk.blocking(false);
              logwritefln("complete ssl handshake for [%s]", nc.address);
              nc.connecting = false;
              (new EventNetConnected(nc.id)).post;
              nc.lastiotime = curst;
            } catch (Exception e) {
              try { xsk.blocking(false); } catch (Exception e) {}
              (new EventNetConnectFailed(nc.id)).post;
              nc.sock = null;
              if (nc.wrdata.length) delete nc.wrdata;
              nc2kill.arrayAppend(nc.id);
            }
          } else {
            nc.connecting = false;
            nc.sock.setTimeouts(60*5); // 5 minutes to i/o
            (new EventNetConnected(nc.id)).post;
            nc.lastiotime = curst;
          }
        }
      } else {
        import core.stdc.errno;
        if (nc.sock !is null && nc.wrdata.length && setWrite.isSet(nc.sock)) {
          try {
            auto r = nc.sock.send(nc.wrdata[]);
            if (r == Socket.ERROR || r < 0) {
              if (/*errno != EINTR &&*/ errno != EAGAIN) {
                (new EventNetErrorSend(nc.id)).post;
                sockClose(nc);
              }
            } else if (r == 0) {
              sockClose(nc);
            } else {
              // sent some bytes
              assert(r <= nc.wrdata.length);
              if (r < nc.wrdata.length) {
                // partial, shift data
                import core.stdc.string : memmove;
                memmove(nc.wrdata.ptr, nc.wrdata.ptr+r, nc.wrdata.length-r);
              }
              nc.wrdata.length -= r;
              nc.wrdata.assumeSafeAppend;
              nc.lastiotime = curst;
            }
          } catch (Exception e) {
            (new EventNetErrorSend(nc.id)).post;
            sockClose(nc);
          }
        }
        if (nc.sock !is null && setRead.isSet(nc.sock)) {
          try {
            auto r = nc.sock.receive(rdbuf[]);
            if (r == Socket.ERROR || r < 0) {
              if (/*errno != EINTR &&*/ errno != EAGAIN) {
                (new EventNetErrorRecv(nc.id)).post;
                sockClose(nc);
              }
            } else if (r == 0) {
              sockClose(nc);
            } else {
              nc.lastiotime = curst;
              (new EventNetReceived(nc.id, rdbuf[0..r])).post;
            }
          } catch (Exception e) {
            (new EventNetErrorRecv(nc.id)).post;
            sockClose(nc);
          }
        }
      }
      if (nc.lastiotime < curst) {
        auto tout = (curst-nc.lastiotime).total!"seconds";
        if ((nc.connecting && tout > 30) || (!nc.connecting && tout > 60*5)) sockClose(nc);
      }
    }
  }
  logwriteln("done");
  // close all connections
  foreach (ref NetChan nc; usedIds.byValue) sockClose(nc);
  // quit event
  (new EventQuited()).post;
  // process posted events
  processEvents();
  // clear everything
  usedIds.clear;
}


// ////////////////////////////////////////////////////////////////////////// //
// put message to "*system*" pane
public class EventSysMsg : Event {
  string msg;
  this () {}
  this (string amsg) { msg = amsg; }
}


public void sysmsgf(A...) (cstring fmt, A args) {
  import std.format : formattedWrite;
  char[] st;
  struct Writer {
    void put (cstring s...) {
      foreach (char ch; s) st.arrayAppend(ch);
    }
  }
  Writer wr;
  formattedWrite(wr, fmt, args);
  //syspane.addLine("<system>", st);
  //logwriteln("<", st, ">");
  (new EventSysMsg(cast(string)st)).post; // it is safe to cast here
}
