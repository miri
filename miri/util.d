/* Invisible Vector IRC client
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
module miri.util;

import miri.types;


// ///////////////////////////////////////////////////////////////////////// //
private static immutable char[256] koi8tolowerTable = [
  '\x00','\x01','\x02','\x03','\x04','\x05','\x06','\x07','\x08','\x09','\x0a','\x0b','\x0c','\x0d','\x0e','\x0f',
  '\x10','\x11','\x12','\x13','\x14','\x15','\x16','\x17','\x18','\x19','\x1a','\x1b','\x1c','\x1d','\x1e','\x1f',
  '\x20','\x21','\x22','\x23','\x24','\x25','\x26','\x27','\x28','\x29','\x2a','\x2b','\x2c','\x2d','\x2e','\x2f',
  '\x30','\x31','\x32','\x33','\x34','\x35','\x36','\x37','\x38','\x39','\x3a','\x3b','\x3c','\x3d','\x3e','\x3f',
  '\x40','\x61','\x62','\x63','\x64','\x65','\x66','\x67','\x68','\x69','\x6a','\x6b','\x6c','\x6d','\x6e','\x6f',
  '\x70','\x71','\x72','\x73','\x74','\x75','\x76','\x77','\x78','\x79','\x7a','\x5b','\x5c','\x5d','\x5e','\x5f',
  '\x60','\x61','\x62','\x63','\x64','\x65','\x66','\x67','\x68','\x69','\x6a','\x6b','\x6c','\x6d','\x6e','\x6f',
  '\x70','\x71','\x72','\x73','\x74','\x75','\x76','\x77','\x78','\x79','\x7a','\x7b','\x7c','\x7d','\x7e','\x7f',
  '\x80','\x81','\x82','\x83','\x84','\x85','\x86','\x87','\x88','\x89','\x8a','\x8b','\x8c','\x8d','\x8e','\x8f',
  '\x90','\x91','\x92','\x93','\x94','\x95','\x96','\x97','\x98','\x99','\x9a','\x9b','\x9c','\x9d','\x9e','\x9f',
  '\xa0','\xa1','\xa2','\xa3','\xa4','\xa5','\xa6','\xa7','\xa8','\xa9','\xaa','\xab','\xac','\xad','\xae','\xaf',
  '\xb0','\xb1','\xb2','\xa3','\xa4','\xb5','\xa6','\xa7','\xb8','\xb9','\xba','\xbb','\xbc','\xad','\xbe','\xbf',
  '\xc0','\xc1','\xc2','\xc3','\xc4','\xc5','\xc6','\xc7','\xc8','\xc9','\xca','\xcb','\xcc','\xcd','\xce','\xcf',
  '\xd0','\xd1','\xd2','\xd3','\xd4','\xd5','\xd6','\xd7','\xd8','\xd9','\xda','\xdb','\xdc','\xdd','\xde','\xdf',
  '\xc0','\xc1','\xc2','\xc3','\xc4','\xc5','\xc6','\xc7','\xc8','\xc9','\xca','\xcb','\xcc','\xcd','\xce','\xcf',
  '\xd0','\xd1','\xd2','\xd3','\xd4','\xd5','\xd6','\xd7','\xd8','\xd9','\xda','\xdb','\xdc','\xdd','\xde','\xdf',
];

private static immutable char[256] koi8toupperTable = [
  '\x00','\x01','\x02','\x03','\x04','\x05','\x06','\x07','\x08','\x09','\x0a','\x0b','\x0c','\x0d','\x0e','\x0f',
  '\x10','\x11','\x12','\x13','\x14','\x15','\x16','\x17','\x18','\x19','\x1a','\x1b','\x1c','\x1d','\x1e','\x1f',
  '\x20','\x21','\x22','\x23','\x24','\x25','\x26','\x27','\x28','\x29','\x2a','\x2b','\x2c','\x2d','\x2e','\x2f',
  '\x30','\x31','\x32','\x33','\x34','\x35','\x36','\x37','\x38','\x39','\x3a','\x3b','\x3c','\x3d','\x3e','\x3f',
  '\x40','\x41','\x42','\x43','\x44','\x45','\x46','\x47','\x48','\x49','\x4a','\x4b','\x4c','\x4d','\x4e','\x4f',
  '\x50','\x51','\x52','\x53','\x54','\x55','\x56','\x57','\x58','\x59','\x5a','\x5b','\x5c','\x5d','\x5e','\x5f',
  '\x60','\x41','\x42','\x43','\x44','\x45','\x46','\x47','\x48','\x49','\x4a','\x4b','\x4c','\x4d','\x4e','\x4f',
  '\x50','\x51','\x52','\x53','\x54','\x55','\x56','\x57','\x58','\x59','\x5a','\x7b','\x7c','\x7d','\x7e','\x7f',
  '\x80','\x81','\x82','\x83','\x84','\x85','\x86','\x87','\x88','\x89','\x8a','\x8b','\x8c','\x8d','\x8e','\x8f',
  '\x90','\x91','\x92','\x93','\x94','\x95','\x96','\x97','\x98','\x99','\x9a','\x9b','\x9c','\x9d','\x9e','\x9f',
  '\xa0','\xa1','\xa2','\xb3','\xb4','\xa5','\xb6','\xb7','\xa8','\xa9','\xaa','\xab','\xac','\xbd','\xae','\xaf',
  '\xb0','\xb1','\xb2','\xb3','\xb4','\xb5','\xb6','\xb7','\xb8','\xb9','\xba','\xbb','\xbc','\xbd','\xbe','\xbf',
  '\xe0','\xe1','\xe2','\xe3','\xe4','\xe5','\xe6','\xe7','\xe8','\xe9','\xea','\xeb','\xec','\xed','\xee','\xef',
  '\xf0','\xf1','\xf2','\xf3','\xf4','\xf5','\xf6','\xf7','\xf8','\xf9','\xfa','\xfb','\xfc','\xfd','\xfe','\xff',
  '\xe0','\xe1','\xe2','\xe3','\xe4','\xe5','\xe6','\xe7','\xe8','\xe9','\xea','\xeb','\xec','\xed','\xee','\xef',
  '\xf0','\xf1','\xf2','\xf3','\xf4','\xf5','\xf6','\xf7','\xf8','\xf9','\xfa','\xfb','\xfc','\xfd','\xfe','\xff',
];

private static immutable ubyte[32] koi8alphaTable = [
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0xfe,0xff,0xff,0x07,0xfe,0xff,0xff,0x07,
  0x00,0x00,0x00,0x00,0xd8,0x20,0xd8,0x20,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,
];


// ///////////////////////////////////////////////////////////////////////// //
char koi8lower (char ch) pure nothrow @trusted @nogc {
  pragma(inline, true);
  return koi8tolowerTable.ptr[cast(int)ch];
}

char koi8upper (char ch) pure nothrow @trusted @nogc {
  pragma(inline, true);
  return koi8toupperTable.ptr[cast(int)ch];
}

bool koi8isalpha (char ch) pure nothrow @trusted @nogc {
  pragma(inline, true);
  return ((koi8alphaTable.ptr[ch/8]&(1<<(ch%8))) != 0);
}


// ///////////////////////////////////////////////////////////////////////// //
T ircstrip(T : cstring) (T str) pure nothrow @trusted @nogc {
  while (str.length && (str.ptr[0] == ' ' || str.ptr[0] == '\t')) str = str[1..$];
  while (str.length && (str[$-1] == ' ' || str[$-1] == '\t')) str = str[0..$-1];
  return str;
}


char irc2lower (char ch) pure nothrow @trusted @nogc {
  pragma(inline, true);
  return
    ch >= 'A' && ch <= 'Z' ? cast(char)(ch-'A'+'a') :
    ch == '[' ? '{' :
    ch == ']' ? '}' :
    ch == '\\' ? '|' :
    ch == '~' ? '^' :
    koi8lower(ch);
}

char irc2upper (char ch) pure nothrow @trusted @nogc {
  pragma(inline, true);
  return
    ch >= 'a' && ch <= 'z' ? cast(char)(ch-'a'+'A') :
    ch == '{' ? '[' :
    ch == '}' ? ']' :
    ch == '|' ? '\\' :
    ch == '^' ? '~' :
    koi8upper(ch);
}

bool ircisalpha (char ch) pure nothrow @trusted @nogc {
  pragma(inline, true);
  return
    (ch >= 'A' && ch <= 'Z') ||
    (ch >= 'a' && ch <= 'z') ||
    ch == '[' || ch == ']' ||
    ch == '{' || ch == '}' ||
    ch == '\\' || ch == '|' ||
    ch == '~' || ch == '^' ||
    ch == '_' ||
    //ch == '@' || ch == '.' // this is not in IRC, but i need it for xmpp
    koi8isalpha(ch);
}

bool ircisdigit (char ch) pure nothrow @trusted @nogc { pragma(inline, true); return (ch >= '0' && ch <= '9'); }

bool ircisalnum (char ch) pure nothrow @trusted @nogc {
  pragma(inline, true);
  return
    (ch >= '0' && ch <= '9') ||
    (ch >= 'A' && ch <= 'Z') ||
    (ch >= 'a' && ch <= 'z') ||
    ch == '[' || ch == ']' ||
    ch == '{' || ch == '}' ||
    ch == '\\' || ch == '|' ||
    ch == '~' || ch == '^' ||
    ch == '_' ||
    //ch == '@' || ch == '.' // this is not in IRC, but i need it for xmpp
    koi8isalpha(ch);
}

bool ircisxdigit (char ch) pure nothrow @trusted @nogc {
  pragma(inline, true);
  return
    (ch >= 'A' && ch <= 'F') ||
    (ch >= 'a' && ch <= 'f') ||
    (ch >= '0' && ch <= '9');
}


// ascii only
bool ircStrEquCI (cstring s0, cstring s1) pure nothrow @trusted @nogc {
  if (s0.length != s1.length) return false;
  foreach (immutable idx, char c0; s0) {
    if (c0.irc2lower != s1.ptr[idx].irc2lower) return false;
  }
  return true;
}


// ascii only
// mask can start with '^'
// drops leading '^' and trailing '_' in nick
bool ircMaskEquCI (cstring mask, cstring nick) pure nothrow @trusted @nogc {
  if (mask.length == 0) return false;
  while (nick.length && nick[0] == '^') nick = nick[1..$];
  while (nick.length && nick[$-1] == '_') nick = nick[0..$-1];
  if (nick.length == 0) return false;
  if (mask[0] == '^') {
    if (mask.length && mask[0] == '^') mask = mask[1..$];
    if (mask.length == 0) return false;
    if (nick.length != mask.length) return false;
    return ircStrEquCI(mask, nick);
  } else {
    return ircStrEquCI(mask, nick);
  }
}


// ascii only
int ircStrCmpCI (cstring s0, cstring s1) pure nothrow @trusted @nogc {
  if (s0.length < s1.length) return -1;
  if (s0.length > s1.length) return 1;
  char c1;
  foreach (immutable idx, char c0; s0) {
    c0 = c0.irc2lower;
    c1 = s1.ptr[idx].irc2lower;
    if (c0 < c1) return -1;
    if (c0 > c1) return 1;
  }
  return 0;
}


bool ircStartsWithCI (cstring str, cstring pat) pure nothrow @trusted @nogc {
  if (pat.length > str.length) return false;
  auto s = cast(const(char)*)str.ptr;
  auto p = cast(const(char)*)pat.ptr;
  foreach (immutable _; 0..pat.length) if (irc2lower(*s++) != irc2lower(*p++)) return false;
  return true;
}


bool ircEndsWithCI (cstring str, cstring pat) pure nothrow @trusted @nogc {
  if (pat.length > str.length) return false;
  auto s = cast(const(char)*)str.ptr+str.length-pat.length;
  auto p = cast(const(char)*)pat.ptr;
  foreach (immutable _; 0..pat.length) if (irc2lower(*s++) != irc2lower(*p++)) return false;
  return true;
}


// ///////////////////////////////////////////////////////////////////////// //
struct IRCLine {
  cstring line;
  cstring argtext; // unparsed command arguments
  cstring entity;
  cstring cmd; // upcased
  cstring[] args;
  cstring[] allargs; // [0] is cmd
}


IRCLine ircParseLine (cstring line) {
  IRCLine res;
  res.line = line;

  void skipSpaces () nothrow @trusted @nogc {
    while (line.length && line.ptr[0] <= ' ') line = line[1..$];
  }

  cstring nextWord () nothrow @trusted @nogc {
    skipSpaces();
    if (line.length == 0) return null;
    usize res = 0;
    while (res < line.length && line.ptr[res] > ' ') ++res;
    auto w = line[0..res];
    while (res < line.length && line.ptr[res] <= ' ') ++res;
    line = line[res..$];
    return w;
  }

  skipSpaces();

  // parse (actually, ignore) tags
  if (line.length && line.ptr[0] == '@') {
    cast(void)nextWord();
  }

  // get entity id
  if (line.length && line.ptr[0] == ':') {
    line = line[1..$];
    if (line.length && line.ptr[0] > ' ') res.entity = nextWord();
  }

  // get command
  cstring cmd = nextWord();
  if (cmd.length < 128) {
    char[128] xcmd;
    foreach (auto idx, char ch; cmd) xcmd[idx] = ch.irc2upper;
    if (xcmd[0..cmd.length] == cmd) {
      res.cmd = cmd;
    } else {
      res.cmd = xcmd[0..cmd.length].idup;
    }
  } else {
    char[] xcmd = new char[cmd.length];
    foreach (auto idx, char ch; cmd) xcmd[idx] = ch.irc2upper;
    res.cmd = cast(string)xcmd; // it is safe to cast here
  }

  res.argtext = line;
  res.allargs ~= res.cmd;
  for (;;) {
    skipSpaces();
    if (line.length == 0) break;
    if (line[0] == ':') {
      res.allargs ~= line[1..$];
      break;
    }
    res.allargs ~= nextWord();
  }
  res.args = res.allargs[1..$];

  return res;
}


// ///////////////////////////////////////////////////////////////////////// //
//void dispatch (cstring entity, cstring[] args)
//version = dispatch_debug;

enum DispatcherMixin = q{
  template isGoodArgs(A...) {
         static if (A.length == 0) enum isGoodArgs = true;
    else static if (is(A[0] == cstring)) enum isGoodArgs = isGoodArgs!(A[1..$]);
    else enum isGoodArgs = false;
  }

  static string buildCall (string name, usize count, bool rest) {
    import std.conv : to;
    string res = name~"(entity";
    foreach (immutable idx; 0..count) res ~= ", args["~idx.to!string~"]";
    if (rest) res ~= ", args["~count.to!string~"..$]";
    res ~= ")";
    return res;
  }

  if (args.length == 0) return;
  auto cmd = args[0];
  args = args[1..$];

  bool doCall(string pfx) () {
    import std.traits, std.meta;
    foreach (string memb; __traits(allMembers, typeof(this))) {
      static if (memb.length > pfx.length && memb[0..pfx.length] == pfx) {
        static if (is(typeof(&__traits(getMember, this, memb)) == function) || is(typeof(&__traits(getMember, this, memb)) == delegate)) {
          alias mem = AliasSeq!(__traits(getMember, this, memb))[0];
          static if (is(ReturnType!mem == void)) {
            alias pars = Parameters!mem;
            static if (pars.length > 0 && variadicFunctionStyle!mem == Variadic.no) {
              static if (isGoodArgs!pars) {
                // fixed number of args
                enum cstr = buildCall(memb, pars.length-1, false);
                static if (is(typeof(mixin(cstr)))) {
                  if (memb[pfx.length..$].ircStrEquCI(cmd) && args.length == pars.length-1) {
                    mixin(cstr~";");
                    return true;
                  }
                }
              } else static if (pars.length > 1 && isGoodArgs!(pars[0..$-1]) && is(pars[$-1] == cstring[])) {
                // entity, args, rest
                enum cstr = buildCall(memb, pars.length-2, true);
                static if (is(typeof(mixin(cstr)))) {
                  if (memb[pfx.length..$].ircStrEquCI(cmd) && args.length >= pars.length-2) {
                    mixin(cstr~";");
                    return true;
                  }
                }
              }
            }
          }
        }
      }
    }
    return false;
  }

  version(dispatch_debug) {
    import std.format : format;
    import iv.strex : quote;
    string rr = "dispatch(%s): <%s> %s".format(args.length, entity, cmd);
    string rrr = rr;
    foreach (immutable idx, cstring s; args) rrr ~= "\n  %s: %s".format(idx, s.quote);
    logwriteln(rrr);
  }

  // to me?
  version(dispatch_debug) logwriteln("  on2me: ", rr);
  if (args.length > 0 && args[0].length && isArgMyNick(args[0])) {
    auto oa = args;
    args = args[1..$];
    if (doCall!"on2me"()) {
      version(dispatch_debug) logwriteln("  on2me-hit: ", rr);
      return;
    }
    args = oa;
  }
  // to channel?
  version(dispatch_debug) logwriteln("  on2chan: ", rr);
  if (args.length > 0 && args[0].length && args[0][0] == '#') {
    if (doCall!"on2chan"()) {
      version(dispatch_debug) logwriteln("  on2chan-hit: ", rr);
      return;
    }
  }
  // to some user?
  version(dispatch_debug) logwriteln("  on2user: ", rr);
  if (args.length > 0 && args[0].length && args[0][0] != '#') {
    if (doCall!"on2user"()) {
      version(dispatch_debug) logwriteln("  on2user-hit: ", rr);
      return;
    }
  }
  // others
  version(dispatch_debug) logwriteln("  oncmd: ", rr);
  if (doCall!"oncmd"()) {
    version(dispatch_debug) logwriteln("  oncmd-hit: ", rr);
  }
};


// ///////////////////////////////////////////////////////////////////////// //
void arrayAppend(T) (ref T[] arr, auto ref T vla) {
  auto optr = arr.ptr;
  arr ~= vla;
  if (arr.ptr !is optr) {
    import core.memory : GC;
    if (arr.ptr is GC.addrOf(arr.ptr)) {
      GC.setAttr(arr.ptr, /*GC.BlkAttr.NO_SCAN|*/GC.BlkAttr.NO_INTERIOR); // less false positives
    }
  }
}


void arrayGrow(T) (ref T[] arr, sptrdiff delta=1) {
  if (delta) {
    auto optr = arr.ptr;
    arr.length += delta;
    if (delta < 0) arr.assumeSafeAppend;
    if (arr.ptr !is optr) {
      import core.memory : GC;
      if (arr.ptr is GC.addrOf(arr.ptr)) {
        GC.setAttr(arr.ptr, /*GC.BlkAttr.NO_SCAN|*/GC.BlkAttr.NO_INTERIOR); // less false positives
      }
    }
  }
}
