/* Invisible Vector IRC client
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
module miri.textpane;

import std.datetime;

import iv.rawtty;
import iv.utfutil;

import iv.egtui;

import iv.eventbus;
import iv.strex;
import iv.vfs.io;

import miri.types;
import miri.cfg;
import miri.rndcolor;
import miri.util;


// ////////////////////////////////////////////////////////////////////////// //
public class EventFocusOut : Event {}
public class EventFocusIn : Event {}


// ////////////////////////////////////////////////////////////////////////// //
string getBrowserCommand (bool forceOpera=false) {
  __gshared string browser;
  if (forceOpera) return "opera";
  if (browser.length == 0) {
    import core.stdc.stdlib : getenv;
    const(char)* evar = getenv("BROWSER");
    if (evar !is null && evar[0]) {
      import std.string : fromStringz;
      browser = evar.fromStringz.idup;
    } else {
      browser = "opera";
    }
  }
  return browser;
}


void openUrl (const(char)[] url, bool forceOpera=false) {
  if (url.length) {
    import std.stdio : File;
    import std.process;
    try {
      auto frd = File("/dev/null");
      auto fwr = File("/dev/null", "w");
      spawnProcess([getBrowserCommand(forceOpera), url.idup], frd, fwr, fwr, null, Config.detached);
    } catch (Exception e) {
      //conwriteln("ERROR executing URL viewer (", e.msg, ")");
    }
  }
}


// ////////////////////////////////////////////////////////////////////////// //
// draw scrollbar in the given tty window
public void drawScrollBar (XtWindow win, int cur, int total) {
  if (win.width < 1 || win.height < 1) return; // it won't be visible anyway
  auto oclr = win.color;
  scope(exit) win.color = oclr;
  win.fg = TtyRgb2Color!(0x00, 0x00, 0x00);
  win.bg = TtyRgb2Color!(0x00, 0x5f, 0xaf);
  int filled;
  if (total < 1) {
    filled = win.height;
  } else {
    if (cur < 0) cur = 0;
    if (cur > total) cur = total;
    filled = (win.height-1)*cur/total;
    if (cur < total && filled == win.height-1) --filled;
  }
  foreach (immutable y; 0..win.height) win.writeCharsAt!true(0, y, 1, (y <= filled ? ' ' : 'a'));
}


// ////////////////////////////////////////////////////////////////////////// //
class TextLine {
  enum Hi {
    Normal,
    Mine,
    ToMe,
    Ignored,
    Action,
    Service,
  }

  string nick; // can be empty
  string text;
  string timestr;
  SysTime time;
  string[] lines; // layouted, w/o nick
  int width; // max width of line
  Hi hi = Hi.Normal;

  //WARNING! `lines` will be unsafely modified!
  void format (int wdt) {
    if (wdt < 1) wdt = 1;
    if (width == wdt) return;
    lines[] = null;
    lines.length = 0;
    lines.assumeSafeAppend;
    width = wdt;

    lines.arrayGrow; // so length is 1 now
    bool wasBlank = false;
    bool lineStart = true;
    //int skipCount = 0;
    int colorState = 0; // 0: none; 1-2: foreground; 3: comma; 4-5: background

    void putChar (char ch) {
      // blanks
      if (ch <= ' ') {
        ch = (ch < ' ' ? '\x9E' : ' '); // middle dot
        if (lineStart) {
          if (lines[$-1].length == width) lines.arrayGrow;
          lines[$-1] ~= ch;
          return;
        }
        // space after word
        wasBlank = true; // it was so blank...
        return;
      }
      // non-blanks
      if (wasBlank) {
        if (lines[$-1].length+2 >= width) {
          lines.arrayGrow;
          lineStart = false; // yep
        } else {
          lines[$-1] ~= ' ';
        }
        wasBlank = false;
        lines[$-1] ~= ch;
        return;
      }
      if (lines[$-1].length+1 >= width) {
        // wrapping
        int pos = cast(int)lines[$-1].length;
        while (pos > 0 && lines[$-1][pos-1] > ' ') --pos;
        if (pos == 0) {
          lines.arrayGrow;
          lineStart = false;
          lines[$-1] ~= ch;
          return;
        }
        lines.arrayGrow;
        lineStart = false;
        lines[$-1] = lines[$-2][pos..$];
        while (pos > 0 && lines[$-2][pos-1] <= ' ') --pos;
        lines[$-2] = lines[$-2][0..pos];
      }
      lines[$-1] ~= ch;
    }

    enum : ubyte {
      Bold = 1,
      Mono,
      Under,
      Over,
      Italic,
      Reverse,
      Color,
      MaxFmt,
    }
    static immutable string fmtchars = "*%_-/|\x9f"; // the last is division sign
    uint flags = 0;

    void toggleFormatting (ubyte type) {
      if (type < 1 || type >= MaxFmt) return;
      if (type-1 >= fmtchars.length) return;
      putChar(fmtchars[type-1]);
      immutable uint mask = 1u<<(type-1);
      if (type != Color) {
        flags ^= mask;
      } else {
        flags |= mask;
      }
    }

    void closeFormatting (ubyte type) {
      if (type < 1 || type >= MaxFmt) return;
      if (type-1 >= fmtchars.length) return;
      immutable uint mask = 1u<<(type-1);
      if (flags&mask) {
        putChar(fmtchars[type-1]);
        flags ^= mask;
      }
    }

    void cancelFormatting () {
      foreach_reverse (ubyte v; Bold..MaxFmt) {
        if (v-1 >= fmtchars.length) continue;
        immutable uint mask = 1u<<(v-1);
        if (flags&mask) putChar(fmtchars[v-1]);
      }
      flags = 0;
    }

    foreach (char ch; text) {
      if (ch == '\n') {
        colorState = 0;
        cancelFormatting();
        lines.arrayGrow;
        wasBlank = false;
        lineStart = true;
        continue;
      }

      if (colorState) {
        import iv.strex : isdigit, isxdigit;
        switch (colorState) {
          case 1: // first digit
            colorState = (isdigit(ch) ? 2 : -2);
            break;
          case 2: // second digit, or comma
            colorState = (ch == ',' ? 4 : isdigit(ch) ? 3 : -1);
            break;
          case 3: // comma
            colorState = (ch == ',' ? 4 : -1); // digit, or crap
            break;
          case 4: // first digit
            colorState = (isdigit(ch) ? 5 : -1);
            break;
          case 5: // second digit
            colorState = (isdigit(ch) ? 0 : -1);
            break;
          default:
            if (colorState >= 16) {
              // hex digits
              colorState += (isxdigit(ch) ? 1 : 0);
              immutable int dnum = colorState-16;
              if (dnum >= 6 || !isxdigit(ch)) {
                colorState = (colorState == 16 ? -2 : -1);
              }
            }
            break;
        }
        if (colorState >= 0) continue;
        if (colorState == -2) closeFormatting(Color); else toggleFormatting(Color);
        colorState = 0;
      }
      //if (skipCount > 0) { --skipCount; continue; }

      if (ch == '\r') continue;
      // special codes
      /* 0x02: bold
       * 0x03: color (with 2 digits after, f.ex. \x0304 = color 4) (and possibly ",d[d]")
       * 0x04: hex color (6 hex digits follows)
       * 0x0f: out/escape (reset formatting)
       * 0x11: monospace
       * 0x16: reverse
       * 0x1d: italic
       * 0x1e: strikethrough
       * 0x1f: underlined
       */
      switch (ch) {
        case '\x02': toggleFormatting(Bold); break;
        case '\x03': colorState = 1; break;
        case '\x04': colorState = 16; break;
        case '\x0f': cancelFormatting(); break;
        case '\x11': toggleFormatting(Mono); break;
        case '\x16': toggleFormatting(Reverse); break;
        case '\x1d': toggleFormatting(Italic); break;
        case '\x1e': toggleFormatting(Over); break;
        case '\x1f': toggleFormatting(Under); break;
        default: putChar(ch); break;
      }
    }
    cancelFormatting();
  }
}


// ////////////////////////////////////////////////////////////////////////// //
public enum TextBG = TtyRgb2Color!(0x2a, 0x2a, 0x2a);
public enum NickBG = TtyRgb2Color!(0x0a, 0x0a, 0x0a);

//public enum TextColor = XtColorFB!(TtyRgb2Color!(0xd0, 0xd0, 0xd0), TextBG);
//public enum TextColor = XtColorFB!(TtyRgb2Color!(0xff, 0xaf, 0x00), TextBG);
public enum TextColor = XtColorFB!(TtyRgb2Color!(0xd2, 0x88, 0x18), TextBG);
//public enum MineTextColor = XtColorFB!(TtyRgb2Color!(0xb2, 0x68, 0x18), TextBG);
public enum ActionTextColor = XtColorFB!(TtyRgb2Color!(0x00, 0xa0, 0xa0), TextBG);
public enum MineTextColor = XtColorFB!(TtyRgb2Color!(0x80, 0x42, 0x00), TextBG);
//public enum ToMeTextColor = XtColorFB!(TtyRgb2Color!(0x54, 0xff, 0x54), 0);
public enum ToMeTextColor = XtColorFB!(TtyRgb2Color!(0x24, 0xcf, 0x24), 0);
public enum IgnoredTextColor = XtColorFB!(TtyRgb2Color!(0x60, 0x60, 0x60), TextBG);
public enum ServiceTextColor = XtColorFB!(TtyRgb2Color!(0x00, 0x40, 0x00), TextBG);

public enum SeparatorColor = XtColorFB!(TtyRgb2Color!(0xd0, 0x00, 0x00), TextBG);


// ////////////////////////////////////////////////////////////////////////// //
class TextPane {
private:
  int mX0, mY0, mWidth, mHeight;

public:
  TextLine[] text; // null: mark
  int bottomSkip; // number of lines to skip at the bottom
  bool active;
  bool hasmark;
  bool centermark;
  bool wasLogMove;
  int maxTextLines = 512;

public:
  this () {
    mX0 = ChanTabWidth;
    mY0 = 1;
    mWidth = TextPaneWidth;
    mHeight = ttyh-mY0;
  }

  int x0 () const pure nothrow @safe @nogc { return mX0; }
  int y0 () const pure nothrow @safe @nogc { return mY0; }
  int width () const pure nothrow @safe @nogc { return mWidth; }
  int height () const pure nothrow @safe @nogc { return mHeight; }

  void x0 (int v) pure nothrow @safe @nogc { mX0 = v; }
  void y0 (int v) pure nothrow @safe @nogc { mY0 = v; }
  void height (int v) pure nothrow @safe @nogc { mHeight = (v < 1 ? 1 : v); }
  void width (int v) {
    if (v < 30) v = 30;
    if (mWidth != v) {
      mWidth = v;
      // reformat
      foreach (TextLine ln; text) {
        ln.width = 0;
        int nickLen = cast(int)ln.nick.length;
        if (nickLen < 15) nickLen = 15;
        int ww = v-nickLen-1-8;
        ln.format(ww);
      }
    }
  }

  void removeExtraLines () {
    if (maxTextLines < 1) return;
    if (text.length <= maxTextLines) return;
    // mark should be kept
    int markpos = -1;
    if (hasmark) {
      foreach (auto idx, TextLine l; text) {
        if (l is null) {
          markpos = cast(int)idx;
          break;
        }
      }
      if (markpos == -1) hasmark = false; // just in case
    }
    int newfirst = cast(int)text.length-maxTextLines;
    if (markpos >= 0 && newfirst > markpos) {
      newfirst = markpos-4;
      if (newfirst < 0) newfirst = 0;
    }
    if (newfirst <= 0) return;
    // kill lines that will be removed
    foreach (immutable c; 0..newfirst) {
      if (text[c] !is null) {
        delete text[c];
        text[c] = null; // just in case
      }
    }
    // copy
    foreach (immutable c; newfirst..text.length) {
      text[c-newfirst] = text[c];
      text[c] = null;
    }
    // shrink
    text.length -= newfirst;
    text.assumeSafeAppend;
  }

  void removeMark () {
    if (!hasmark) return;
    int pos = 0;
    while (pos < text.length) {
      if (text.ptr[pos] is null) {
        foreach (immutable c; pos+1..text.length) text.ptr[c-1] = text.ptr[c];
        text[$-1] = null;
        text.length -= 1;
        text.assumeSafeAppend;
      } else {
        ++pos;
      }
    }
    hasmark = false;
    centermark = false;
    removeExtraLines(); // just in case
  }

  void addMark () {
    if (!hasmark) {
      text ~= null;
      hasmark = true;
      centermark = true;
      wasLogMove = false;
    }
  }

  void addLine (SysTime time, cstring nick, cstring atext, TextLine.Hi hi=TextLine.Hi.Normal, bool unimportant=false) {
    import std.format : format;
    if (!active && !hasmark && !unimportant) {
      // put mark only for interesting messages
      switch (hi) {
        case TextLine.Hi.Normal:
        case TextLine.Hi.ToMe:
          addMark();
          break;
        default:
      }
    }
    auto ln = new TextLine();
    ln.time = time;
    ln.timestr = "[%02s:%02s]".format(ln.time.hour, ln.time.minute);
    ln.nick = nick.idup;
    ln.text = atext.idup;
    ln.hi = hi;
    int nickLen = cast(int)nick.length;
    if (nickLen < 15) nickLen = 15;
    ln.format(mWidth-nickLen-1-8);
    text ~= ln;
    removeExtraLines();
  }

  void addLine (cstring nick, cstring atext, TextLine.Hi hi=TextLine.Hi.Normal, bool unimportant=false) {
    addLine(Clock.currTime, nick, atext, hi, unimportant);
  }

  // this processes CTCPs (only ACTION for now)
  public void putMessage (SysTime time, cstring unick, cstring atext, TextLine.Hi hi=TextLine.Hi.Normal, bool unimportant=false) {
    import iv.strex : indexOf;
    while (atext.length > 0) {
      auto actp = atext.indexOf("\x01ACTION ");
      if (actp < 0) { addLine(time, unick, atext, hi, unimportant); return; }
      if (actp > 0) { addLine(time, unick, atext[0..actp], hi, unimportant); atext = atext[actp..0]; }
      atext = atext[8..$];
      //while (atext.length && atext.ptr[0] <= ' ' && atext.ptr[0] != '\x01') atext = atext[1..$];
      actp = atext.indexOf('\x01');
      cstring act;
      if (actp < 0) { act = atext; atext = null; } else { act = atext[0..actp]; atext = atext[actp+1..$]; }
      addLine(time, unick, act, (hi != TextLine.Hi.Ignored ? TextLine.Hi.Action : hi), unimportant);
    }
  }

  public void putMessage (cstring unick, cstring atext, TextLine.Hi hi=TextLine.Hi.Normal, bool unimportant=false) {
    putMessage(Clock.currTime, unick, atext, hi, unimportant);
  }

  int totallines () {
    int res = 0;
    foreach (TextLine l; text) if (l !is null) res += cast(int)l.lines.length;
    return res;
  }

  void doPageUp () {
    int total = totallines;
    if (total <= mHeight) return;
    centermark = false;
    auto obs = bottomSkip;
    if (mHeight <= 4) {
      bottomSkip += mHeight;
    } else {
      bottomSkip += mHeight-4;
    }
    if (bottomSkip > total-mHeight/2) bottomSkip = total-mHeight/2;
    wasLogMove = (wasLogMove || obs != bottomSkip);
  }

  void doPageDown () {
    if (bottomSkip == 0) removeMark();
    auto obs = bottomSkip;
    if (mHeight <= 4) {
      bottomSkip -= mHeight;
    } else {
      bottomSkip -= mHeight-4;
    }
    if (bottomSkip < 0) bottomSkip = 0;
    centermark = false;
    wasLogMove = (wasLogMove || obs != bottomSkip);
  }

  void doLineUp () {
    int total = totallines;
    if (total <= mHeight) return;
    auto obs = bottomSkip;
    centermark = false;
    bottomSkip += 1;
    if (bottomSkip > total-mHeight/2) bottomSkip = total-mHeight/2;
    wasLogMove = (wasLogMove || obs != bottomSkip);
  }

  void doLineDown () {
    if (bottomSkip == 0) removeMark();
    auto obs = bottomSkip;
    bottomSkip -= 1;
    if (bottomSkip < 0) bottomSkip = 0;
    centermark = false;
    wasLogMove = (wasLogMove || obs != bottomSkip);
  }

  void doCenterMark (bool force=false) {
    if (!hasmark || (!centermark && !force)) return;
    // find mark y
    int markofs = 0;
    foreach_reverse (TextLine l; text) {
      if (l is null) break;
      markofs += cast(int)l.lines.length;
    }
    //auto total = totallines;
    auto obs = bottomSkip;
    if (markofs <= mHeight) {
      bottomSkip = 0;
    } else {
      bottomSkip = markofs-mHeight+mHeight/3;
      if (bottomSkip < 0) bottomSkip = 0;
    }
    centermark = false;
    if (obs != bottomSkip) wasLogMove = false;
  }

  // draw it from the bottom
  void draw () {
    auto win = XtWindow(mX0, mY0, mWidth, mHeight);
    win.color = TextColor;
    win.fill(0, 0, win.width, win.height, ' ');
    if (text.length == 0) return;
    int y = win.height-1+bottomSkip;
    int lnum = cast(int)text.length-1;
    while (y >= 0 && lnum >= 0) {
      auto lnx = text[lnum--];
      if (lnx is null) {
        // separator
        win.color = SeparatorColor;
        win.hline(0, y, win.width);
        --y;
      } else {
        ubyte nickFG = (lnx.nick.length ? randomNickColor(lnx.nick, win.bg) : 7);
        if (lnx.hi == TextLine.Hi.Ignored) nickFG = (IgnoredTextColor>>8)&0xff;
        int nickLen = cast(int)lnx.nick.length;
        if (nickLen < 15) nickLen = 15;
        foreach_reverse (immutable ldx, string s; lnx.lines) {
          // draw nick
          win.bg = NickBG;
          win.fg = nickFG;
          win.writeCharsAt(0, y, nickLen+1+8, ' ');
          if (ldx == 0) {
            win.writeStrAt(nickLen+1+8-1-cast(int)lnx.nick.length, y, lnx.nick);
            win.writeStrAt(0, y, lnx.timestr);
          }
          final switch (lnx.hi) {
            case TextLine.Hi.Normal: win.color = TextColor; break;
            case TextLine.Hi.Mine: win.color = MineTextColor; break;
            case TextLine.Hi.ToMe: win.color = ToMeTextColor; break;
            case TextLine.Hi.Ignored: win.color = IgnoredTextColor; break;
            case TextLine.Hi.Action: win.color = ActionTextColor; break;
            case TextLine.Hi.Service: win.color = ServiceTextColor; break;
          }
          win.writeCharsAt(nickLen+1+8-1, y, win.width, ' ');
          win.writeStrAt(nickLen+1+8, y, s);
          --y;
        }
      }
    }
    if (bottomSkip > 0 || hasmark) {
      win.x0 = win.x0-1;
      win.width = win.width+2;
      auto lcc = totallines;
      win.drawScrollBar(lcc-bottomSkip, lcc);
    }
  }

  // x and y are relative to line, y is never out of bounds
  bool doClickAt (TextLine line, int x, int y) {
    if (!line || !line.lines.length) return false;
    // open everything
    string s = line.lines[0];
    if (line.lines.length > 1) {
      foreach (immutable c; 1..line.lines.length) s ~= line.lines[c];
    }
    bool res = false;
    detectUrl(s, 0, (const(char)[] url, usize spos, usize epos) {
      openUrl(url);
      //ttyBeep();
      res = true;
      return true; // continue
    });
    return res;
  }

  // x and y are absoulte
  // returns `false` if click wasn't eaten
  bool clicked (int x, int y) {
    x -= mX0;
    y -= mY0;
    if (x < 0 || y < 0 || x >= mWidth || y >= mHeight) return false;
    // find the line
    int cy = mHeight-1+bottomSkip;
    int lnum = cast(int)text.length-1;
    while (cy >= 0 && lnum >= 0) {
      auto lnx = text[lnum--];
      // mark?
      if (lnx is null) {
        if (cy == y) return true;
        --cy;
        continue;
      }
      int y0 = cy-cast(int)lnx.lines.length+1;
      if (y >= y0 && y <= cy) {
        if (doClickAt(lnx, x, y-y0)) return true;
        break;
      }
      cy = y0-1;
    }
    ttyBeep();
    return false;
  }
}
