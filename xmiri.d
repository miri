/* Invisible Vector IRC client
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
module xmiri;

import std.datetime;

import iv.rawtty;
import iv.srex;
import iv.strex : indexOf, startsWith, xstrip;
import iv.utfutil;

import iv.eventbus;
import iv.egtui;
import iv.vfs.io;
import iv.tox;

import miri;
import miri.other.tox;
import tray.trayicon;

import iv.libnotify;


// ////////////////////////////////////////////////////////////////////////// //
bool checkMatchFile (const(char)[] msg, string fname) {
  if (fname.length == 0 || msg.length == 0) return false;
  try {
    int maxlen = 0;
    import std.file, std.path;
    auto filterFilename = buildPath(configDir, fname);
    foreach (auto ln; VFile(filterFilename).byLine) {
      ln = ln.xstrip;
      if (ln.length == 0 || ln[0] == '#') continue;
      auto cpos = ln.indexOf(':');
      if (cpos <= 0) continue;
      auto name = ln[0..cpos].xstrip;
      auto value = ln[cpos+1..$].xstrip;
      if (name.length == 0 || value.length == 0) continue;
      if (name == "maxlen") {
        import std.conv : to;
        try { maxlen = value.to!int; } catch (Exception) {}
        if (maxlen > 0 && msg.length > maxlen) return true;
        continue;
      }
      if (name == "substring") {
        if (msg.indexOf(value) >= 0) return true;
        continue;
      }
      if (name == "glob") {
        import std.path : globMatch;
        if (globMatch(msg, value)) return true;
        continue;
      }
    }
  } catch (Exception) {}
  return false;
}

bool isSrvInfoIdiot (const(char)[] msg) {
  if (!msg.startsWith("srvinfo:")) return false;
  msg = msg[8..$].xstrip;
  return checkMatchFile(msg, "srvinfo_idiots.rc");
}


bool isTgIgnore (const(char)[] msg) {
  return checkMatchFile(msg, "tg_ignore.rc");
}


// ////////////////////////////////////////////////////////////////////////// //
void fuckStdErr () {
  import core.sys.posix.unistd : dup2;
  import core.sys.posix.fcntl /*: open*/;
  auto xfd = open("/dev/null", O_WRONLY, 0x1b6/*0o600*/);
  if (xfd < 0) return;
  dup2(xfd, 2); // fuck stderr
}


void showNotify (cstring title, cstring text) {
  Recoder rc;
  rc.utfucked = true;
  auto tt8 = rc.encodeBuf(title~"\0");
  auto tx8 = rc.encodeBuf(text~"\0");
  auto n = notify_notification_new(tt8.ptr, tx8.ptr, "none");
  GError* ge;
  notify_notification_show(n, &ge);
}


// ////////////////////////////////////////////////////////////////////////// //
__gshared TextPane syspane;
__gshared TtyEditor inputOneLine;
__gshared TtyEditor inputMultiLine;
__gshared TtyEditor inputUserFilter;
__gshared bool inputActive = true;
__gshared bool inputModeSingle = true;
__gshared bool doQuit = false;
__gshared bool userFilterFocused = false;


// ////////////////////////////////////////////////////////////////////////// //
/**
 * Build list of suitable autocompletions.
 *
 * Params:
 *  cmd = user-given command prefix
 *  cmdlist = list of all available commands
 *
 * Returns:
 *  null = no matches (empty array)
 *  array with one item: exactly one match
 *  array with more that one item = [0] is max prefix, then list of autocompletes
 *
 * Throws:
 *  Out of memory exception
 */
cstring[] autocompleteFromList (cstring cmd, cstring[] cmdlist...) nothrow @trusted {
  alias usize = size_t;
  if (cmdlist.length == 0) return [cmd];
  cstring found; // autoinit
  usize foundlen, pfxcount; // autoinit
  // first pass: count prefixes, remember command with longest prefix
  foreach (cstring s; cmdlist) {
    if (cmd.length <= s.length) {
      usize pos = cmd.length;
      foreach (immutable idx; 0..cmd.length) if (cmd[idx].irc2lower != s[idx].irc2lower) { pos = idx; break; }
      if (pos == cmd.length) {
        if (s.length > found.length) found = s;
        ++pfxcount;
      }
    }
  }
  if (pfxcount == 0) return null; // nothing was found
  if (pfxcount == 1) return [found]; // one match found
  // we are too many, do something
  cstring[] res;
  res.length = pfxcount+1; // we know size beforehand
  usize respos = 1; // res[0] -- longest prefix, start with [1]
  usize slen = cmd.length; // not longer than found.length
  foreach (cstring s; cmdlist) {
    if (s.length >= slen) {
      usize pos = slen;
      foreach (immutable idx; 0..slen) if (found[idx].irc2lower != s[idx].irc2lower) { pos = idx; break; }
      if (pos == slen) {
        // i found her! remember (and fix) prefix
        res[respos++] = s;
        // do circimcision
        for (; pos < found.length && pos < s.length; ++pos) if (found[pos].irc2lower != s[pos].irc2lower) break;
        if (pos < found.length) {
          found = found[0..pos];
          if (slen > pos) slen = pos;
        }
      }
    }
  }
  // set first item to longest prefix
  res[0] = found;
  return res;
}


// ////////////////////////////////////////////////////////////////////////// //
void autocomplete (TtyEditor ed, IRCChannel chan) {
  if (ed is null) return;
  auto pos = ed.curpos;
  if (pos == 0) return;
  if (pos < ed.textsize && ed[pos].ircisalnum) return;
  if (!ed[pos-1].ircisalnum && ed[pos-1] != '/') return;
  // collect word
  int sp = pos;
  while (sp > 0 && ed[sp-1].ircisalnum) --sp;
  if (sp > 0 && ed[sp-1] == '/') --sp;
  if (pos-sp > 64) return;

  bool atLineStart = true;
  for (int pp = sp; pp > 0; --pp) {
    if (ed[pp-1] > ' ') { atLineStart = false; break; }
  }

  bool atCommand = false;
  for (int pp = 0; pp < ed.textsize; ++pp) {
    auto ch = ed[pp];
    if (ch > ' ') {
      atCommand = (ch == '/');
      break;
    }
  }

  auto startpos = sp; // we may need it later
  char[128] wordbuf;
  int wbpos;
  while (sp < pos) wordbuf[wbpos++] = ed[sp++];
  cstring word = wordbuf[0..wbpos];
  cstring[] aclist;
  // nicks
  string lastText;
  if (word[0] != '/') {
    if (pos < ed.textsize && ed[pos] <= ' ') {
      lastText = (atLineStart ? ":" : ",");
    } else {
      lastText = (atLineStart ? ": " : ", ");
    }
    if (atCommand) lastText = "";
    if (chan !is null) {
      aclist ~= chan.server.self.nick;
      foreach (IRCUser u; chan.users) {
        if (!u.ignored && !u.isme) aclist ~= u.nick;
      }
    }
  } else {
    // commands
    void addCmd (cstring s) {
      if (s.length == 0) return;
      string xn = "/";
      foreach (char ch; s) xn ~= ch.irc2lower;
      foreach (cstring cmd; aclist) if (ircStrEquCI(cmd, xn)) return;
      aclist ~= xn;
      import std.algorithm : sort;
      aclist.sort;
    }
    void addCommands () {
      cstring[] args;
      cstring origtext;
      foreach (string mname; __traits(allMembers, mixin(__MODULE__))) {
        static if (mname.length > 3 && mname[0..3] == "cmd") {
          static if (is(typeof(__traits(getMember, mixin(__MODULE__), mname)) == function)) {
            static if (is(typeof({__traits(getMember, mixin(__MODULE__), mname)(args[0], args[1..$], origtext);}))) {
              addCmd(mname[3..$]);
            } else static if (is(typeof({__traits(getMember, mixin(__MODULE__), mname)(args[0], args[1..$]);}))) {
              addCmd(mname[3..$]);
            }
          }
        }
      }
    }
    // append server aliases
    void addAliases () {
      import std.file, std.path;
      foreach (DirEntry de; dirEntries(configDir, "*.config.rc", SpanMode.shallow)) {
        if (!de.isFile) continue;
        aclist ~= "/"~de.baseName(".config.rc");
      }
    }
    addCommands();
    addAliases();
    lastText = " ";
  }
  aclist = autocompleteFromList(word, aclist);
  if (aclist.length == 0) { ttyBeep; return; }
  if (aclist.length == 1 || aclist[0].length > word.length) {
    //ed.insertText!("end", false)(pos, aclist[0][word.length..$]);
    ed.replaceText!("end", false)(startpos, cast(int)word.length, aclist[0]);
    // add undoable space if this is only completion
    if (aclist.length == 1) {
      ed.insertText!("end", false)(ed.curpos, lastText);
      return;
    }
  }
  // write variants
  chatlist.textpane.addLine(null, "autocomplete options:", TextLine.Hi.Service);
  foreach (cstring s; aclist[1..$]) chatlist.textpane.addLine(null, "  "~s, TextLine.Hi.Service);
}


// ////////////////////////////////////////////////////////////////////////// //
@property TextLine.Hi hiType (IRCUser user) {
  if (user is null) return TextLine.Hi.Normal;
  if (user.ignored) return TextLine.Hi.Ignored;
  if (user.isme) return TextLine.Hi.Mine;
  return TextLine.Hi.Normal;
}


private bool containsCI (const(char)[] str, const(char)[] pat) {
  if (pat.length == 0 || str.length < pat.length) return false;
  foreach (usize pos; 0..str.length-pat.length+1) {
    auto s = str[pos..pos+pat.length];
    bool ok = true;
    foreach (usize n; 0..pat.length) {
      import iv.strex : tolower;
      if (s[n].tolower != pat[n].tolower) {
        ok = false;
        break;
      }
    }
    if (ok) return true;
  }
  return false;
}


// ////////////////////////////////////////////////////////////////////////// //
class ChatList {
  static struct Item {
    // only one of this can be set
    IRCServer srv;
    IRCChannel chan;
    IRCUser user;
    TextPane textpane;

    this (IRCServer asrv) { textpane = new TextPane(); srv = asrv; }
    this (IRCChannel achan) { textpane = new TextPane(); chan = achan; }
    this (IRCUser auser) { textpane = new TextPane(); user = auser; }

    @property bool system () { return (srv is null && chan is null && user is null); }
    @property bool server () { return (srv !is null); }
    @property bool channel () { return (chan !is null); }
    @property bool privchat () { return (user !is null); }
    @property string text () {
      import std.format : format;
      return
        srv !is null ? (srv.srvalias.length ? srv.srvalias : srv.address) :
        chan !is null ? "%s (%s)".format(chan.name, chan.users.length) :
        user !is null ? user.nick :
        "*system*";
    }
  }

  int mCurItem;
  int topitem;
  Item[] items;
  bool focused = true;

  this () {
    items.length = 1; // system pane
    items[0].textpane = syspane;
    syspane.active = true;
    this.connectListeners();
  }

  void moveItemUp (int idx) {
    if (idx < 1 || idx >= items.length || items.length < 2) return;
    Item it = items[idx];
    if (it.system) return;
    int newidx = idx-1;
    if (it.privchat) {
      // privchat
      // move up to channel/server/system
      while (newidx >= 0) {
        Item prevIt = items[newidx];
        if (!prevIt.privchat) { ++newidx; break; }
        --newidx;
      }
    } else if (it.channel) {
      // channel
      // move up to server/system
      while (newidx >= 0) {
        Item prevIt = items[newidx];
        if (!prevIt.channel) { ++newidx; break; }
        --newidx;
      }
    } else if (it.server) {
      // server
      // move this server and all its children before previous server
      while (newidx >= 0) {
        Item prevIt = items[newidx];
        if (prevIt.server) break;
        --newidx;
      }
    } else {
      return;
    }
    if (newidx < 0 || newidx == idx) return; // just in case
    // server move is complex
    if (it.server) {
      // collect server
      Item[] sls;
      int sidx = idx;
      sls ~= items[sidx++];
      while (sidx < items.length) {
        if (items[sidx].server) break;
        sls ~= items[sidx++];
      }
      // remove server
      Item[] newlist = items[0..idx]~items[sidx..$];
      // insert server
      items = newlist[0..newidx]~sls~newlist[newidx..$];
      // fix cursor
      if (mCurItem >= idx && mCurItem < idx+sls.length) {
        mCurItem = mCurItem-idx+newidx;
        if (topitem > mCurItem) topitem = mCurItem;
      }
    } else {
      for (usize n = items.length-2; n >= newidx; --n) items[n+1] = items[n];
      items[newidx] = it;
      if (mCurItem == idx) {
        mCurItem = newidx;
        if (topitem > mCurItem) topitem = mCurItem;
      }
    }
    normCurItem();
  }

  TextPane paneForSystem () {
    foreach (ref Item it; items) if (it.system) return it.textpane;
    return null;
  }

  TextPane paneFor (IRCServer asrv) {
    if (asrv is null) return null;
    foreach (ref Item it; items) if (it.srv is asrv) return it.textpane;
    return null;
  }

  TextPane paneFor (IRCChannel achan) {
    if (achan is null) return null;
    foreach (ref Item it; items) if (it.chan is achan) return it.textpane;
    return null;
  }

  TextPane paneFor (IRCUser auser) {
    if (auser is null) return null;
    foreach (ref Item it; items) if (it.user is auser) return it.textpane;
    return null;
  }

  public {
    void onEvent (EventUserNickChanged evt) {
      foreach (ref Item it; items) {
        bool putNotice = false;
        if (it.channel && it.chan.hasUser(evt.user)) {
          putNotice = true;
        } else if (it.privchat && it.user is evt.user) {
          putNotice = true;
        }
        if (putNotice) {
          it.textpane.addLine(evt.user.visNick, "changed nick from '"~evt.oldnick~"' to '"~evt.user.nick~"'", TextLine.Hi.Service);
        }
      }
    }

    void onEvent (EventJoin/*Part*/ evt) {
      auto idx = ensureChannel(evt.chan);
      if (idx < 0) return;
    }

    void onEvent (EventChanEnter evt) {
      if (evt.user.ignored) return; // don't show enter/leave messages for ignored users
      if (auto pane = paneFor(evt.chan)) {
        string msg = "enters";
        if (evt.msg.length) msg ~= ": "~evt.msg;
        pane.addLine(evt.user.visNick, msg, TextLine.Hi.Service);
      }
    }

    void onEvent (EventChanLeave evt) {
      if (evt.user.ignored) return; // don't show enter/leave messages for ignored users
      if (auto pane = paneFor(evt.chan)) {
        string msg = "leaves";
        if (evt.msg.length) msg ~= ": "~evt.msg;
        pane.addLine(evt.user.visNick, msg, TextLine.Hi.Service);
      }
    }

    private T extractCTCP(T:cstring) (ref T str) {
      auto pos = str.indexOf('\x01');
      if (pos < 0) return null;
      auto epos = pos+1;
      while (epos < str.length) {
        // quoting?
        if (str.ptr[epos] == 0x10) {
          if (str.length-epos < 2) return null;
          epos += 2;
        } else if (str.ptr[epos] == 0x01) {
          break;
        } else {
          ++epos;
        }
      }
      if (epos >= str.length && str.ptr[epos] != 0x01) return null;
      auto res = str[pos+1..epos];
      if (pos == 0) {
        // easy deal
        str = str[epos+1..$];
      } else {
        str = str[0..pos]~str[epos+1..$];
      }
      return res;
    }

    private T extractCTCPKeyword(T:cstring) (ref T str) {
      str = str.ircstrip;
      usize pos = 0;
      while (pos < str.length && str[pos] > ' ') ++pos;
      auto res = str[0..pos];
      str = str[pos..$].ircstrip;
      return res;
    }

    void onEvent (EventChanSysNotice evt) {
      if (evt.chan is null) return;
      foreach (ref Item it; items) {
        if (it.chan is evt.chan) {
          it.textpane.putMessage(evt.time, evt.user.visNick, evt.msg, TextLine.Hi.Service);
        }
      }
    }

    void onEvent (EventPrivSysNotice evt) {
      if (evt.user is null) return;
      foreach (ref Item it; items) {
        if (it.user is evt.user) {
          it.textpane.putMessage(evt.time, evt.user.visNick, evt.msg, TextLine.Hi.Service);
        }
      }
    }

    void onEvent (EventPrivChat evt) {
      if (!hasPrivChat(evt.user)) {
        if (evt.user.ignored) {
          auto ii = evt.user.server.findIgnoreInfo(evt.user.nick);
          if (ii.valid && !ii.allowprivchat) return;
        }
      }

      TextPane upane;

      void openChat (bool forced=false) {
        if (upane !is null) return;
        if (!forced && evt.user.ignored) return;
        ensurePrivChat(evt.user);
        upane = paneFor(evt.user);
      }

      auto msg = evt.msg;
      // CTCP processor
      for (;;) {
        auto ctcp = extractCTCP(msg);
        if (ctcp.ptr is null) break;
        ctcp = ctcp.ircstrip;
        auto kw = extractCTCPKeyword(ctcp);
        if (kw.ircStrCmpCI("ACTION")) {
          if (ctcp.length) {
            openChat(!evt.hisname);
            if (evt.hisname) {
              upane.addLine(evt.time, evt.user.visNick, ctcp, TextLine.Hi.Action);
            } else {
              upane.addLine(evt.time, evt.user.server.self.visNick, ctcp, TextLine.Hi.Action);
            }
          }
        } else if (kw.ircStrCmpCI("VERSION")) {
          if (evt.hisname) {
            logwritefln("*** sending version response to %s", evt.user.nick);
            evt.user.server.sendf("NOTICE %s :\x01VERSION Miriel, The IRC Goddess\x01", evt.user.nick);
          } else {
            logwritefln("*** version query to %s -- echo", evt.user.nick);
          }
        } else if (kw.ircStrCmpCI("CLIENTINFO")) {
          if (evt.hisname) {
            logwritefln("*** sending CLIENTINFO response to %s", evt.user.nick);
            evt.user.server.sendf("NOTICE %s :\x01CLIENTINFO ACTION CLIENTINFO PING SOURCE VERSION\x01", evt.user.nick);
          } else {
            logwritefln("*** CLIENTINFO query to %s -- echo", evt.user.nick);
          }
        } else if (kw.ircStrCmpCI("PING")) {
          if (evt.hisname) {
            logwritefln("*** sending PING response to %s", evt.user.nick);
            evt.user.server.sendf("NOTICE %s :\x01PING %s\x01", evt.user.nick, ctcp);
          } else {
            logwritefln("*** PING query to %s -- echo", evt.user.nick);
          }
        } else if (kw.ircStrCmpCI("SOURCE")) {
          if (evt.hisname) {
            logwritefln("*** sending SOURCE response to %s", evt.user.nick);
            evt.user.server.sendf("NOTICE %s :\x01SOURCE %s\x01", evt.user.nick, "https://repo.or.cz/miri.git");
          } else {
            logwritefln("*** SOURCE query to %s -- echo", evt.user.nick);
          }
        }
      }

      if (msg.ircstrip.length) {
        openChat();
        if (!focused) {
          // blink only for interesting messages
          if (evt.hisname && !evt.user.ignored) {
            showNotify(evt.user.visNick, msg);
            (new EventTrayBlink()).post;
          }
        }
        if (!evt.hisname) {
          // my echo
          if (upane !is null) upane.putMessage(evt.time, evt.user.server.self.visNick, msg, TextLine.Hi.Mine);
        } else {
          if (upane !is null) upane.putMessage(evt.time, evt.user.visNick, msg);
        }
      }
    }

    // put highlighted messages to server pane too
    void onEvent (EventChanChat evt) {
      if (evt.chan.server.dead) return;
      if (evt.chan.joined) ensureChannel(evt.chan);
      auto pane = paneFor(evt.chan);
      auto hi = (evt.hasme ? TextLine.Hi.ToMe : evt.user.hiType);

      auto msg = evt.msg;
      for (;;) {
        auto ctcp = extractCTCP(msg);
        if (ctcp.ptr is null) break;
        ctcp = ctcp.ircstrip;
        auto kw = (ctcp.indexOf(' ') >= 0 ? ctcp[0..ctcp.indexOf(' ')] : ctcp);
        ctcp = ctcp[kw.length..$].ircstrip;
        // ignore VERSION request sent to channel
        if (kw == "ACTION") {
          if (pane !is null && ctcp.length) {
            if (evt.user.ignored) ctcp = "*** "~ctcp;
            pane.addLine(evt.time, evt.user.visNick, ctcp, (evt.user.ignored ? TextLine.Hi.Ignored : TextLine.Hi.Action));
          }
        }
      }

      bool srvinfo = msg.startsWith("srvinfo:");

      // idiots
      if (srvinfo && isSrvInfoIdiot(msg)) return;

      // convert "tg: <nick> text"
      string visNick = evt.user.visNick;
      if (!srvinfo && msg.startsWith("tg: <")) {
        auto epos = msg.indexOf('>');
        if (epos >= 0) {
          string nn = msg[5..epos].xstrip;
          msg = (epos+1 < msg.length ? msg[epos+1..$].xstrip : null);
          if (msg.length == 0) msg = "[fooboo]";
          if (nn.length != 0) {
            visNick = "^";
            foreach (immutable idx, char ch; nn) {
              if (!ircisalnum(ch)) {
                if (visNick.length == 1) continue;
                if (visNick[$-1] == '_') continue;
                visNick ~= '_';
              } else {
                visNick ~= ch;
              }
            }
            while (visNick.length > 1 && visNick[$-1] == '_') visNick = visNick[0..$-1];
            if (visNick.length == 1) visNick ~= "dumbass^";
          }
        }
      }

      if (msg.indexOf("tg: ") >= 0 && isTgIgnore(msg)) return;

      // custom ignore
      if (evt.chan.server.checkIgnoreMask(visNick)) return;

      if (pane !is null && msg.ircstrip.length) pane.putMessage(evt.time, visNick, msg, hi);

      // ignore "srvinfo:" messages
      if (srvinfo) return;

      // if this is highlighted message to inactive channel, put it to server text pane
      if (evt.user.isme) return;
      if (evt.user.ignored) return;

      if (!focused && pane !is null && !pane.active) {
        if ((evt.chan.blink && !evt.user.nonotify) || hi == TextLine.Hi.ToMe) {
          // blink only for interesting messages
          switch (hi) {
            case TextLine.Hi.Normal:
            case TextLine.Hi.ToMe:
              (new EventTrayBlink()).post;
              break;
            default:
          }
        }
        if ((evt.chan.popup && !evt.user.nonotify) || hi == TextLine.Hi.ToMe) {
          // blink only for interesting messages
          switch (hi) {
            case TextLine.Hi.Normal:
            case TextLine.Hi.ToMe:
              if (msg.ircstrip.length) showNotify(evt.chan.name~" -- "~visNick, msg);
              break;
            default:
          }
        }
      }
      if (pane !is null && pane.active /*&& !pane.hasmark*/) return;
      if (!evt.hasme) return;
      if (auto spane = paneFor(evt.user.server)) {
        spane.putMessage(evt.time, visNick, evt.msg, TextLine.Hi.ToMe);
      }
    }

    void onEvent (EventChanTopic evt) {
      if (evt.chan.server.dead) return;
      if (evt.chan.joined) ensureChannel(evt.chan);
      auto pane = paneFor(evt.chan);
      if (pane is null || pane !is textpane) return;
      // do something here, but for now topic will be drawn automatically
    }

    void onEvent (EventNickServNotice evt) {
      if (evt.server.dead) return;
      if (auto pane = paneFor(evt.server)) {
        pane.putMessage(evt.time, "NickServ", evt.msg);
      }
    }

    void onEvent (EventOtherNotice evt) {
      if (evt.server.dead) return;
      if (auto pane = paneFor(evt.server)) {
        pane.putMessage(evt.time, null, evt.msg, (evt.hasme ? TextLine.Hi.ToMe : TextLine.Hi./*Normal*/Service), evt.unimportant);
      }
    }

    void onEvent (EventUserNotice evt) {
      if (evt.user.server.dead) return;
      if (auto pane = paneFor(evt.user.server)) {
        pane.putMessage(evt.time, evt.user.visNick, evt.msg, (evt.hasme ? TextLine.Hi.ToMe : evt.user.hiType));
      }
    }

    void onEvent (EventFocusOut evt) {
      focused = false;
      textpane.active = false;
      //textpane.addLine(null, "focusout", TextLine.Hi.Service);
    }

    void onEvent (EventFocusIn evt) {
      focused = true;
      if (!textpane.active) {
        if (!textpane.wasLogMove) textpane.doCenterMark(true);
      }
      textpane.active = true;
      (new EventTrayUnBlink()).post;
      //textpane.addLine(null, "focusout", TextLine.Hi.Service);
    }
  }

  @property TextPane textpane () {
    if (items.length == 0) return syspane;
    if (mCurItem < 0) return items[0].textpane;
    if (mCurItem >= items.length) return items[$-1].textpane;
    return items[mCurItem].textpane;
  }

  @property IRCServer cursrv(bool donorm=true) () {
    static if (donorm) normCurItem();
    if (mCurItem < 0 || mCurItem >= items.length) return null;
    if (items[mCurItem].server) return items[mCurItem].srv;
    if (items[mCurItem].channel) return items[mCurItem].chan.server;
    if (items[mCurItem].privchat) return items[mCurItem].user.server;
    return null;
  }

  @property IRCChannel curchan () {
    normCurItem();
    if (mCurItem < 0 || mCurItem >= items.length) return null;
    if (items[mCurItem].channel) return items[mCurItem].chan;
    return null;
  }

  @property IRCUser curpriv () {
    normCurItem();
    if (mCurItem < 0 || mCurItem >= items.length) return null;
    if (items[mCurItem].user) return items[mCurItem].user;
    return null;
  }

  bool sendMsg (cstring msg) {
    normCurItem();
    if (mCurItem < 0 || mCurItem >= items.length) return false;
    // send to channel?
    IRCServer srv;
    IRCChannel dchan;
    IRCUser duser;
    cstring unick;
    if (items[mCurItem].channel) {
      dchan = items[mCurItem].chan;
      if (!dchan.joined) return false;
      srv = dchan.server;
      unick = dchan.name;
    } else if (items[mCurItem].privchat) {
      duser = items[mCurItem].user;
      if (duser.dead) {
        sysmsgf("can't send message (user '%s' is dead):\n%s", duser.nick, msg);
        return false;
      }
      srv = duser.server;
      unick = duser.nick;
    }
    if (srv is null) {
      sysmsgf("can't send message (no server):\n%s", msg);
      return false;
    }
    if (!srv.alive) {
      sysmsgf("can't send message ([%s] is dead):\n%s", srv.address, msg);
      return false;
    }

    bool doWrap = true;
    if (msg.length >= 2 && msg[0..2] == "::") {
      doWrap = false;
      msg = msg[2..$];
      while (msg.length && msg[0] == ' ') msg = msg[1..$];
    }

    //if (dchan !is null) dchan.logChatMessageF(Clock.currTime, "<%s> %s", srv.self.nick, msg);
    //if (duser !is null) duser.logChatMessageF(Clock.currTime, true, "%s", msg);
    if (dchan !is null) dchan.say(msg, doWrap);
    if (duser !is null) duser.say(msg, doWrap);
    return true;
  }

  // insert before pos
  private void insertItemBefore (Item it, int pos) {
    if (pos < 0) pos = 0;
    if (pos >= items.length) {
      items.arrayAppend(it);
    } else {
      if (mCurItem >= pos) ++mCurItem;
      items.arrayGrow;
      foreach_reverse (immutable c; pos..items.length-1) items[c+1] = items[c];
      items[pos] = it;
    }
    normCurItem();
  }

  IRCServer findAndActivateServerByAlias (IRCServer xsrv) {
    IRCServer res;
    activateServerByAlias!true(xsrv, true, &res);
    return res;
  }

  bool activateServerByAlias(bool addit=false) (IRCServer xsrv, bool activate=true, IRCServer* sout=null) {
    if (sout !is null) *sout = null;
    if (xsrv is null) return false;
    foreach (immutable idx, ref Item it; items) {
      if (it.server && it.srv.srvalias == xsrv.srvalias) {
        if (activate) {
          if (mCurItem >= 0 && mCurItem < items.length) items[mCurItem].textpane.active = false;
          mCurItem = cast(int)idx;
          items[mCurItem].textpane.active = true;
          //(new EventXKbSetLayout(items[mCurItem].srv.forceXKbLayout)).post;
        }
        if (sout !is null) *sout = it.srv;
        normCurItem();
        return true;
      }
    }
    static if (addit) {
      insertItemBefore(Item(xsrv), int.max);
      if (activate) {
        if (mCurItem >= 0 && mCurItem < items.length) items[mCurItem].textpane.active = false;
        mCurItem = cast(int)items.length-1;
        items[mCurItem].textpane.active = true;
        //(new EventXKbSetLayout(items[mCurItem].srv.forceXKbLayout)).post;
        normCurItem();
      }
      if (sout !is null) *sout = xsrv;
    }
    return false;
  }

  void doCloseTab (int tabidx) {
    if (tabidx < 1 || tabidx >= items.length) return;
    if (items[tabidx].server) {
      auto srv = items[tabidx].srv;
      // close all channels and private chats for this server
      for (;;) {
        bool again = false;
        foreach (immutable idx, ref it; items) {
          if (it.channel && it.chan.server is srv) {
            doCloseTab(cast(int)idx);
            again = true;
            break;
          }
        }
        if (!again) break;
      }
      srv.disconnect();
      srv.markDead();
      tabidx = 1;
      while (tabidx < items.length && items[tabidx].srv !is srv) ++tabidx;
      if (tabidx >= items.length) return;
    } else if (items[tabidx].chan) {
      if (items[tabidx].chan.joined) {
        items[tabidx].chan.server.sendf("PART %s", items[tabidx].chan.name);
      }
    } else if (items[tabidx].privchat) {
    } else {
      return;
    }
    items[tabidx].textpane.active = false;
    foreach (immutable c; tabidx+1..items.length) items[c-1] = items[c];
    items[$-1] = Item.init;
    items.length -= 1;
    items.assumeSafeAppend;
    if (mCurItem >= tabidx) {
      --mCurItem;
      normCurItem();
      items[mCurItem].textpane.active = true;
    }
  }

  void doCloseCurrentTab () {
    doCloseTab(mCurItem);
  }

  private int ensureChannel (IRCChannel xchan) {
    if (xchan is null) return -1;
    foreach (immutable idx, ref Item it; items) {
      if (it.channel && it.chan is xchan) return cast(int)idx;
    }
    // not found, create new channel item in current server
    activateServerByAlias!true(xchan.server, false);
    // find server item
    int srvidx = 0;
    while (srvidx < items.length) {
      if (items[srvidx].server && items[srvidx].srv is xchan.server) break;
      ++srvidx;
    }
    if (srvidx >= items.length) assert(0, "wtf?!");
    // add channel, sorted
    int pos = srvidx+1;
    while (pos < items.length) {
      if (!items[pos].channel) break;
      if (items[pos].chan.name.ircStrCmpCI(xchan.name) > 0) break;
      ++pos;
    }
    insertItemBefore(Item(xchan), pos);
    return pos;
  }

  private bool hasPrivChat (IRCUser xuser) {
    if (xuser is null) return false;
    foreach (immutable idx, ref Item it; items) {
      if (it.privchat && it.user is xuser) return true;
    }
    return false;
  }

  private int ensurePrivChat(bool activate=false) (IRCUser xuser) {
    if (xuser is null) return -1;
    foreach (immutable idx, ref Item it; items) {
      if (it.privchat && it.user is xuser) return cast(int)idx;
    }
    // not found, create new channel item in current server
    activateServerByAlias!true(xuser.server, false);
    // find server item
    int srvidx = 0;
    while (srvidx < items.length) {
      if (items[srvidx].server && items[srvidx].srv is xuser.server) break;
      ++srvidx;
    }
    if (srvidx >= items.length) assert(0, "wtf?!");
    ++srvidx;
    // skip channels
    while (srvidx < items.length && items[srvidx].channel) ++srvidx;
    // add privchat, sorted
    int pos = srvidx;
    while (pos < items.length) {
      if (!items[pos].privchat) break;
      if (items[pos].user.nick.ircStrCmpCI(xuser.nick) > 0) break;
      ++pos;
    }
    insertItemBefore(Item(xuser), pos);
    if (activate && mCurItem != pos) {
      if (mCurItem >= 0 && mCurItem < items.length) items[mCurItem].textpane.active = false;
      mCurItem = pos;
      items[mCurItem].textpane.active = true;
      normCurItem();
    }
    return pos;
  }

  // for layouts
  private IRCServer lastActiveServer = null;
  private Object lastActiveChanUser = null;

  void fixLayout () {
    if (mCurItem < 0 || mCurItem >= items.length) {
      lastActiveServer = null;
      lastActiveChanUser = null;
      return;
    }
    auto it = &items[mCurItem];
    // system pane?
    if (it.system) {
      if (lastActiveServer !is null || lastActiveChanUser !is null) {
        lastActiveServer = null;
        lastActiveChanUser = null;
        (new EventXKbSetLayout(0)).post;
      }
      return;
    }
    // server pane?
    if (it.server) {
      if (lastActiveServer !is it.srv || lastActiveChanUser !is null) {
        lastActiveServer = it.srv;
        lastActiveChanUser = null;
        (new EventXKbSetLayout(0)).post;
      }
      return;
    }
    // channel pane?
    if (it.channel) {
      if (lastActiveChanUser !is it.chan) {
        int doChange = it.chan.server.forceXKbLayout;
        lastActiveServer = it.chan.server;
        lastActiveChanUser = it.chan;
        if (doChange >= 0) (new EventXKbSetLayout(doChange)).post;
      }
      return;
    }
    // private chat pane?
    if (it.privchat) {
      if (lastActiveChanUser !is it.user) {
        int doChange = it.user.server.forceXKbLayout;
        lastActiveServer = it.user.server;
        lastActiveChanUser = it.user;
        if (doChange >= 0) (new EventXKbSetLayout(doChange)).post;
      }
      return;
    }
    // something unknown, reset flags
    lastActiveServer = null;
    lastActiveChanUser = null;
  }

  void normCurItem () {
    if (mCurItem < 0) mCurItem = 0;
    if (mCurItem >= items.length) mCurItem = cast(int)items.length-1;
    if (topitem < mCurItem) topitem = mCurItem;
    if (topitem+ttyh >= mCurItem) {
      topitem = mCurItem-ttyh+1;
      if (topitem < 0) topitem = 0;
    }
    textpane.doCenterMark();
    fixLayout();
  }

  void goUp () {
    if (mCurItem > 0) {
      textpane.active = false;
      --mCurItem;
      normCurItem();
      textpane.active = true;
    }
  }

  void goDown () {
    if (mCurItem+1 < items.length) {
      textpane.active = false;
      ++mCurItem;
      normCurItem();
      textpane.active = true;
    }
  }

  void draw () {
    import miri.textpane : NickBG;
    auto win = XtWindow(0, 0, ChanTabWidth, ttyh);
    win.fg = 15;
    win.bg = NickBG;
    win.fill(0, 0, win.width, win.height, ' ');
    win.vline(win.width-1, 0, win.height);
    win.width = win.width-1;
    normCurItem(); // just in case
    int y = 0;
    int it = topitem;
    while (y < win.height && it < items.length) {
      win.fg = 7;
      win.bg = NickBG;
      auto item = &items[it];
      if (it == mCurItem) {
        win.fg = 15;
        win.bg = TtyRgb2Color!(0x00, 0x5f, 0xff);
        win.writeCharsAt(0, y, win.width, ' ');
      } else {
        win.fg = (item.server ? 15 : 7);
        win.bg = NickBG;
        if (!item.textpane.active && item.textpane.hasmark) {
          win.fg = TtyRgb2Color!(0x00, 0xff, 0x00);
        } else {
          if (item.channel && !item.chan.joined) win.fg = 6;
          if (item.server && !item.srv.alive) win.fg = 2;
          if (item.privchat && item.user.dead) win.fg = 8;
        }
      }
      win.writeStrAt((item.privchat ? 1 : item.channel ? 1 : 0), y, item.text);
      ++it;
      ++y;
    }
  }
}

__gshared ChatList chatlist;


// ////////////////////////////////////////////////////////////////////////// //
void curpanemsgf(A...) (cstring fmt, A args) {
  import std.format : formattedWrite;
  char[] st;
  struct Writer { void put (cstring s...) { st ~= s[]; } }
  Writer wr;
  formattedWrite(wr, fmt, args);
  (new EventSysMsg(cast(string)st)).post; // it is safe to cast here
  if (st.length) chatlist.textpane.addLine(null, st, TextLine.Hi.Service);
}


void curpanemsgfnosys(A...) (cstring fmt, A args) {
  import std.format : formattedWrite;
  char[] st;
  struct Writer { void put (cstring s...) { st ~= s[]; } }
  Writer wr;
  formattedWrite(wr, fmt, args);
  (new EventSysMsg(cast(string)st)).post; // it is safe to cast here
  if (st.length) chatlist.textpane.addLine(null, st, TextLine.Hi.Service);
}


void doSendText (cstring text) {
  if (text.ircstrip.length == 0) return;
  if (!chatlist.sendMsg(text)) ttyBeep;
}


T getNickFromArg(T : cstring) (T arg) {
  auto nk = arg.ircstrip;
  if (nk.length && (nk[$-1] == ':' || nk[$-1] == ',')) nk = nk[0..$-1].ircstrip;
  return nk;
}


char[] argsJoin (cstring[] args) {
  char[] tlong;
  foreach (cstring s; args[]) {
    s = s.xstrip;
    if (s.length == 0) continue;
    if (tlong.length) tlong ~= ' ';
    tlong ~= s;
  }
  return tlong;
}


// ////////////////////////////////////////////////////////////////////////// //
// optional cmdXXX arg: `cstring text`: text *WITHOUT* command (i.e. only args), unparsed

// move current item up
void cmdMoveUp (cstring cmd, cstring[] args/*, cstring text*/) {
  if (!chatlist) return;
  chatlist.moveItemUp(chatlist.mCurItem);
}


// salias alias server[:port] nick[:password] [options]
//   utfuck
//   koi8/cp1251/cp866
//   sendcodepage
void cmdSAlias (cstring cmd, cstring[] args/*, cstring text*/) {
  if (args.length < 3) {
    sysmsgf("%s",
      "salias alias proto:server[:port] nick[:password] [options]\n"~
      "  utfuck\n"~
      "  koi8/cp1251/cp866\n"~
      "  sendcodepage"
    );
    return;
  }
  IRCServer srv;
  try {
    import iv.strex : startsWith, lastIndexOf;
    if (args[1].startsWith("tox:")) {
      srv = new IRCServer(args[1], 0);
    } else if (args[1].indexOf(':') >= 0) {
      import std.conv : to;
      auto idx = args[1].lastIndexOf(':');
      auto port = args[1][idx+1..$].to!ushort;
      srv = new IRCServer(args[1][0..idx], port);
    } else {
      srv = new IRCServer(args[1], 6667);
    }
  } catch (Exception e) {
    curpanemsgf("error parsing server address: %s", e.msg);
    return;
  }
  srv.srvalias = args[0].idup;
  if (args[2].indexOf(':') >= 0) {
    auto idx = args[2].indexOf(':');
    srv.self.nick = args[2][0..idx].idup;
    srv.password = args[2][idx+1..$].idup;
  } else {
    srv.self.nick = args[2].idup;
  }
  if (srv.self.nick.length == 0) {
    curpanemsgf("empty nick!");
    return;
  }
  foreach (cstring opt; args[3..$]) {
         if ("utfuck".ircStrEquCI(opt)) srv.utfucked = true;
    else if ("koi8".ircStrEquCI(opt)) { srv.utfucked = false; srv.codepage = srv.CodePage.koi8u; }
    else if ("cp1251".ircStrEquCI(opt)) { srv.utfucked = false; srv.codepage = srv.CodePage.cp1251; }
    else if ("cp866".ircStrEquCI(opt)) { srv.utfucked = false; srv.codepage = srv.CodePage.cp866; }
    else if ("sendcp".ircStrEquCI(opt)) srv.sendCodepage = true;
    else if ("sendcodepage".ircStrEquCI(opt)) srv.sendCodepage = true;
    else {
      curpanemsgf("invalid option: %s", opt);
      return;
    }
  }
  if (srv.saveConfig()) {
    curpanemsgf("alias '%s' added", srv.srvalias);
  } else {
    curpanemsgf("ERROR adding alias '%s'", srv.srvalias);
  }
}

void cmdXCmd (cstring cmd, cstring[] args, cstring text) {
  if (args.length == 0 || text.length == 0) {
    curpanemsgf("cmd what?");
    return;
  }
  auto srv = chatlist.cursrv;
  if (srv is null) {
    curpanemsgf("command to which server?");
    return;
  }
  if (!srv.alive) {
    curpanemsgf("can't command dead server");
    return;
  }
  srv.sendf("%s", text);
}

void cmdMsg (cstring cmd, cstring[] args, cstring text) {
  if (args.length < 2 || text.length == 0) {
    curpanemsgf("msg whom?");
    return;
  }
  auto srv = chatlist.cursrv;
  if (srv is null) {
    curpanemsgf("command to which server?");
    return;
  }
  if (!srv.alive) {
    curpanemsgf("can't command dead server");
    return;
  }
  // get name
  text = text.xstrip();
  usize sppos = 0;
  while (sppos < text.length && text[sppos] > ' ') ++sppos;
  if (sppos >= text.length) {
    curpanemsgf("msg what?");
    return;
  }
  cstring who = text[0..sppos];
  text = text[sppos..$].xstrip();
  if (text.length == 0) {
    curpanemsgf("msg what?");
    return;
  }
  srv.sendf("PRIVMSG %s :%s", who, text);
}


void cmdQuit (cstring cmd, cstring[] args) {
  (new EventQuitRun()).post;
}

void cmdClose (cstring cmd, cstring[] args) {
  chatlist.doCloseCurrentTab();
}

void cmdJoin (cstring cmd, cstring[] args) {
  if (args.length == 0) {
    curpanemsgf("join to?");
    return;
  }
  auto srv = chatlist.cursrv;
  if (srv is null) {
    curpanemsgf("join on which server?");
    return;
  }
  foreach (cstring cname; args) {
    if (cname.length == 0) continue;
    if (cname[0] != '#' || cname.length < 2) {
      curpanemsgf("invalid channel name: '%s'", cname);
    } else {
      srv.sendf("JOIN %s", cname);
    }
  }
}

void cmdPart (cstring cmd, cstring[] args) {
  if (args.length != 0) {
    curpanemsgf("/part cannot accept args");
    return;
  }
  if (auto chan = chatlist.curchan) {
    if (chan.joined) {
      chan.server.sendf("PART %s", chan.name);
    } else {
      curpanemsgf("you aren't joined %s", chan.name);
    }
  } else {
    curpanemsgf("part from?");
  }
}

void cmdNick (cstring cmd, cstring[] args) {
  if (args.length != 1) {
    curpanemsgf("change nick to?");
    return;
  }
  auto srv = chatlist.cursrv;
  if (srv is null) {
    curpanemsgf("change nick on which server?");
    return;
  }
  auto nk = getNickFromArg(args[0]);
  if (nk.length == 0 || nk[0] == '#') { ttyBeep; return; }
  srv.sendf("NICK %s", nk);
}

void cmdAuthorizeNick (cstring cmd, cstring[] args) {
  auto srv = chatlist.cursrv;
  if (srv is null) {
    curpanemsgf("authorize on which server?");
    return;
  }
  srv.authorize();
}

// start private chat
void cmdPrivate (cstring cmd, cstring[] args) {
  if (args.length != 1) {
    curpanemsgf("private chat with?");
    return;
  }
  auto srv = chatlist.cursrv;
  if (srv is null) {
    curpanemsgf("change nick on which server?");
    return;
  }
  auto nk = getNickFromArg(args[0]);
  if (nk.length == 0 || nk[0] == '#') { ttyBeep; return; }
  if (auto user = srv.findUser(nk)) {
    chatlist.ensurePrivChat!true(user);
  } else {
    curpanemsgf("opening chat with non-channel nick '%s'", nk);
    auto newusr = srv.findUser!true(nk);
    if (newusr) chatlist.ensurePrivChat!true(newusr);
  }
}

void cmdMe (cstring cmd, cstring[] args, cstring text) {
  if (args.length == 0 || text.length == 0) {
    curpanemsgf("me what?");
    return;
  }
  string act = "\x01ACTION "~text.idup~"\x01";
  if (!chatlist.sendMsg(act)) ttyBeep;
}

void cmdVersion (cstring cmd, cstring[] args, cstring text) {
  auto srv = chatlist.cursrv;
  if (srv is null) {
    curpanemsgf("VERSION on which server?");
    return;
  }
  if (args.length == 0 || text.length == 0) {
    curpanemsgf("version who?");
    return;
  }
  if (args.length != 1) {
    curpanemsgf("which nick?");
    return;
  }
  cstring nick = args[0].xstrip();
  while (nick.length && (nick[$-1] == ',' || nick[$-1] == ':' || nick[$-1] == '.' || nick[$-1] == ';')) nick = nick[0..$-1];
  if (nick.length == 0) {
    curpanemsgf("which nick?");
    return;
  }
  srv.sendf("PRIVMSG %s :\x01VERSION\x01", nick);
}

// /allowchat nick
void cmdAllowChat (cstring cmd, cstring[] args) {
  auto srv = chatlist.cursrv;
  if (srv is null) {
    curpanemsgf("ignore on which server?");
    return;
  }
  if (args.length == 0) return;
  auto unick = getNickFromArg(args[0]);
  if (unick.length == 0) { ttyBeep; return; }
  srv.allowIngoredPrivChat(unick, true);
}

// /nochat nick
void cmdNoChat (cstring cmd, cstring[] args) {
  auto srv = chatlist.cursrv;
  if (srv is null) {
    curpanemsgf("ignore on which server?");
    return;
  }
  if (args.length == 0) return;
  auto unick = getNickFromArg(args[0]);
  if (unick.length == 0) { ttyBeep; return; }
  srv.allowIngoredPrivChat(unick, false);
}

// /ignore [nick] [dsc] [longdsc]
void cmdIgnore (cstring cmd, cstring[] args) {
  auto srv = chatlist.cursrv;
  if (srv is null) {
    curpanemsgf("ignore on which server?");
    return;
  }
  // list ignores?
  if (args.length == 0) {
    auto list = srv.allIgnores();
    if (list.length == 0) {
      curpanemsgfnosys("you are ignoring noone");
    } else {
      import std.algorithm : sort;
      import std.string : format;
      list.sort!((ref i0, ref i1) => (ircStrCmpCI(i0.nick, i1.nick) < 0));
      string rep = "you are ignoring %s people".format(list.length);
      foreach (ref ii; list) {
        rep ~= "\n ";
        if (ii.allowprivchat) rep ~= "*"; else rep ~= " ";
        rep ~= ii.nick;
        if (ii.ignoreShort.length > 0) rep ~= " <"~ii.ignoreShort~">";
        if (ii.ignoreLong.length > 0) rep ~= " : "~ii.ignoreLong;
      }
      curpanemsgfnosys("%s", rep);
    }
    return;
  }
  // add new ignore
  auto unick = getNickFromArg(args[0]);
  if (unick.length == 0) { ttyBeep; return; }
  cstring tshort;
  char[] tlong;
  if (args.length > 1) tshort = args[1];
  if (args.length > 2) tlong = argsJoin(args[2..$]);
  auto ii = srv.findIgnoreInfo(unick);
  if (ii.nick.length != 0) {
    curpanemsgfnosys("you are already ignoring %s", ii.nick);
  } else {
    srv.ignoreUser(unick, tshort, tlong);
    ii = srv.findIgnoreInfo(unick);
    if (ii.nick.length > 0) curpanemsgfnosys("you are now ignoring %s", ii.nick);
  }
}

// /unignore nick
void cmdUnignore (cstring cmd, cstring[] args) {
  auto srv = chatlist.cursrv;
  if (srv is null) {
    curpanemsgf("unignore on which server?");
    return;
  }
  if (args.length != 1) {
    curpanemsgfnosys("unignore who?");
    return;
  }
  auto unick = getNickFromArg(args[0]);
  if (unick.length == 0) { ttyBeep; return; }
  auto ii = srv.findIgnoreInfo(unick);
  if (ii.nick.length == 0) {
    curpanemsgfnosys("you are not ignoring %s", unick);
  } else {
    srv.unignoreUser(unick);
    ii = srv.findIgnoreInfo(unick);
    if (ii.nick.length == 0) curpanemsgfnosys("you are no longer ignoring %s", unick);
  }
}

// /nonotify [nick]
void cmdNonotify (cstring cmd, cstring[] args) {
  auto srv = chatlist.cursrv;
  if (srv is null) {
    curpanemsgf("nonotify on which server?");
    return;
  }
  // list ignores?
  if (args.length == 0) {
    auto list = srv.allNonitify();
    if (list.length == 0) {
      curpanemsgfnosys("nonotify list is empty");
    } else {
      import std.algorithm : sort;
      import std.string : format;
      list.sort!((ref i0, ref i1) => (ircStrCmpCI(i0.nick, i1.nick) < 0));
      string rep = "nonotify %s people".format(list.length);
      foreach (ref ii; list) {
        rep ~= "\n ";
        if (ii.ignore) rep ~= "*"; else rep ~= " ";
        rep ~= ii.nick;
      }
      curpanemsgfnosys("%s", rep);
    }
    return;
  }
  // add new nonotify
  auto unick = getNickFromArg(args[0]);
  if (unick.length == 0) { ttyBeep; return; }
  auto ii = srv.findNonotifyInfo(unick);
  if (ii.nick.length != 0) {
    curpanemsgfnosys("%s is already nonotify", ii.nick);
  } else {
    srv.nonotifyUser(unick);
    ii = srv.findNonotifyInfo(unick);
    if (ii.nick.length > 0) curpanemsgfnosys("%s is nonotify now", ii.nick);
  }
}

// /donotify nick
void cmdDonotify (cstring cmd, cstring[] args) {
  auto srv = chatlist.cursrv;
  if (srv is null) {
    curpanemsgf("donotify on which server?");
    return;
  }
  if (args.length != 1) {
    curpanemsgfnosys("donotify who?");
    return;
  }
  auto unick = getNickFromArg(args[0]);
  if (unick.length == 0) { ttyBeep; return; }
  auto ii = srv.findNonotifyInfo(unick);
  if (ii.nick.length == 0) {
    curpanemsgfnosys("%s is not nonotify", unick);
  } else {
    srv.donotifyUser(unick);
    ii = srv.findNonotifyInfo(unick);
    if (ii.nick.length == 0) curpanemsgfnosys("%s is donotify now", unick);
  }
}

void cmdBlink (cstring cmd, cstring[] args) {
  auto chan = chatlist.curchan;
  if (chan is null) {
    curpanemsgf("what chan?");
    return;
  }
  if (args.length == 1 && args[0] == "?") {
    curpanemsgf("blink status: %s", (chan.blink ? "on" : "off"));
    return;
  }
  if (args.length != 0) {
    curpanemsgf("/blink doesn't like args");
    return;
  }
  chan.server.setChanBlink(chan, true);
}

void cmdUnblink (cstring cmd, cstring[] args) {
  auto chan = chatlist.curchan;
  if (chan is null) {
    curpanemsgf("what chan?");
    return;
  }
  if (args.length == 1 && args[0] == "?") {
    curpanemsgf("blink status: %s", (chan.blink ? "on" : "off"));
    return;
  }
  if (args.length != 0) {
    curpanemsgf("/unblink doesn't like args");
    return;
  }
  chan.server.setChanBlink(chan, false);
}

void cmdPopup (cstring cmd, cstring[] args) {
  auto chan = chatlist.curchan;
  if (chan is null) {
    curpanemsgf("what chan?");
    return;
  }
  if (args.length == 1 && args[0] == "?") {
    curpanemsgf("popup status: %s", (chan.blink ? "on" : "off"));
    return;
  }
  if (args.length != 0) {
    curpanemsgf("/popup doesn't like args");
    return;
  }
  chan.server.setChanPopup(chan, true);
}

void cmdUnpopup (cstring cmd, cstring[] args) {
  auto chan = chatlist.curchan;
  if (chan is null) {
    curpanemsgf("what chan?");
    return;
  }
  if (args.length == 1 && args[0] == "?") {
    curpanemsgf("popup status: %s", (chan.blink ? "on" : "off"));
    return;
  }
  if (args.length != 0) {
    curpanemsgf("/unpopup doesn't like args");
    return;
  }
  chan.server.setChanPopup(chan, false);
}

void cmdDisconnect (cstring cmd, cstring[] args) {
  auto srv = chatlist.cursrv;
  if (srv is null) {
    curpanemsgf("disconnect from which server?");
    return;
  }
  srv.disconnect(); // this will stop reconnection attempts
}

void cmdReconnect (cstring cmd, cstring[] args) {
  auto srv = chatlist.cursrv;
  if (srv is null) {
    curpanemsgf("reconnect to which server?");
    return;
  }
  srv.ForceReconnect();
}

/*
void cmdReconnect (cstring cmd, cstring[] args) {
  auto srv = chatlist.cursrv;
  if (srv is null) {
    curpanemsgf("reconnect to which server?");
    return;
  }
  if (!srv.alive) srv.connect();
}
*/

void cmdVersion (cstring cmd, cstring[] args) {
  if (args.length == 0) {
    if (auto user = chatlist.curpriv) {
      user.server.sendf("PRIVMSG %s :\x01VERSION\x01", user.nick);
    } else {
      curpanemsgf("version of what?");
    }
    return;
  }
  if (args.length != 1) {
    curpanemsgf("version of what?");
    return;
  }
  auto srv = chatlist.cursrv;
  if (srv is null) {
    curpanemsgf("on which server?");
    return;
  }
  auto nk = getNickFromArg(args[0]);
  if (nk.length == 0) { ttyBeep; return; }
  if (auto user = srv.findUser(nk)) {
    srv.sendf("PRIVMSG %s :\x01VERSION\x01", user.nick);
  }
}

void cmdWhoIs (cstring cmd, cstring[] args) {
  if (args.length == 0) {
    if (auto user = chatlist.curpriv) {
      user.server.sendf("WHOIS %s", user.nick);
    } else {
      curpanemsgf("whois of what?");
    }
    return;
  }
  if (args.length != 1) {
    curpanemsgf("whois of what?");
    return;
  }
  auto srv = chatlist.cursrv;
  if (srv is null) {
    curpanemsgf("on which server?");
    return;
  }
  auto nk = getNickFromArg(args[0]);
  if (nk.length == 0) { ttyBeep; return; }
  if (auto user = srv.findUser(nk)) {
    srv.sendf("WHOIS %s", user.nick);
  }
}

// /topic [topic]
void cmdTopic (cstring cmd, cstring[] args, cstring text) {
  text = text.xstrip;
  auto chan = chatlist.curchan;
  if (chan is null) {
    curpanemsgf("topic for what?");
    return;
  }
  chan.server.sendf("TOPIC %s :%s", chan.name, text);
}

void cmdForceLayout (cstring cmd, cstring[] args) {
  if (args.length != 1) {
    curpanemsgf("which layout?");
    return;
  }
  auto srv = chatlist.cursrv;
  if (srv is null) {
    curpanemsgf("for which server?");
    return;
  }
  int lay = -1;
  try {
    import std.conv : to; lay = args[0].to!int;
  } catch (Exception e) {
    curpanemsgf("invalid layout number");
    return;
  }
  if (lay < 0) lay = -1;
  if (srv.forceXKbLayout != lay) {
    srv.forceXKbLayout = lay;
    srv.saveConfig();
  }
}


// ////////////////////////////////////////////////////////////////////////// //
void doCommand (cstring text) {
  if (text.ircstrip.length == 0) return;
  if (text.ptr[0] == '/') {
    // command
    auto origtext = text;
    while (origtext.length && origtext.ptr[0] > ' ') origtext = origtext[1..$];
    while (origtext.length && (origtext.ptr[0] == ' ' || origtext.ptr[0] == '\t')) origtext = origtext[1..$];
    // parse
    cstring[] args;
    text = text[1..$];
    while (text.length > 0) {
      if (text.ptr[0] <= ' ') { text = text[1..$]; continue; }
      typeof(text.length) pos = 0;
      while (pos < text.length && text.ptr[pos] > ' ') ++pos;
      args ~= text[0..pos];
      text = text[pos..$];
    }
    if (args.length == 0) {
      ttyBeep();
      sysmsgf("%s", "empty command");
      return;
    }
    foreach (string mname; __traits(allMembers, mixin(__MODULE__))) {
      static if (mname.length > 3 && mname[0..3] == "cmd") {
        static if (is(typeof(__traits(getMember, mixin(__MODULE__), mname)) == function)) {
          static if (is(typeof({__traits(getMember, mixin(__MODULE__), mname)(args[0], args[1..$], origtext);}))) {
            if (mname.length == args[0].length+3 && mname[3..$].ircStrEquCI(args[0])) {
              __traits(getMember, mixin(__MODULE__), mname)(args[0], args[1..$], origtext);
              return;
            }
          } else static if (is(typeof({__traits(getMember, mixin(__MODULE__), mname)(args[0], args[1..$]);}))) {
            if (mname.length == args[0].length+3 && mname[3..$].ircStrEquCI(args[0])) {
              __traits(getMember, mixin(__MODULE__), mname)(args[0], args[1..$]);
              return;
            }
          }
        }
      }
    }
    // try server alias
    if (args.length == 1) {
      try {
        auto srv = new IRCServer(args[0]);
        // i found her!
        srv = chatlist.findAndActivateServerByAlias(srv);
        if (srv !is null) srv.connect();
        return;
      } catch (Exception e) {}
    }
    // just send it verbatim
    import std.array : join;
    string cmd;
    foreach (char ch; args[0]) cmd ~= irc2upper(ch);
    if (auto user = chatlist.curpriv) {
      if (args.length > 1) {
        user.server.sendf("%s %s %s", cmd, user.nick, args[1..$].join(" "));
      } else {
        user.server.sendf("%s %s", cmd, user.nick);
      }
    } else if (auto chan = chatlist.curchan) {
      if (args.length > 1) {
        chan.server.sendf("%s %s %s", cmd, chan.name, args[1..$].join(" "));
        //chan.server.sendf("%s %s", cmd, args[1..$].join(" "));
      } else {
        chan.server.sendf("%s %s", cmd, chan.name);
        //chan.server.sendf("%s", cmd);
      }
    } else if (auto srv = chatlist.cursrv) {
      if (args.length > 1) {
        srv.sendf("%s %s", cmd, args[1..$].join(" "));
      } else {
        srv.sendf("%s", cmd);
      }
    } else {
      sysmsgf("unknown command: '%s'", args[0]);
    }
    return;
  }
  doSendText(text);
}


// ////////////////////////////////////////////////////////////////////////// //
void sizecb () {
  TextPaneWidth = ttyw-ChanTabWidth-1-NickTabWidth-1;
  if (TextPaneWidth < 30) TextPaneWidth = 30;
}


// ////////////////////////////////////////////////////////////////////////// //
int userPaneX0 () { return ChanTabWidth+TextPaneWidth; }
int userPaneY0 () {
  int y = ttyh-userPaneHeight;
  if (y < 1) y = 1;
  return y;
}

int userPaneWidth () {
  int w = ttyw-ChanTabWidth+TextPaneWidth;
  if (w < 17) w = 17;
  return w;
}

int userPaneHeight () {
  int h = ttyh-15;
  if (h < 4) h = 4;
  return h;
}


void fixWidgetSizes () {
  int tphgt = ttyh-1-(inputModeSingle ? inputOneLine.height : inputMultiLine.height);
  string mynick;

  foreach (ref it; chatlist.items) {
    if (auto tp = it.textpane) {
      tp.x0 = ChanTabWidth;
      tp.y0 = 1;
      tp.height = tphgt;
      tp.width = TextPaneWidth;
      if (!tp.active) {
        if (!tp.wasLogMove) tp.doCenterMark(true);
      }
    }
  }

  if (auto srv = chatlist.cursrv) mynick = srv.self.nick;

  inputOneLine.x0 = ChanTabWidth+(mynick.length ? cast(int)mynick.length+1 : 0);
  inputOneLine.y0 = ttyh-1;
  inputOneLine.width = TextPaneWidth-(mynick.length ? cast(int)mynick.length+1 : 0);

  inputMultiLine.x0 = ChanTabWidth+(mynick.length ? cast(int)mynick.length+1 : 0);
  inputMultiLine.y0 = ttyh-inputMultiLine.height;
  inputMultiLine.width = TextPaneWidth-(mynick.length ? cast(int)mynick.length+1 : 0);

  inputUserFilter.x0 = userPaneX0+1;
  inputUserFilter.y0 = userPaneY0-1;
  inputUserFilter.width = userPaneWidth-1;
}


char[] getEditorText (EditorEngine ed) {
  if (ed is null) return null;
  char[] text;
  text.reserve(ed.textsize);
  foreach (char ch; ed[]) text ~= ch;
  return text;
}


void switchInputMode (bool multi) {
  if (inputModeSingle == !multi) return;
  if (multi) {
    // switch to multiline
    auto text = getEditorText(inputOneLine);
    inputOneLine.clear();
    inputMultiLine.clear();
    inputMultiLine.insertText!"end"(0, text);
    inputMultiLine.clearUndo();
    inputModeSingle = false;
  } else {
    // switch to single line
    auto text = getEditorText(inputMultiLine);
    while (text.length > 0 && text[$-1] <= ' ') text = text[0..$-1];
    if (text.indexOf('\n') >= 0) return; // can't
    inputMultiLine.clear();
    inputOneLine.clear();
    inputOneLine.insertText!"end"(0, text);
    inputOneLine.clearUndo();
    inputModeSingle = true;
  }
}


bool keycb (TtyEvent key) {
  //if (key == "^C") return -1; // abort
  fixWidgetSizes();

  if (key == "FocusOut") { (new EventFocusOut()).post; return true; }
  if (key == "FocusIn") { (new EventFocusIn()).post; return true; }

  if (key == "^L") { xtFullRefresh(); return true; }

  if (key == "M-L") { chatlist.textpane.doCenterMark(true); return true; }

  if (key == "^PageUp") { chatlist.goUp(); return true; }
  if (key == "^PageDown") { chatlist.goDown(); return true; }

  if (key.mouse) {
    if (key.key == TtyEvent.Key.MLeftUp) {
      if (auto tp = chatlist.textpane) tp.clicked(key.x, key.y);
    }
    return true;
  }

  if (inputActive) {
    if (inputModeSingle) {
      if (key == "PageUp") { if (auto tp = chatlist.textpane) tp.doPageUp(); return true; }
      if (key == "PageDown") { if (auto tp = chatlist.textpane) tp.doPageDown(); return true; }
      if (key == "S-Up") { if (auto tp = chatlist.textpane) tp.doLineUp(); return true; }
      if (key == "S-Down") { if (auto tp = chatlist.textpane) tp.doLineDown(); return true; }
    }
    if (key == "M-PageUp") { if (auto tp = chatlist.textpane) tp.doPageUp(); return true; }
    if (key == "M-PageDown") { if (auto tp = chatlist.textpane) tp.doPageDown(); return true; }
    if (key == "M-Up") { if (auto tp = chatlist.textpane) tp.doLineUp(); return true; }
    if (key == "M-Down") { if (auto tp = chatlist.textpane) tp.doLineDown(); return true; }
  }

  // go to input line
  if (!inputActive && key == "F8") {
    inputActive = true;
    userFilterFocused = false;
    return true;
  }

  // go to userlist filter
  if (!userFilterFocused && key == "S-F8") {
    inputActive = false;
    userFilterFocused = true;
    return true;
  }

  if (key == "M-M") { switchInputMode(inputModeSingle); return true; }
  if (inputActive && key == "^Up") { switchInputMode(true); return true; }
  if (inputActive && key == "^Down") { switchInputMode(false); return true; }

  if (inputActive) {
    TtyEditor cured = (inputModeSingle ? inputOneLine : inputMultiLine);
    if (cured is null) return false; // just in case
    // send?
    if ((inputModeSingle && key == "Enter") || (!inputModeSingle && key == "M-Enter")) {
      import core.time;
      static lastSendTime = MonoTime.zero;
      auto curtime = MonoTime.currTime;
      if ((curtime-lastSendTime).total!"msecs" < 400) {
        ttyBeep();
      } else {
        auto text = getEditorText(cured);
        cured.clear();
        switchInputMode(false);
        doCommand(text);
      }
      lastSendTime = curtime;
      return true;
    }
    // autocompletion
    if (key == "Tab") {
      cured.autocomplete(chatlist.curchan);
      return true;
    }
    // normal input
    auto tlen = cured.textsize;
    if (cured.processKey(key)) {
      // switch to eng if we're tying a command
      if (tlen == 0 && cured.textsize == 1 && cured[0] == '/') {
        (new EventXKbSetLayout(0)).post;
      }
    }
  }

  if (userFilterFocused) {
    return inputUserFilter.processKey(key);
  }

  return false;
}


// ////////////////////////////////////////////////////////////////////////// //
void drawScreen () {
  void drawUsers () {
    import miri.textpane : NickBG;

    // clear whole pane and draw vline
    auto win = XtWindow(userPaneX0, 0, ttyw, ttyh);
    win.fg = 15;
    win.bg = NickBG;
    win.fill(0, 0, win.width, win.height, ' ');
    win.vline(0, 0, win.height);

    inputUserFilter.fullDirty();
    inputUserFilter.drawPage();

    auto chan = chatlist.curchan;
    if (chan is null || chan.users.length == 0) return;

    RegExp userFilterRE;

    if (inputUserFilter.textsize > 0) {
      import iv.strex : xstrip;
      auto text = inputUserFilter.getEditorText().xstrip;
      if (text.length) {
        import std.utf : byChar;
        userFilterRE = RegExp.create(text.byChar, SRFlags.CaseInsensitive);
      }
    }

    win = XtWindow(userPaneX0+1, userPaneY0, userPaneWidth-1, userPaneHeight);
    win.fg = 15;
    win.bg = NickBG;

    int y = 0;
    int it = 0;
    while (y < win.height && it < chan.users.length) {
      auto user = chan.users[it];
      // do filtering
      if (userFilterRE.valid) {
        auto rectx = Thompson.create(userFilterRE);
        if (rectx.exec(user.nick) != SRes.Ok) { ++it; continue; } // not matched
      }
      win.fg = 7;
      win.bg = NickBG;
      char mode = ' ';
      if (chan.isOp(user)) { mode = '@'; win.fg = 15; }
      if (chan.isVoiced(user)) { mode = '+'; win.fg = 3+8; }
      if (user.ignored) {
        win.fg = 238;
      } else {
        final switch (user.status) {
          case IRCUser.Status.Offline: win.fg = 239; break;
          case IRCUser.Status.Online: break;
          case IRCUser.Status.Away: win.fg = 6; break;
        }
      }
      win.writeCharsAt(0, y, 1, mode);
      win.writeStrAt(1, y, user.visNick);
      ++it;
      ++y;
    }
  }

  scope(exit) xtFlush();

  fixWidgetSizes();

  chatlist.draw();
  chatlist.textpane.draw();

  // draw channel topic
  {
    import miri.textpane : TextBG;
    auto win = XtWindow(chatlist.textpane.x0, 0, chatlist.textpane.width, 1);
    win.fg = 7;
    win.bg = TtyRgb2Color!(0x20, 0x20, 0x20);
    win.writeCharsAt(0, 0, win.width, ' ');
    if (auto cc = chatlist.curchan) win.writeStrAt(0, 0, cc.topic);
  }

  drawUsers();

  string mynick;
  if (auto srv = chatlist.cursrv) mynick = srv.self.nick;
  TtyEditor ied = (inputModeSingle ? inputOneLine : inputMultiLine);

  if (mynick.length) {
    import miri.textpane : TextBG;
    auto win = XtWindow(ied.x0-cast(int)mynick.length-1, ied.y0, cast(int)mynick.length+1, ied.height);
    win.fg = 4+8;
    win.bg = TextBG;
    win.fill(0, 0, win.width, win.height, ' ');
    win.writeCharsAt(win.width-1, 0, 1, ':');
    win.writeStrAt(0, 0, mynick);
  }
  ied.fullDirty();
  {
    // uncomment the following to disable scrollbar
    //auto scs = ttyScissor;
    //scope(exit) ttyScissor = scs;
    //ttyScissor = scs.crop(ied.x0, ied.y0, ied.width, ied.height);
    ied.drawPage();
  }

  if (userFilterFocused) {
    inputUserFilter.drawCursor();
  }
}


// ////////////////////////////////////////////////////////////////////////// //
void main (string[] args) {
  version(none) {
    void test () {
      cstring[] args;
      cstring origtext;
      foreach (string mname; __traits(allMembers, mixin(__MODULE__))) {
        static if (mname.length > 3 && mname[0..3] == "cmd") {
          //pragma(msg, is(typeof(__traits(getMember, mixin(__MODULE__), mname)) == function));
          static if (is(typeof(__traits(getMember, mixin(__MODULE__), mname)) == function)) {
            static if (is(typeof({__traits(getMember, mixin(__MODULE__), mname)(args[0], args[1..$], origtext);}))) {
              writeln("0: ", mname);
            } else static if (is(typeof({__traits(getMember, mixin(__MODULE__), mname)(args[0], args[1..$]);}))) {
              writeln("1: ", mname);
            }
          }
        }
      }
    }
    test();
    assert(0);
  }

  if (ttyIsRedirected) assert(0, "no redirections, please");
  xtInit();
  ttyEnableMouseReports();
  if (ttyw < ChanTabWidth+NickTabWidth+30 || ttyh < 20) assert(0, "tty is too small");
  TextPaneWidth = ttyw-ChanTabWidth-1-NickTabWidth-1;

  foreach (string s; args[1..$]) {
    import iv.strex : startsWith;
    if (s == "--log") {
      logOpenFile("zlog.log");
    } else if (s.startsWith("--log=")) {
      logOpenFile(s[6..$]);
    }
  }

  initConfig();

  setupChatLogger();

  inputOneLine = new TtyEditor(ChanTabWidth, ttyh-1, TextPaneWidth, 1, true);
  inputUserFilter = new TtyEditor(ChanTabWidth, ttyh-1, TextPaneWidth, 1, true);

  inputMultiLine = new TtyEditor(ChanTabWidth, ttyh-8, TextPaneWidth, 8, false);
  inputMultiLine.hideStatus = true;

  fuckStdErr(); // for libnotify
  notify_init("Miri");
  scope(exit) notify_uninit();

  auto ttymode = ttyGetMode();
  scope(exit) {
    normalScreen();
    ttyDisableFocusReports();
    ttySetMode(ttymode);
  }
  ttySetRaw();
  altScreen();
  ttyEnableFocusReports();

  syspane = new TextPane();
  syspane.active = true;

  /*
  syspane.addLine("ketmar", "** hello", TextLine.Hi.Action);
  syspane.addLine("ketmar", "hello", TextLine.Hi.Mine);
  */
  /*
  {
    syspane.addLine("ketmar", "hello", TextLine.Hi.Mine);
    syspane.addMark();
    syspane.addLine("ketmar1", "fuck you");
    syspane.addLine("ketmar2", "  and fuck you too!", TextLine.Hi.ToMe);
    syspane.addLine("asshole[idiot]", "and you all!", TextLine.Hi.Ignored);
    foreach (int n; 0..128) {
      import std.format : format;
      syspane.addLine("line#%s".format(n), "item #%s".format(n));
    }
    //syspane.doCenterMark();
  }
  */

  addEventListener((EventSysMsg evt) {
    syspane.addLine("<system>", evt.msg);
  });

  sysmsgf("%s", "welcome...");

  //(new EventSysMsg("postponed event...")).later(5000);

  chatlist = new ChatList();

  addEventListener((EventTtyResized evt) { sizecb(); });
  addEventListener((EventTtyKey evt) { if (keycb(evt.key)) evt.eat(); });

  { import etc.linux.memoryerror; registerMemoryErrorHandler(); }

  {
    import core.stdc.stdio;
    FILE* errfl = fopen("/home/ketmar/back/D/prj/miri/zerr.err", "a");
    if (errfl is null) assert(0);
    errfl.fprintf("**********************************\n");
    errfl.fflush();
    try {
      netioRun(
        () => drawScreen(),
      );
      errfl.fclose();
    } catch (Throwable e) {
      import core.stdc.stdlib : abort;
      import core.memory : GC;
      import core.thread;
      GC.disable();
      ttyBeep();
      thread_suspendAll(); // stop right here, you criminal scum!
      auto enm = typeid(e).name;
      errfl.fprintf("\n******** FATAL %.*s: %.*s (%.*s:%u)\n", cast(uint)enm.length, enm.ptr, cast(uint)e.msg.length, e.msg.ptr, cast(uint)e.file.length, e.file.ptr, cast(uint)e.line);
      errfl.fflush();
      auto se = e.toString;
      errfl.fprintf("********\n%.*s\n********\n", cast(uint)se.length, se.ptr);
      errfl.fflush();
      errfl.fclose();
      //assert(0);
      normalScreen();
      ttyDisableFocusReports();
      ttySetMode(ttymode);
      abort();
    }
  }
}
